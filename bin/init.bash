#!/bin/bash

getdomain(){
	ok='f'
	trace='.'
	for i in {1..16}; do
		if [ -e "$trace/.proj.domain" ]; then
			ok='t'
			break;
		fi
		trace="$trace/.."
	done
	if [ "$ok" == 't' ]; then
		echo $trace;
	else
		exit 1;
	fi
}

url=$(getdomain)
export DOMAIN_PROJ=$(readlink -f -- "$url")
echo "Including: $DOMAIN_PROJ/bin"
export PATH=$DOMAIN_PROJ/bin:$PATH

source "$DOMAIN_PROJ/etc/autocomplete"
