#!/bin/bash

getdomain(){
	ok='f'
	trace='.'
	for i in {1..16}; do
		if [ -e "$trace/.$1.domain" ]; then
			ok='t'
			break;
		fi
		trace="$trace/.."
	done
	if [ "$ok" == 't' ]; then
		echo $trace;
	else
		exit 1;
	fi
}

echo_log(){
	echo -e "[LOG]: \033[1;0;32m$1\033[0m";
}

echo_warn(){
	echo -e "\033[1;1;41m$1\033[0m";
}

error(){
	echo -e "\033[1;1;31m[$1]: \033[0m$2";
	exit 1;
}

getcommands(){
	ls "$DOMAIN_PROJ/bin/proj.d"
}

getDeveloper(){
	email=$(git config user.email)
	ti2lines "$DOMAIN_PROJ/developers" | while read line; do
		declare -A person="$line"
		if [[ "${person[email]}" == "$email" ]]; then
			echo "${person[id]}"
			return 0;
		fi
	done;
	return 1;
}

if [ "$DOMAIN_PROJ" == "" ]; then
	exit "Environment Error" "Variable DOMAIN_PROJ is not defined"
fi

