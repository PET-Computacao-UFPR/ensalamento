#!/bin/bash

firefox google.com
source $HOME/.dbus/Xdbus

PATH_NOTIFY_SEND=$(whereis notify-send | cut -d' ' -f2)
echo $PATH_NOTIFY_SEND

PATH_FIREFOX=$(whereis firefox | cut -d' ' -f2)

# Verifica se o site caiu
wget -O /tmp/index_temp.html ensalamento.c3sl.ufpr.br || ( $PATH_NOTIFY_SEND 'Caiu o site Assembly!' 'Essa notificação notifica aos notificados.' --icon=dialog-information && $PATH_FIREFOX pudim.com.br )


