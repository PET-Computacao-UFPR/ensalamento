namespace :db do
  desc "Fix requireroom from schedules"
  task :fix_schedules => :environment do
      schedules = Schedule.all
      schedules.each do |sched|
          if sched.klass.subject.room_type.blank?
              sched.requireroom = false
              sched.save
          end
      end
  end
end
