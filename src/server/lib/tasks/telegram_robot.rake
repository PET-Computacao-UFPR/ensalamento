require 'telegram/bot'

namespace :telegram do
  desc "Start robot"
  task :start => :environment do
    token = '326068063:AAF17wrfywnziJCXuTwic8pTXmLpjsRckC0'
    Telegram::Bot::Client.run(token) do |bot|
      bot.listen do |raw|
        if raw.text.blank?;    next; end
        if raw.text[0] != '/'; next; end

        res = ""; msg = raw.text.split(" ")
        if msg[0] == "/subject"
            subject = Subject.findCode(msg[1])
            if subject.blank?
              res = "Disciplina #{msg[1]} não encontrada\n"
            else
              subject.getValidKlasses.each do |klass|
                res += klass.to_telegram + "\n"
              end
            end

        elsif msg[0] == '/professor'
            professor = Professor.findCode(msg[1])
            if professor.blank?
              res = "Professor #{msg[1]} não encontrado\n"
            else
              professor.klasses.each do |klass|
                res += klass.to_telegram + "\n"
              end
            end

        #elsif msg[0] == '/stop'
          #bot.api.send_message(chat_id: raw.chat.id, text: "finalizado!")

        else
           res = "Comando #{msg[0]} não encontrado"
        end

        bot.api.send_message(chat_id: raw.chat.id, text: res)
      end
    end
  end
end
