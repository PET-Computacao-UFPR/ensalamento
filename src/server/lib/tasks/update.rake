namespace :idb do
          desc "Update the database from YAML file"
          task :update => :environment do
                raw = YAML.load_file("teste.yml")
                raw["data"].each do |line|
                    code = line["code"]
                    subject = Subject.findCode(code)
                    if subject.blank?
                      puts("Subject #{code} not found")
                      next;
                    end
                    if line["hour_theorical"].present?
                        subject.hour_theorical = line["hour_theorical"]
                    end
                    if line["hour_practical"].present?
                        subject.hour_practical = line["hour_practical"]
                    end
                    if line["hour_internship"].present?
                        subject.hour_internship = line["hour_internship"]
                    end
                    if line["credits"].present?
                        subject.credits = line["credits"]
                    end
                    subject.save
                    puts("Subject #{code} updated")
                end
        end
end
