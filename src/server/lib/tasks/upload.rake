require "i18n"

namespace :db do
  desc "Upload all data from excel file to database"
  task :upload, [:season_id] => :environment do |cmd,args|
    season_id = args[:season_id]
    season    = Season.find( season_id )

    File.open("public/uploads/pid", 'w') { |file| file.write(Process.pid) }

    @arquivo = Roo::Spreadsheet.open("public/uploads/arquivo.csv",csv_options:{col_sep: ","})
    @arquivo = @arquivo.drop(0)

    hash_departments = Hash.new()
    Department.all.each do |department|
        hash_departments[ department.code ] = department
    end

    hash_courses     = Hash.new()
    Course.all.each do |course|
        hash_courses[ course.code ] = course
    end

    hash_subjects    = Hash.new()
    Subject.all.each do |subject|
        hash_subjects[ subject.code ] = subject
    end

    total = @arquivo.size.to_s
    File.open("public/uploads/total", 'w') { |file| file.write(total) }

    def_roomtype = RoomType.find(1);

    log = ""
    File.open("public/uploads/log", 'w') {}
    @arquivo.each_with_index do |linha, x|
        File.open("public/uploads/done", 'w') { |file| file.write((x+1).to_s) }
        subject_code       = linha[0].downcase
        subject_name       = linha[1]
        klass_code         = linha[2].downcase
        klass_size         = linha[3]
        schedule_weekday   = linha[4]
        schedule_ini       = linha[5]
        schedule_end       = linha[6]
        klass_type         = linha[7]
        course_code        = linha[8]
        department_name    = linha[9].downcase
        #session_name       = linha[10].downcase
        professor_code     = linha[11]

        if course_code.is_a? Numeric
            course_code = linha[8].round.to_s
        end

        department_code = I18n.transliterate(department_name)
        if department_code.include?"coordenacao do curso de "
          department_code = department_code[24,department_code.length]
          department_name = department_name[24,department_name.length]
        elsif department_code.include?"departamento de "
          department_code = department_code[16,department_code.length]
          department_name = department_name[16,department_name.length]
        end
        department_code = department_code.gsub(" ","_")


        if schedule_weekday.present?
            schedule_weekday = schedule_weekday.downcase
        end

        if schedule_weekday == "sábado" || schedule_weekday == "sabado"
            schedule_weekday = 7;
        else
            schedule_weekday = schedule_weekday.to_i
        end

        # Insert in the database the new departments
        if hash_departments.has_key?(department_code)
            department = hash_departments[ department_code ]
        else
            department = Department.new
            department.code = department_code
            department.name = department_name
            department.save
            hash_departments[ department_code ] = department
            log += "Criado Departamento: "+department_name+"\n"
        end

        # Insert in the database the new courses
        if hash_courses.has_key?(course_code)
            course = hash_courses[ course_code ]
        else
            course = Course.new
            course.code = course_code
            course.department << department
            course.save
            hash_courses[ course_code ] = course
            log += "Criado Curso: "+course_code.to_s+"\n"
        end

        # Insert in the database the new subjects
        if hash_subjects.has_key?(subject_code)
            subject = hash_subjects[ subject_code ]
            subject.courses << course
            subject.save
        else
            subject = Subject.new()
            subject.code = subject_code
            subject.name = subject_name
            subject.department = department
            subject.courses << course
            if schedule_ini.present?
              subject.room_type = def_roomtype
            end
            subject.save
            hash_subjects[ subject_code ] = subject
            log += "Criado Disciplina: "+subject_code+"-"+subject_name+"\n"
        end

puts subject.code

        if !Klass.exists?(code: klass_code, subject_id: subject.id, course_id: course.id)
            klass = Klass.new
            klass.code = klass_code
            klass.vacancy = klass_size
            klass.subject = subject
            klass.course  = course
            klass.season  = season
            if professor_code.present?
                professor = Professor.find_by(code:professor_code.downcase);
                if professor.present?
                    klass.professor = professor;
                end
            end
            klass.save
        else
            klass = Klass.where( ["code = ? and subject_id = ? and course_id = ?", klass_code, subject.id, course.id] ).first
        end

        if schedule_ini.present?
            #if !Schedule.exists?(klass: klass, weekday: schedule_weekday, ini: schedule_ini)   ## colocar verificacao por hora de inicio tambem
                schedule = Schedule.new
                schedule.ini = schedule_ini
                schedule.end = schedule_end
                schedule.weekday = schedule_weekday
                schedule.category  = klass_type
                schedule.requireroom = (subject.room_type.blank?) ? 'f' : 't';
                schedule.klass = klass;
                schedule.save
            #end
        end


        if log.size > 0
            open('public/uploads/log', 'a') do |f|
                f.puts log
                log = ""
            end
        end
    end

    FileUtils.rm("public/uploads/pid")

  end
end
