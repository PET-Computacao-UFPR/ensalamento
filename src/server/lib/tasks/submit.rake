namespace :idb do
          desc "Upload all data from excel file to database"
          task :submit => :environment do
                raw = File.read('public/ensalador/output1')
                lines = raw.split("\n");
                lines.each do |line|
                        part = line.split(" ");
                        room_code = part[0]
                        sched_id  = part[1]
                        schedule = Schedule.find(sched_id)
                        room = Room.findCode(room_code);
                        schedule.room = room
                        schedule.save
                end
        end
end

