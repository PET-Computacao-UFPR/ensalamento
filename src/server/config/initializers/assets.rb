# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.2'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js 	)
Rails.application.config.assets.precompile += %w( management/management.css management/management.js )
Rails.application.config.assets.precompile += %w( style.css )
Rails.application.config.assets.precompile += %w( geral.js )
Rails.application.config.assets.precompile += %w( about.js )
Rails.application.config.assets.precompile += %w( fullcalendarjs.js )
Rails.application.config.assets.precompile += %w( fullcalendarcss.css )
Rails.application.config.assets.precompile += %w( bootstrap/css/bootstrap.css bootstrap/jquery-2.1.4.min.js bootstrap/js/bootstrap.min.js bootstrap/script.js bootstrap/jquery.mask.min.js fullcalendar/lib/moment.min.js fullcalendar/fullcalendar.js fullcalendar/lang-all.js fullcalendar/fullcalendar.css)
