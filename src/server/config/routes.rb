Rails.application.routes.draw do
  require 'sidekiq/web'
  devise_for :users, controllers: {registrations: 'registrations'}

  namespace :professor do
      root to:'professor#index'
      resources :subjects
  end

  namespace :public do
      root to:'public#index'
      resources :blocks,      only:["index","show"]
      resources :courses,     only:["index","show"]
      resources :departments, only:["index","show"]
      resources :professors,  only:["index","show"]
      resources :rooms,       only:["index","show"]
      resources :room_types,  only:["index","show"]
      resources :subjects,    only:["index","show"]
      resources :sectors,     only:["index","show"]
      resources :klasses,     only:["index","show"]
      resources :weeks,       only:["index","show"]
      resources :appointments,only:["index","new","create"]

      post 'appointments/create', controller:'appointments', action: :create
      get 'appointments/query', to:'appointments#query', as:'appointments_query'
      get 'appointments/query_sched', to:'appointments#query_sched', as:'appointments_query_sched'
      get 'appointments/query_appoint', to:'appointments#query_appoint', as:'appointments_query_appoint'

      get "reports", to: "reports#index"

      get "db",      to: "db#index"
      get "teste",   to: "db#teste"
      #get "subjects/:subject_id/:id", to: "klasses#show"
  end

  namespace :legislation do
      resources :entities,      only:["index","show"]
  end



  namespace :about do
      root to:'about#index'
	  get "license", to: "about#license"
      resources :developers,  only:["index","show"]
      resources :tasks,       only:["index","show"]
      resources :entities,    only:["index","show"]
  end

  namespace :admin do
      resources :users
      get "select", to: "actions#select"
      get "logs",   to: "actions#log"
  end

  namespace :committee do
      get "start",  to: "actions#start"
      put "upload", to: "actions#upload"
      get "status", to: "actions#status"
      get "apagatudo", to: "actions#apagatudo"
      get "reset", to: "actions#reset", as: "reset_departments"
      get "submit", to: "actions#submit"
      get "statusdept", to: "departments#status"
      get "join_all", to: "actions#join_all_same_klasses"

      put "update_klasses", to: "actions#update_klasses"

      get "teste", to: "actions#teste"

      resources :blocks
      resources :courses
      resources :departments
      get "departments/:id/delete_klasses", to: "departments#delete_klasses"

      resources :klasses
      resources :professors
      resources :rooms
      resources :room_types
      resources :seasons
      resources :sectors
      resources :schedules
      resources :subjects
  end

  namespace :secdep do
      root to:"actions#root"

      resources :assistance
      resources :blocks
      resources :rooms
      resources :subjects
      resources :schedules
      resources :klasses
      resources :professors
      resources :equivalences
      resources :courses

      get 'changeroom',       to: 'actions#changeroom'
      get 'klassprof',        to: 'actions#klassprof'
      get 'operation',        to: 'actions#operation'
      get 'open',             to: 'actions#open'
      get 'close',            to: 'actions#close'
      get 'setrequireroom',   to: 'actions#setRequireRoom'
      get 'statistics',       to: 'actions#statistics'

      put 'join',             to: 'klasses#join'
      get 'addschedule',      to: 'klasses#addSchedule'

      match 'jt_subjects',    to: 'subjects#index', via: [:get, :post]
  end

  namespace :secsector do
      resources :appointments, only:["index", "edit", "destroy"]
      resources :blocks
      resources :departments
      resources :klasses
      resources :rooms
      resources :schedules

      get '/changeroom',     to: 'actions#changeroom'
      get 'appointments/query', to:'appointments#query'
      get 'appointments/:id/appointments/query', to:'appointments#query'
      
      get 'addschedule',      to: 'klasses#addSchedule'
  end


  namespace :ensalador do
      root to: 'actions#index'
      post 'execute',   to: 'actions#execute'
      get 'data',       to: 'actions#data'
      get 'result',     to: 'actions#result', as: "result"
  end

  namespace :management do
    root to: 'welcome#index'
  end


  root to: 'welcome#show'

  get '/welcome/show', to:'welcome#show', as:'welcome_show'
  get '/welcome/query', to:'welcome#query', as:'welcome_query'

  get '/search', to: 'search#show', as: "search"


  get '/search/autocomplete/:query', to: 'search#autocomplete', as: "search_autocomplete"


  get '/class/:id', to:  'klasses#show', as: "class"
  get '/course/:id', to: 'courses#show', as: "course"
  get '/room/:id', to:   'rooms#show', as: "room"

  get '/pudim', to: redirect('http://www.pudim.com.br'), as: 'pudim'

end
