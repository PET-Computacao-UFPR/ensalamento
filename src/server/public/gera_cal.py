#!/usr/bin/python
# coding: utf-8

import numpy as np
import sys
#import subprocess
#import os
import json

import calendar



# *********************** Main program ************************ #
if ( len(sys.argv) < 2 ):
    print("python gera_cal.py ANO")
    sys.exit()

ano = sys.argv[1]
ano_n = int(sys.argv[1])

out_file = "calendar/out_" + ano + ".json"

#days = [ "dom", "seg", "ter", "qua", "qui", "sex", "sab" ]
#for day in days:
#    print day.decode('utf-8').encode('utf-8')

calen = {}
calendar.setfirstweekday(calendar.SUNDAY)

cal = calendar.monthcalendar( (ano_n-1), 12)
prev = cal[ len(cal)-1 ]

k = 0
for i in range(1, 13):
    cal = calendar.monthcalendar(ano_n, i)

    for j in range( len(cal) ):
        m = i
        if( cal[j][0] != 0 ):
            k += 1
        else:
            l = 0
            while ( cal[j][l] == 0 ):
                cal[j][l] = prev[l]
                l += 1
            m -= 1

        calen[k] = { "week": k }
        calen[k].update({ "day": cal[j][0] })
        calen[k].update({ "month": m })
        #for l in range(0, len(days) ):
        #    calen[k].update({ days[l]: cal[j][l] })

        prev = cal[j]

f_out = open( out_file, "w" )

conca = "["
f_out.write(conca)

for keys,values in calen.items():

    if ( keys == calen.keys()[-1] ):
        conca = str(json.dumps(values))
    else:
        conca = str(json.dumps(values)) + ','

    f_out.write(conca)

conca = "]"
f_out.write(conca)

f_out.close
