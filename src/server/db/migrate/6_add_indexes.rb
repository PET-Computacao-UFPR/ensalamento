class AddIndexes < ActiveRecord::Migration
  def change
    add_index :blocks,     :code, unique: false
    add_index :professors, :code, unique: false
    add_index :rooms,      :code, unique: false
    add_index :room_types, :code, unique: false
    add_index :sectors,    :code, unique: false
    add_index :subjects,   :code, unique: false
  end
end
