class AddFieldsToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :description,     :text
    add_column :subjects, :credits,         :integer, default: 0
    add_column :subjects, :hour_theorical,  :integer, default: 0
    add_column :subjects, :hour_practical,  :integer, default: 0
    add_column :subjects, :hour_internship, :integer, default: 0
    add_column :subjects, :hour_total,      :integer, default: 0
  end
end
