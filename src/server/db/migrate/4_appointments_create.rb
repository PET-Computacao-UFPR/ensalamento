class AppointmentsCreate < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string  :title
      t.integer :week
      t.date    :day
      t.time    :start_at
      t.time    :end_at
      t.string  :allDay
      t.string  :requester
      t.string  :comment
      t.boolean :was_accepted
      t.timestamps null: false
    end

    #create_table :weeks, id: false do |t|
    #  t.date    :ini, primary_key: true
    #end

    #add_reference :appointments, :week, index: true, foreign_key: true
    add_reference :appointments, :room, index: true, foreign_key: true
    add_reference :appointments, :sector, index: true, foreign_key: true
  end
end
