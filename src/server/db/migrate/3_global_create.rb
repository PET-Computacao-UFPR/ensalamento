class GlobalCreate < ActiveRecord::Migration
  def change
    add_reference :klasses,      :subject,    index: true, foreign_key: true, on_delete: :cascade
    add_reference :assistances,  :subject,    index: true, foreign_key: true, on_delete: :cascade
    add_reference :schedules,    :room,       index: true, foreign_key: true
    add_reference :users,        :department, index: true, foreign_key: true
    add_reference :users,        :sector,     index: true, foreign_key: true
    add_reference :klasses,      :professor,  index: true, foreign_key: true
  end
end
