class UserCreate < ActiveRecord::Migration
  def change

    #create_table :roles do |t|
    #  t.string :name
    #  t.string :agent_type
    #  t.integer :agent_id
    #  t.timestamps null: false
    #end
    #add_index :roles, [:agent_type, :agent_id]

    create_table(:users) do |t|
      ## Database authenticatable
      t.string  :email,              null: false, default: ""
      t.string  :encrypted_password, null: false, default: ""
      t.boolean :eh_admin,           null: false, default: 'f'
      t.boolean :eh_committee,       null: false, default: 'f'

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at
      ## Rememberable
      t.datetime :remember_created_at
      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable
      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at
      t.timestamps null: false
    end

    #create_table :roles_users, id: false do |t|
    #    t.references :roles, :users
    #end


    create_table(:logs) do |t|
      t.string :text
    end

    # Relations
    #add_reference :users,        :role,       index: true, foreign_key: true
    add_reference :logs,         :user,       index: true, foreign_key: true

    # Index
    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
  end
end
