class SemesterCreate < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.time :ini
      t.time :end
      t.string :category
      t.boolean :requireroom, default: 't'
      t.integer :weekday
      t.timestamps null: false
    end

    create_table :klasses do |t|
      t.string :code
      t.integer :vacancy
      t.boolean :requireroom
      t.boolean :is_join
      t.timestamps null: false
    end

    create_table :assistances do |t|
      t.string :people
    end

    create_table :seasons do |t|
      t.string :code
      t.date   :ini
      t.date   :fim
    end

    # Relations
    add_reference :klasses,      :klass,       index: true, foreign_key: true
    add_reference :schedules,    :klass,       index: true, foreign_key: true, on_delete: :cascade
    add_reference :schedules,    :assistance,  index: true, foreign_key: true, on_delete: :cascade
    add_reference :klasses,      :season,      index: true, foreign_key: true, on_delete: :cascade
  end
end
