class UniversityCreate < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.string :code
      t.string :name
      t.float :latitude
      t.float :longitude
      t.timestamps null: false
    end

    create_table :courses do |t|
      t.string :code
      t.string :name
      t.timestamps null: false
    end

    create_table :departments do |t|
      t.string  :code
      t.string  :name
      t.boolean :is_modifying
      t.timestamps null: false
    end

    create_table :courses_subjects, id: false do |t|
        t.references :course, :subject
    end

    create_table :courses_departments, id: false do |t|
        t.references :course, :department
    end

    create_table :professors do |t|
       t.string :code
       t.string :name
       t.string :email
       t.string :web
    end

    create_table :rooms do |t|
      t.string :code
      t.integer :size
      t.string :sector
      t.integer :floor
      t.string :information
      t.boolean :can_use,        default: true
      t.timestamps null: false
    end

    create_table :room_types do |t|
      t.string :code
      t.string :name
      t.timestamps null: false
    end

    create_table :sectors do |t|
      t.string :code
      t.string :name
      t.string :web
      t.string :email
      t.timestamps null: false
    end

    create_table :subjects do |t|
      t.string :code
      t.string :name
      t.boolean :is_equivalence
      t.timestamps null: false
    end

    # Relations
    add_reference :departments,  :sector,     index: true, foreign_key: true
    add_reference :blocks,       :sector,     index: true, foreign_key: true
    add_reference :subjects,     :department, index: true, foreign_key: true
    add_reference :professors,   :department, index: true, foreign_key: true
    add_reference :rooms,        :block,      index: true, foreign_key: true
    add_reference :departments,  :block,      index: true, foreign_key: true
    add_reference :courses,      :block,      index: true, foreign_key: true
    add_reference :klasses,      :course,     index: true, foreign_key: true
    add_reference :subjects,     :room_type,  index: true, foreign_key: true
    add_reference :rooms,        :room_type,  index: true, foreign_key: true
    add_reference :subjects,     :subject,    index: true, foreign_key: true
  end
end
