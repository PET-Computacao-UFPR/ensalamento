class Admin::UsersController < ManagementController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
      @users = User.all.order("LOWER(email)").page(params[:page]).per(75)
  end

  def show; end

  def new; @user = User.new; end

  def edit; end

  def create
    @user = User.new(user_params)
    respond_to do |format|
        if @user.save
            format.html { redirect_to admin_users_url, notice: 'Usuario criado com sucesso!' }
            format.json { render :show, status: :created, location: @user }
        else
            format.html { render :new }
            format.json { render json: @user.errors, status: :unprocessable_entity }
        end
    end
  end


  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to admin_users_url, notice: 'Usuário atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: 'Usuário deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_user; @user = User.find(params[:id]); end

    def user_params
        params.require(:user).permit(:email, :password, :secretary, :committee, :department_id, :sector_id, :eh_admin, :eh_committee)
    end
end
