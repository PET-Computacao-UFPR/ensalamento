class Admin::BlocksController < ManagementController

  def index
    @blocks = Block.all.order("LOWER(code)")
    @distances = Block.distances
  end

  def show
  end

  def new
    @block = Block.new
  end

  def edit
  end

  def create    
    @block = Block.new(block_params)
    @block.department = Department.find( block_params[:department_id] )

    respond_to do |format|
      if @block.save
        format.html { redirect_to management_block_path(@block), notice: 'Bloco criado com sucesso.' }
        format.json { render :show, status: :created, location: @block }
      else
        format.html { render :new }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @block.update(block_params)
        format.html { redirect_to management_block_path(@block), notice: 'Bloco atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @block }
      else
        format.html { render :edit }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @block.destroy
    respond_to do |format|
      format.html { redirect_to management_blocks_url, notice: 'Bloco deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_block; @block = Block.find(params[:id]); end
end
