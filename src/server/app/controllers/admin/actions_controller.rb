class Admin::ActionsController < ManagementController

  def select
      if params["type"] == "department"
          cookies["department_id"] = params["id"]
      elsif params["type"] == "sector"
          cookies["sector_id"] = params["id"]
      else
          render text: "error"
      end
      render text: "ok"
  end

  def log
    @logs = Log.all.order("id DESC")
    respond_to do |format|
        format.html {@logs = @logs.page(params[:page]).per(200)}
        format.json {render :json => @logs}
    end

  end

end
