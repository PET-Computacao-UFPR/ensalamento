class SearchController < ApplicationController
  def autocomplete
    klass = Klass.search(params[:query], fields: search_fields("klass"), limit: 5).map do |klass|
      {search: "klass-"+klass.id.to_s, value: klass.discipline.code+" "+klass.name+" ("+klass.discipline.name+")"}
    end
    room = Room.search(params[:query], autocomplete: true, limit: 5).map do |room|
      {search: "room-"+room.id.to_s, value: room.code+" ("+room.block+")"}
    end
    course = Course.search(params[:query], fields: search_fields("course"), limit: 5).map do |course|
      {search: "course-"+course.id.to_s, value: course.name+" ("+course.code+")"}
    end

    results = klass + room + course

    render json: results
  end

  def show

    klass = Klass.search params[:query], fields: search_fields("klass")
    room = Room.search params[:query], fields: search_fields("room")
    course = Course.search params[:query], fields: search_fields("course")

    # group results with score
    @results = []
    [klass, room, course].each do |model|
      model.each_with_index do |result, index|
        @results << [model.hits[index]["_score"], result]
      end
    end

    # order by highest score
    @results.sort_by!{|r| r[0]}.reverse!
    # map only objects, remove score
    @results.map!{|r| r[1]}
  end

  private

  def search_fields(model)
    case model
    when "klass" then ['discipline_code', {'discipline_name': :text_middle}, 'name', {'professor': :text_middle}]
    when "room" then ['code', 'block', 'sector']
    when "course" then [{'name': :text_middle}, 'code']
    else []
    end
  end
end
