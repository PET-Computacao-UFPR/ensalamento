
class Management::EnsaladorController < ManagementController
  before_action :set_user, only: [:update, :destroy]
  skip_before_filter :verify_authenticity_token


  def index
    @lines = {}
    klasses = Klass.allFathers

    klasses.each do |klass|
        roomtype = klass.subject.room_type
        if roomtype.present? and klass.requireroom?
            without_room = klass.schedules.where(["room_id is null"])
            if @lines.has_key?(roomtype.id)
                @lines[ roomtype.id ]["qtde"] += klass.schedules.size
                @lines[ roomtype.id ]["no_room"] += without_room.size()
            else
                line = {}
                line["qtde"]    = klass.schedules.size
                line["no_room"] = without_room.size()
                line["name"]    = roomtype.name
                @lines[ roomtype.id ] = line
            end
        end
    end
  end



  def exec
      # generate input
      val = params[:val]
      dist = val[:dist]
      free = val[:free]


      #department = Department.find(6)
      #klasses = []
      #_klasses = Klass.allRequiredRoom
      #_klasses.each do |klass|
      #    if klass.code == "AN1"
      #        klasses.push(klass)
      #    end
      # end
      #@rooms = department.block.rooms.where("size>0 and can_use='t'")

      klasses = Klass.allRequiredRoom
      @rooms  = Room.getValidRooms
      data2ensalamento_ti @rooms, klasses, "public/ensalador/input1", 1

      # execute ensalador
      input  = "public/ensalador/input1"
      output = "public/ensalador/output1"
      cmd = "ensalador2 '"+input+"' "+dist+" "+free+" > '"+output+"'"
      if !system( cmd )
          exitstatus = $?.exitstatus
          msg = "Erro desconhecido"
          if exitstatus == 127
              msg = "Comando ensalador não foi encontrado"
          else
              msg = output + " <<\n" + File.read(output)
          end
          render json: msg
          return
      end

      # Generate the Statistics
      @stats = []
      file = "public/ensalador/output1"
      @stats.push Stat.new("MC+CB", file)

  end



  def store
      Schedule.update_all(room_id:nil)
      contents = File.read('public/ensalador/output1')
      lines = contents.split("\n")
      line  = Line.new
      lines.each do |text|
            line.load text
            if line.isOk? && line.free >= 0
                line.schedule.room = line.room
                line.schedule.save
            end
      end
      redirect_to management_klasses_path, notice:"Publicado!"
  end


  def noroom
  end


  def klasses
      @roomscheds = []
      file = "public/ensalador/output1"
      contents = File.read(file)
      line = Line.new
      lines = contents.split("\n")
      lines.each do |text|
          line.load text
          @roomscheds.push( RoomSched.new(line) )
      end
      #@roomscheds.sort_by {|obj| obj.subject_code}
      @roomscheds.sort! { |a, b|  a.hour <=> b.hour }
  end

  private
    #Use callbacks to share common setup or constraints before_actiontween actions.
    def set_user
      @usuarioLogado = User.find(params[:id])
    end

    #Never trust parameters from the scary internet, only allow the white list through.
    def spreadsheet_params
      params.permit(:file)
    end



    def data2ensalamento_ti(rooms, klasses, filename, type)
        text = ""; log = ""; qtde=0;
        rooms.each do |room|
            if type != 0
                text += room.toTiObj+"\n"
            else
                text += room.toText+"\n"
            end
        end
        if type == 0
            text = rooms.size.to_s+"\n"+text
        end

        File.open(filename, 'w') { |file| file.write(text) }
        text = ""; qtde=0; log=""; log_qtde=0
        klasses.each do |klass|
            room_type_id = klass.subject.room_type_id
            if (room_type_id.present? and room_type_id>=1&&room_type_id<=4)
                klass.schedules.where(["requireroom='t'"]).each do |sched|
                    if type != 0
                        text += sched.toTiObj+"\n"
                    else
                        if sched.isNormal
                            text += sched.toText+"\n"
                            qtde += 1
                        else
                            log += sched.toTiObj+"\n"
                        end
                    end
                end
            end
        end
        if type == 0
            text = qtde.to_s+"\n"+text
        end
        File.open(filename, 'a+') { |file| file.write(text) }
        File.open("public/log.txt", 'w') { |file| file.write(log) }
    end


end
