class Management::CoursesController < ManagementController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index
    $usuarioLogado = current_user
    respond_to do |format|
        format.html {
            @courses = Course.all.page(params[:page]).per(75)
        }
        format.json {
            @courses = Course.all
            render :json => @courses
        }
    end
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
      @matrix = Matrix.new
      @matrix.loadCourse @course
  end

  # GET /courses/new
  def new
    @course = Course.new
    @departments = Department.all
  end

  # GET /courses/1/edit
  def edit

  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)
    #department = Department.find( course_params[:department] )
    #@course.department = department

    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Curso criado com sucesso!' }
        format.json { render :show, status: :created, location: @course }
      else
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to management_courses_url, notice: 'Curso atualizado com sucesso!' }
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to management_courses_url, notice: 'Curso deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :code, :department_id, :block_id)
    end
    def permission_secretary
      if current_user.role?('secretary')
        render :file => 'public/422.html'
      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'
      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'
      end
    end
end
