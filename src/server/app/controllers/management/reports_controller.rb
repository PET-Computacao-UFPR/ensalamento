class Management::ReportsController < ManagementController
  def index
  end


  def qtde
      text = ""

      all_ini  = Array.new(24){ Array.new(8,0) }
      all_dur  = Array.new(24){ Array.new(8,0) }
      all_size = Array.new(24){ Array.new(8,0) }

      Department.all.each do |department|
          text_extra = "aaaa"
          size = 0
          text += department.name + "\n"
          matrix_ini  = Array.new(24){ Array.new(8,0) }
          matrix_dur  = Array.new(24){ Array.new(8,0) }
          matrix_size = Array.new(24){ Array.new(8,0) }

          department.subjects.all.each do |subject|
              subject.klasses.all.each do |klass|
                  klass.schedules.all.each do |sched|
                      day = sched.weekday
                      s_ini = sched.ini.hour
                      s_end = sched.end.hour

                      if ( sched.ini.min != 30 )
                          text_extra += subject.name
                          text_extra += "," + sched.ini.hour.to_s + ":" + sched.ini.min.to_s
                          text_extra += "," + sched.end.hour.to_s + ":" + sched.end.min.to_s + ",\n"
                      end

                      if ( sched.ini.min < 30 )
                          s_ini -= 1
                      end

                      if ( sched.end.min > 30 )
                          s_end += 1
                      end

                      all_ini[s_ini][day]     += 1
                      all_size[s_ini][day]    += klass.vacancy
                      matrix_ini[s_ini][day]  += 1
                      matrix_size[s_ini][day] += klass.vacancy

                      for i in s_ini+1..s_end-1
                          all_ini[i][day]     += 1
                          all_size[i][day]    += klass.vacancy
                          matrix_ini[i][day]  += 1
                          matrix_size[i][day] += klass.vacancy
                      end
                  end
                  size += klass.schedules.size
              end
          end
          text += "Qtde Total de Turmas," + size.to_s + "\n"

          text += calendar2cvs(matrix_ini, matrix_size)
          text += "\n\n";
      end

      text += "Todos os Departamento\n"
      text += calendar2cvs(all_ini, all_size)

      File.open("public/report_qtde.csv", 'w') { |file| file.write(text) }
      send_file("public/report_qtde.csv", disposition: 'attachment')
  end




  def code
      text = ""
      all = Array.new(24){ Array.new(8) }

      for hour in 7..all.size-1
          for day in 2..7
              all[hour][day] = Hash.new
          end
      end

      Department.all.each do |department|
          text += department.name + "\n"

          matrix = Array.new(24){ Array.new(8) }

          for hour in 7..matrix.size-1
              for day in 2..7
                  matrix[hour][day] = Hash.new
              end
          end

          department.subjects.all.each do |subject|
              subject.klasses.all.each do |klass|
                  klass.schedules.all.each do |sched|
                      day = sched.weekday
                      s_ini = sched.ini.hour
                      s_end = sched.end.hour

                      if ( sched.ini.min < 30 )
                          s_ini -= 1
                      end
                      if ( sched.end.min > 30 )
                          s_end += 1
                      end

                      increment_by_code(all,s_ini,day,subject.code)
                      increment_by_code(matrix,s_ini,day,subject.code)
                      for i in s_ini+1..s_end-1
                          increment_by_code(all,i,day,subject.code)
                          increment_by_code(matrix,i,day,subject.code)
                      end
                  end

              end
          end

          text += calendar2cvs_name(matrix)
          text += "\n\n";
      end

      text += "Todos os Departamentos\n"
      text += calendar2cvs_name(all)
      File.open("public/report_code.csv", 'w') { |file| file.write(text) }
      send_file("public/report_code.csv", disposition: 'attachment')
  end





  def name
      text = ""
      all = Array.new(24){ Array.new(8) }

      for hour in 7..all.size-1
          for day in 2..7
              all[hour][day] = Hash.new
          end
      end

      Department.all.each do |department|
          text += department.name + "\n"

          matrix = Array.new(24){ Array.new(8) }

          for hour in 7..matrix.size-1
              for day in 2..7
                  matrix[hour][day] = Hash.new
              end
          end

          department.subjects.all.each do |subject|
              subject.klasses.all.each do |klass|
                  klass.schedules.all.each do |sched|
                      day = sched.weekday
                      s_ini = sched.ini.hour
                      s_end = sched.end.hour

                      if ( sched.ini.min < 30 )
                          s_ini -= 1
                      end
                      if ( sched.end.min > 30 )
                          s_end += 1
                      end

                      increment_by_code(all,s_ini,day,subject.name)
                      increment_by_code(matrix,s_ini,day,subject.name)
                      for i in s_ini+1..s_end-1
                          increment_by_code(all,i,day,subject.name)
                          increment_by_code(matrix,i,day,subject.name)
                      end
                  end

              end
          end

          text += calendar2cvs_name(matrix)
          text += "\n\n";
      end

      text += "Todos os Departamentos\n"
      text += calendar2cvs_name(all)
      File.open("public/report_name.csv", 'w') { |file| file.write(text) }
      send_file("public/report_name.csv", disposition: 'attachment')
  end









  def course_qtde
      text = ""

      all_ini  = Array.new(24){ Array.new(8,0) }
      all_dur  = Array.new(24){ Array.new(8,0) }
      all_size = Array.new(24){ Array.new(8,0) }

      Course.all.each do |course|
          text += course.code
          if not course.name.blank?
              text += " - " + course.name
          end
          text += "\n"
          matrix_ini  = Array.new(24){ Array.new(8,0) }
          matrix_dur  = Array.new(24){ Array.new(8,0) }
          matrix_size = Array.new(24){ Array.new(8,0) }

          course.klasses.all.each do |klass|
              klass.schedules.all.each do |sched|
                  day = sched.weekday
                  s_ini = sched.ini.hour
                  s_end = sched.end.hour

                  if ( sched.ini.min < 30 )
                      s_ini -= 1
                  end

                  if ( sched.end.min > 30 )
                      s_end += 1
                  end

                  all_ini[s_ini][day]     += 1
                  all_size[s_ini][day]    += klass.vacancy
                  matrix_ini[s_ini][day]  += 1
                  matrix_size[s_ini][day] += klass.vacancy

                  for i in s_ini+1..s_end-1
                      all_ini[i][day]     += 1
                      all_size[i][day]    += klass.vacancy
                      matrix_ini[i][day]  += 1
                      matrix_size[i][day] += klass.vacancy
                  end
              end
          end

          text += calendar2cvs(matrix_ini, matrix_size)
          text += "\n\n";
      end

      text += "Todos os Cursos\n"
      text += calendar2cvs(all_ini, all_size)

      File.open("public/report_course_qtde.csv", 'w') { |file| file.write(text) }
      send_file("public/report_course_qtde.csv", disposition: 'attachment')
  end




  def course_code
      text = ""
      all = Array.new(24){ Array.new(8) }

      for hour in 7..all.size-1
          for day in 2..7
              all[hour][day] = Hash.new
          end
      end

      Course.all.each do |course|
          text += course.code
          if not course.name.blank?
              text += " - " + course.name
          end
          text += "\n"

          matrix = Array.new(24){ Array.new(8) }

          for hour in 7..matrix.size-1
              for day in 2..7
                  matrix[hour][day] = Hash.new
              end
          end

          course.klasses.all.each do |klass|
              klass.schedules.all.each do |sched|
                  day = sched.weekday
                  s_ini = sched.ini.hour
                  s_end = sched.end.hour
                  if ( sched.ini.min < 30 )
                      s_ini -= 1
                  end
                  if ( sched.end.min > 30 )
                      s_end += 1
                  end

                  subject_code = klass.subject.code
                  increment_by_code(all,s_ini,day,subject_code)
                  increment_by_code(matrix,s_ini,day,subject_code)
                  for i in s_ini+1..s_end-1
                      increment_by_code(all,i,day,subject_code)
                      increment_by_code(matrix,i,day,subject_code)
                  end
              end
          end


          text += calendar2cvs_name(matrix)
          text += "\n\n";
      end

      text += "Todos os Cursos\n"
      text += calendar2cvs_name(all)
      File.open("public/report_course_code.csv", 'w') { |file| file.write(text) }
      send_file("public/report_course_code.csv", disposition: 'attachment')
  end





  def course_name
      text = ""
      all = Array.new(24){ Array.new(8) }

      for hour in 7..all.size-1
          for day in 2..7
              all[hour][day] = Hash.new
          end
      end

      Course.all.each do |course|
          text += course.code
          if not course.name.blank?
              text += " - " + course.name
          end
          text += "\n"

          matrix = Array.new(24){ Array.new(8) }

          for hour in 7..matrix.size-1
              for day in 2..7
                  matrix[hour][day] = Hash.new
              end
          end


          course.klasses.all.each do |klass|
              klass.schedules.all.each do |sched|
                  day = sched.weekday
                  s_ini = sched.ini.hour
                  s_end = sched.end.hour

                  if ( sched.ini.min < 30 )
                      s_ini -= 1
                  end
                  if ( sched.end.min > 30 )
                      s_end += 1
                  end

                  subject_name = klass.subject.name
                  increment_by_code(all,s_ini,day,subject_name)
                  increment_by_code(matrix,s_ini,day,subject_name)
                  for i in s_ini+1..s_end-1
                      increment_by_code(all,i,day,subject_name)
                      increment_by_code(matrix,i,day,subject_name)
                  end
              end

          end

          text += calendar2cvs_name(matrix)
          text += "\n\n";
      end

      text += "Todos os Cursos\n"
      text += calendar2cvs_name(all)
      File.open("public/report_course_name.csv", 'w') { |file| file.write(text) }
      send_file("public/report_course_name.csv", disposition: 'attachment')
  end













  def data
      text = ""
      #Course.all.each do |course|
      #    text += "Course{";
      #    text += "id="+course.id.to_s+";";
      #    text += "code='"+course.code+"';";
      #    text += "lat='"+course.latitude.to_s+"';";
      #    text += "log='"+course.longitude.to_s+"';";
      #    text += "}\n";
      #end

      Block.all.each do |block|
        text += "Block{\n";
        text += "\tcode='"+block.code+"';\n";
        text += "\tlat='"+block.latitude.to_s+"';\n";
        text += "\tlog='"+block.longitude.to_s+"';\n";
        block.classrooms.each do |room|
            if room.size > 0
                text += "\tRoom{";
                text += "id="+room.id.to_s+";";
                text += "code='"+room.code+"';";
                text += "size="+room.size.to_s+";";
                text += "}\n";
            end
        end
        text += "}\n";
      end

      Schedule.all.each do |sched |
          day = sched.weekday
          s_ini = sched.ini.hour
          s_end = sched.end.hour
          if ( sched.ini.min < 30 )
              s_ini -= 1
          end
          if ( sched.end.min > 30 )
              s_end += 1
          end

          if sched.klass.vacancy > 1
              text += "Schedule{"
              text += "id="+sched.id.to_s+";"
              text += "day="+day.to_s+";"
              text += "ini="+s_ini.to_s+";"
              text += "end="+s_end.to_s+";"
              text += "course="+sched.klass.course.id.to_s+";"
              text += "size="+sched.klass.vacancy.to_s+";"
              text += "lat='"+sched.klass.course.latitude.to_s+"';";
              text += "log='"+sched.klass.course.longitude.to_s+"';";
              text += "}\n"
          end
      end

      File.open("public/data.ti", 'w') { |file| file.write(text) }
      render text: text
  end




end


def calendar2cvs (matrix_ini, matrix_size)
    max = 0
    text = ""
    for hour in 7..matrix_ini.size-1
        cols = matrix_ini[hour].size
        text += hour.to_s + ":30,"
        for day in 2..cols-1
            text += matrix_ini[hour][day].to_s + " ("
            if matrix_ini[hour][day] > 0
                text += matrix_size[hour][day].to_s
            end
            text += "),"
            if matrix_ini[hour][day] > max
                max = matrix_ini[hour][day]
            end
        end
        text += "\n";
    end
    return "Maximo de Turmas por Horario, " + max.to_s + "\n" + text
end


def calendar2cvs_name(matrix_ini)
    text = ""
    for day in 2..7
        if day < 7
            text += day.to_s + "º feira\n"
        else
            text += "Sabado\n"
        end

        for hour in 7..matrix_ini.size-1
            text += hour.to_s + ":30,"

            size = 0
            matrix_ini[hour][day].each do |key,value|
                size += value
            end
            text += size.to_s+","

            matrix_ini[hour][day].each do |key,value|
                text += key+"("+value.to_s+"),"
            end
            text += "\n";
        end
        text += "\n";
    end
    return text
end



def increment_by_code(matrix, hour, day, code)
    if not matrix[hour][day].has_key?(code)
        matrix[hour][day][code] = 0
    end
    matrix[hour][day][code] += 1
end
