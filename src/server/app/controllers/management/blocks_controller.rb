class Management::BlocksController < ManagementController
  before_action :set_block, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :destroy]

  # GET /disciplines
  # GET /disciplines.json
  def index
    $usuarioLogado = current_user
    respond_to do |format|
        format.html {
            @blocks = Block.all
        }
        format.json {
            @blocks = Block.all
            render :json => @blocks
        }
    end

    @distances = []
    for i in 0..@blocks.size-1
        line = []
        lat1 = @blocks[i].latitude
        log1 = @blocks[i].longitude
        for j in 0..@blocks.size-1
            lat2 = @blocks[j].latitude
            log2 = @blocks[j].longitude
            lat2 = (lat1 - lat2) * (lat1 - lat2)
            log2 = (log1 - log2) * (log1 - log2)
            dist = Math.sqrt(lat2+log2) * 100000.0
            if ( dist < 10000.0 )
                line.push dist.to_i
            else
                line.push "*"
            end
        end
        @distances.push line
    end
  end

  # GET /disciplines/1
  # GET /disciplines/1.json
  def show
  end

  # GET /disciplines/new
  def new
    @block = Block.new
  end

  # GET /disciplines/1/edit
  def edit
  end

  # POST /disciplines
  # POST /disciplines.json
  def create
    @block = Block.new(block_params)
    @block.department = Department.find( block_params[:department_id] )

    respond_to do |format|
      if @block.save
        format.html { redirect_to management_block_path(@block), notice: 'Bloco criado com sucesso.' }
        format.json { render :show, status: :created, location: @block }
      else
        format.html { render :new }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
    respond_to do |format|
      if @block.update(block_params)
        format.html { redirect_to management_block_path(@block), notice: 'Bloco atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @block }
      else
        format.html { render :edit }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /disciplines/1
  # DELETE /disciplines/1.json
  def destroy
    @block.destroy
    respond_to do |format|
      format.html { redirect_to management_blocks_url, notice: 'Bloco deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_block
      @block = Block.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def block_params
      params.require(:block).permit(:code,:sector_id, :latitude, :longitude)
    end
    def permission_secretary
      if (current_user.role?('secretary'))
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'

      end
    end
end
