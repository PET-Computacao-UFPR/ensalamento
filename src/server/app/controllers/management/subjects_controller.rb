class Management::SubjectsController < ManagementController
  before_action :set_subject, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only: [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :edit, :update, :destroy]

  # GET /disciplines
  # GET /disciplines.json
  def index
    $usuarioLogado = current_user
    respond_to do |format|
        format.html {
            @subjects = Subject.allFathers.order(:code).page(params[:page]).per(75)
        }
        format.json {
            @subjects = Subject.allFathers.order(:code)
            render :json => @subjects
        }
    end
  end

  # GET /disciplines/1
  # GET /disciplines/1.json
  def show
      if @subject.is_equivalence
          @klasses = []
          @subject.subjects.each do |son|
              @klasses += son.getValidKlasses
          end
      else
          @klasses = @subject.getValidKlasses
      end
  end

  # GET /disciplines/new
  def new
    @subject = Subject.new
  end

  # GET /disciplines/1/edit
  def edit

  end

  # POST /disciplines
  # POST /disciplines.json
  def create
    @subject = Subject.new(subject_params)
    @subject.room_type = RoomType.find( subject_params[:room_type_id] )

    respond_to do |format|
      if @subject.save
        format.html { redirect_to management_subjects_path, notice: 'Disciplina criada com sucesso.' }
        format.json { render :show, status: :created, location: @subject }
      else
        format.html { render :new }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
      room_type_id = subject_params[:room_type_id]
      requireroom = (subject_params[:room_type_id].blank?) ? false : true;

      @subject.klasses.each do |klass|
          klass.schedules.update_all({requireroom:requireroom})
      end

      respond_to do |format|
      if @subject.update(subject_params)
        format.html { redirect_to management_spreadsheets_subjects_path, notice: 'Disciplina atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @subject }
      else
        format.html { render :edit }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /disciplines/1
  # DELETE /disciplines/1.json
  def destroy
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to management_subjects_url, notice: 'Disciplina deletada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_params
      params.require(:subject).permit(:name, :code, :room_type_id, :requireroom)
    end

    def permission_secretary
      if current_user.role?('secretary') && current_user.department != @subject.department
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'
      end
    end
end
