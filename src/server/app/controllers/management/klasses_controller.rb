class Management::KlassesController < ManagementController
  before_action :set_klass, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :edit, :update, :destroy]

  # GET /klasses
  # GET /klasses.json
  def index
    $usuarioLogado = current_user
    respond_to do |format|
        format.html {
            @klasses = Klass.allFathers.order(:subject_id).page(params[:page]).per(75)
        }
        format.json {
            @klasses = Klass.allFathers.order(:subject_id)
            render :json => @klasses
        }
    end
  end

  # GET /klasses/1
  # GET /klasses/1.json
  def show
  end

  # GET /klasses/new
  def new
      @klass = Klass.new

      if ( params[:subject_id].present? )
        @klass.subject = Subject.find( params[:subject_id] )
      end
  end

  # GET /klasses/1/edit
  def edit
  end

  # POST /klasses
  # POST /klasses.json
  def create
    @klass = Klass.new(klass_params)
    @klass.subject = Subject.find( klass_params[:subject_id] )

    respond_to do |format|
      if @klass.save

        format.html { redirect_to management_klass_path( @klass), notice: 'Classe criada com sucesso.' }
        format.json { render :show, status: :created, location: @klass }
      else
        format.html { render :new }
        format.json { render json: @klass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /klasses/1
  # PATCH/PUT /klasses/1.json
  def update
    respond_to do |format|
      if @klass.update(klass_params)
        format.html { redirect_to :back, notice: 'Classe atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @klass }
      else
        format.html { render :edit }
        format.json { render json: @klass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /klasses/1
  # DELETE /klasses/1.json
  def destroy
    @klass.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Classe deletada com sucesso.' }
      format.json { head :no_content }
    end
  end


  def delAll
      if Klass.destroy_all
          redirect_to management_klasses_url, notice: 'Todas as Turmas foram apagadas'
      else
          redirect_to management_klasses_url, alert: @klass.errors
      end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_klass
      @klass = Klass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def klass_params
      params.require(:klass).permit(:code, :vacancy, :room, :subject_id, :room_id, :course_id)
    end

    def permission_secretary
      if current_user.role?('secretary')
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'

      end
    end
end
