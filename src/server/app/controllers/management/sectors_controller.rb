require 'matrix'

class Management::SectorsController < ManagementController
  before_action :set_sector, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :edit, :update, :destroy]

  # GET /departments
  # GET /departments.json
  def index
    $usuarioLogado = current_user
    respond_to do |format|
        format.html {
            @sectors = Sector.all.page(params[:page])
        }
        format.json {
            @sectors = Sector.all.page(params[:page])
            render :json => @sectors
        }
    end

  end

  # GET /departments/1
  # GET /departments/1.json
  def show
    @departments  = @sector.departments


  end

  # GET /departments/new
  def new
    @sector = Sector.new
  end

  # GET /departments/1/edit
  def edit
  end

  # POST /departments
  # POST /departments.json
  def create
    @sector = Sector.new(sector_params)

    respond_to do |format|
      if @sector.save
        format.html { redirect_to management_sector_path(@sector), notice: 'Setor criado com sucesso!' }
        format.json { render :show, status: :created, location: @sector }
      else
        format.html { render :new }
        format.json { render json: @sector.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /departments/1
  # PATCH/PUT /departments/1.json
  def update
    respond_to do |format|
      if @sector.update(sector_params)
        format.html { redirect_to management_sector_path(@sector), notice: 'Setor atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @sector }
      else
        format.html { render :edit }
        format.json { render json: @sector.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @sector.destroy
    respond_to do |format|
      format.html { redirect_to management_sector_url, notice: 'Setor deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  def reset_modifying
    Sector.update_all(is_modifying: true)
    redirect_to :back
  end


  def finalize_modification
    currentuser_set_modifying false
    redirect_to :back
  end


  def reopen_modification
    currentuser_set_modifying true
    redirect_to :back
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sector
      @sector = Sector.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sector_params
      params.require(:sector).permit(:name)
    end

    def currentuser_set_modifying(bool_status)
      if current_user.present? and current_user.sector.present?
        current_user.sector.is_modifying = bool_status;
        current_user.sector.save
      end
    end
    def permission_secretary
      if current_user.role?('secretary')
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'

      end
    end
end
