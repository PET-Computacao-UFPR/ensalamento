class Management::RoomsController < ManagementController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: []

  def index
    $usuarioLogado = current_user
    #@horarios = []
    #for i in 1..7
    #  @horarios[i] = free(i)
    #end

    respond_to do |format|
        format.html {
            @rooms = Room.all.page(params[:page]).per(75)
        }
        format.json {
            @rooms = Room.all
            render :json => @rooms
        }
    end
  end

  def show

  end

  def new
    @room = Room.new

  end

  def edit

  end

  def create
    @room = Room.new(room_params)
    @room.block     =  Block.find( room_params[:block_id] )
    @room.room_type =  RoomType.find( room_params[:room_type_id] )

    respond_to do |format|
      if @room.save
        format.html { redirect_to management_rooms_path, notice: 'Sala criada com sucesso!' }
        format.json { render :show, status: :created, location: @room }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to management_rooms_path, notice: 'Sala atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy

    @room.destroy
    respond_to do |format|
      format.html { redirect_to management_rooms_url, notice: 'Sala deletada com sucesso' }
      format.json { head :no_content }
    end
  end

  def free (weekday)
    salas = Room.all
    @hash = {}
    salas.each do |sala|
      v_horarios = []
      horarios = []
      for i in 0 .. 23
        horarios[i] = 0 #livre
      end
      horarios_sala = sala.schedules #verificar
      horarios_sala.each do |horario|
        a = horario.ini.hour..horario.end.hour-1
        v_horarios << a if horario.weekday == weekday #aqui tenho um vetor com todo os horarios da sala, de acordo com o dia da semana escolhido por parametro
      end
      v_horarios.each do |h|
        h.each do |intervalo|
          horarios[intervalo] = 1 #ocupado
        end
      end
      @hash[sala.code] = horarios
    end
    @hash
  end

# a=Array.new(24,1)

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:code, :size, :block_id, :room_type_id, :information, :can_use)
    end
    def permission_secretary
      if current_user.role?('secretary')
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'

      end
    end
end
