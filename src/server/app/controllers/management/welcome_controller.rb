class Management::WelcomeController < ManagementController
  def index
  end

  def query
      res = ""
      path = params[:path]

      if path == nil
          datapkg = Department.all
          datapkg.each do |data|
              res += data.toTiObj
          end
          datapkg = Block.all
          datapkg.each do |data|
              res += data.toTiObj
          end
      else
          obj_s = path.split(":")
          name = obj_s[0]
          id   = obj_s[1]
          if name == "Department"
              obj = Department.find( id );
              obj.subjects.each do |subject|
                  res += subject.toTiObj
              end
              obj.users.each do |user|
                  res += user.toTiObj
              end
          elsif name == "Course"
              obj = Course.find( id );
          elsif name == "Subject"
              obj = Subject.find( id );
              obj.klasses.each do |klass|
                  res += klass.toTiObj
              end
          elsif name == "Block"
              obj = Block.find( id );
              obj.classrooms.each do |room|
                  res += room.toTiObj
              end
          end

      end
      #res += obj.toTiObj

      render text: res
  end
end
