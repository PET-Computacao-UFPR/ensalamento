class Management::GridController < ManagementController
  before_action :set_user, only: [:update, :destroy]
  skip_before_filter :verify_authenticity_token  
  require 'csv'
  #require 'ffi'

  # module Ensalador
  #   extend FFI::Library
  #   ffi_lib 'c'
  #   ffi_lib 'teste/ffi_test.so'
  #   #ffi_lib 'ensalador/build/assign'
  #   attach_function :main
  # end


  def show
    $usuarioLogado = current_user
  end

  def inserir
    $usuarioLogado = current_user
    @notice = ""
  end

  def uploadFile
    post = save(params[:upload])
    @notice = 'Arquivo inserido com sucesso.'
    render :inserir

  end


  def new
    @users = User.all.page(params[:page])
    $usuarioLogado = current_user

    @caminho = $usuarioLogado.spreadsheet.url.split('?')[0];

    @final = @caminho.split('/');

    @final[8] = "final.xls";

    @final = @final.join("/");


    @existeFinal = File.exists?('public'+@final);

    @final = @final + '?'+$usuarioLogado.spreadsheet.url.split('?')[1];

    $i = 0

    @arquivo = []

    while $i < @users.length

      if @users[$i].spreadsheet.url != "/spreadsheets/original/missing.png"

        begin
          @arquivoAux = Roo::Spreadsheet.open('public'+@users[$i].spreadsheet.url.split('?')[0])  
          @arquivoAux.each_with_index do |linha, x|

            @arquivo.push(linha)
            
          end
        rescue Exception => e
          
        end



      else
        
      end
      $i += 1
    end
      #@teste = Ensalador.main()
      #puts FfiCustomTest.ffi_fibonacci(9)
      #puts FfiCustomTest.ffi_pow(2, 10)

  end

  def salvarArquivoFinal
    begin

      @dados = params[:dados]

      $i = 0

      @usuarioLogado = current_user

      @caminho = 'public'+@usuarioLogado.spreadsheet.url.split('?')[0];

      @caminho = @caminho.split("/");

      @caminho[8] = "final.xls";

      @caminho = @caminho.join("/");

      


      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet

      header_format = Spreadsheet::Format.new(
        :weight => :bold,
        :horizontal_align => :center,
        :bottom => :none,
        :locked => true
      )

      sheet1.row(0).default_format = header_format


      while $i < @dados.length
        sheet1.row($i).replace(@dados[$i].split(','))
        $i += 1
      end

      book.write(@caminho)


      #primeiro passo do sistema do Edimilson: gerar as entradas para o sistema dele
      #a variável @caminho contém o caminho que está o .xls com as turmas formatadas para o ensalamento
      #system("ensalador/build/xls/gen_in.sh " + @caminho + "  public/system")

      #segundo passo do sistema do Edimilson: gerar o .xls final com o ensalamento realizado
      #system("ensalador/build/assign " + "public/system" + "  public/system")


      render :json =>{:result => "ok"}  

    rescue Exception => e
      render :json =>{:result => "erro"}  
    end  
      
  end

  def save(upload)
    directory = 'public/system'
    # create the file path
    path = File.join(directory,'Turmas.xls')
    # write the file
    File.open(path, "wb") { |f| f.write(upload['datafile'].read)}
  end


  def create
    #moderators = (params[:spreadsheet][:moderators] == "1") ? true : false
    
    if current_user.spreadsheet.blank?
      redirect_to management_grid_path, notice: "Não há arquivos para processar!"
    else
      GridWorker.perform_async(current_user.id, params[:mode])

      redirect_to management_grid_path, notice: "Processando..."
    end
  end

  private

  def is_admin?
    if current_user.role?('admin')
      true
    else
      redirect_to management_root_path, notice: "Você não possui permissão!"
    end
  end
end
