require 'matrix'

class Management::DepartmentsController < ManagementController
  before_action :set_department, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :edit, :update, :destroy]


  # GET /departments
  # GET /departments.json
  def index
    $usuarioLogado = current_user
    respond_to do |format|
        format.html {
            @departments = Department.all.order("name").page(params[:page])
        }
        format.json {
            @departments = Department.all
            render :json => @departments
        }
    end

  end

  # GET /departments/1
  # GET /departments/1.json
  def show
    @courses  = @department.courses
      @users  = @department.users
    @subjects = @department.subjects.all.order(:code).page(params[:page]).per(75)

    #@matrix =  Matrix.build( 17, 7){ |row, col| 0 }
    @matrix = Matrix.new
    @matrix.loadDepartment @department
  end

  # GET /departments/new
  def new
    @department = Department.new
  end

  # GET /departments/1/edit
  def edit
  end

  # POST /departments
  # POST /departments.json
  def create
    @department = Department.new(department_params)
    @department.sector = Sector.find( user_params[:sector_id] )
    respond_to do |format|
      if @department.save
        format.html { redirect_to management_department_path(@department), notice: 'Departamento criado com sucesso!' }
        format.json { render :show, status: :created, location: @department }
      else
        format.html { render :new }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /departments/1
  # PATCH/PUT /departments/1.json
  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to management_department_path(@department), notice: 'Departamento atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @department }
      else
        format.html { render :edit }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to management_departments_url, notice: 'Departamento deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  def reset_modifying
    Department.update_all(is_modifying: true)
    redirect_to :back
  end


  def finalize_modification
    currentuser_set_modifying false
    redirect_to :back
  end


  def reopen_modification
    currentuser_set_modifying true
    redirect_to :back
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.require(:department).permit(:name, :sector_id, :block_id)
    end

    def currentuser_set_modifying(bool_status)
      if current_user.present? and current_user.department.present?
        current_user.department.is_modifying = bool_status;
        current_user.department.save
      end
    end
    def permission_secretary
      if current_user.role?('secretary')
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'

      end
    end
end
