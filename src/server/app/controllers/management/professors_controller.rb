  class Management::ProfessorsController < ManagementController
  before_action :set_professor, only: [:show, :edit, :update, :destroy]
  before_action :permission_secretary, only:  [:create, :edit, :update, :destroy]
  before_action :permission_sector, only: [:create, :edit, :update, :destroy]
  before_action :permission_committee, only: [:create, :destroy]

  # GET /disciplines
  # GET /disciplines.json
  def index
    @professors = if params[:query].present?
      Professor.search(params[:query])
    else
      Professor.all
    end
  end
  # @professors = Professor.search( params[:query], page: params[:page], match: :word_start )


  # GET /disciplines/1
  # GET /disciplines/1.json
  def show
  end

  # GET /disciplines/new
  def new
    @professor = Professor.new
  end

  # GET /disciplines/1/edit
  def edit
  end

  # POST /disciplines
  # POST /disciplines.json
  def create
    @professor = Professor.new(professor_params)
    @professor.department = Department.find( professor_params[:department_id] )

    respond_to do |format|
      if @professor.save
        format.html { redirect_to management_professor_path(@professor), notice: 'professor criado com sucesso.' }
        format.json { render :show, status: :created, location: @professor }
      else
        format.html { render :new }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
    respond_to do |format|
      if @professor.update(professor_params)
        format.html { redirect_to management_professor_path(@professor), notice: 'Professor atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @professor }
      else
        format.html { render :edit }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /disciplines/1
  # DELETE /disciplines/1.json
  def destroy
    @professor.destroy
    respond_to do |format|
      format.html { redirect_to management_professors_url, notice: 'Professor deletado com sucesso.' }
      format.json { head :no_content }
    end
  end


  def autocomplete
    render json: Professor.search(params[:query], autocomplete: true, load: true, misspellings: {below: 5}).map do |prof|
        { name: prof.name, id: prof.id }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_professor
      @professor = Professor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def professor_params
      params.require(:professor).permit(:name,:email,:web,:department_id)
    end
    def permission_secretary
      if (current_user.role?('secretary'))
        render :file => 'public/422.html'

      end
    end
    def permission_committee
      if (current_user.role?('committee'))
        render :file => 'public/422.html'

      end
    end
    def permission_sector
      if (current_user.role?('sector'))
        render :file => 'public/422.html'
      end
    end
end
