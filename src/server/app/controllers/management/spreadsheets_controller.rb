
class Management::SpreadsheetsController < ManagementController
  before_action :set_user, only: [:update, :destroy]
  skip_before_filter :verify_authenticity_token
  require 'csv'
  require 'roo'
  require 'spreadsheet'


  def new
      format = params[:format]
      if format=="json"
          file = File.new("public/uploads/total", "r")
          total = file.gets
          file.close

          file = File.new("public/uploads/done", "r")
          done = file.gets
          file.close

          file = File.open("public/uploads/log", "rb")
          log = file.read
          file.close
          log.gsub! "\n", "<br>"

          json = "{\"total\":"+total+",\"done\":"+done+",\"log\":\""+log+"\"}"
          render text: json
      end
  end


  def create
      uploaded_io = params[:file] #spreadsheet_params[:file]
      File.open(Rails.root.join('public', 'uploads', 'arquivo.xls'), 'wb') do |file|
        file.write(uploaded_io.read)
      end
      file = File.new("public/uploads/total", "w")
      file.write("0");
      file.close
      file = File.new("public/uploads/done", "w")
      file.write("0");
      file.close
      file = File.new("public/uploads/log", "w")
      file.close
      system("rake db:upload &")
      redirect_to :back
  end



  def query
      # @tipo = params[:tipo]

      respond_to do |format|
          format.json {
              @rooms = Room.where("can_use = ?", true).order(:code)
              # @rooms = Room.all.order(:code)
              render :json => @rooms.select(:id, :code, :size)
          }
      end
  end

    def queryProf
        # @tipo = params[:tipo]

        respond_to do |format|
            format.json {
                @professors = Professor.all.order(:name)
                render :json => @professors.select(:id, :name)
            }
        end
    end


  def changeRoom_query
    resposta = []
    ota = []

    resposta = {"status" => "Nem mudou"}

    @schedule = Schedule.new
    @schedule = Schedule.find( params[:id] )

    @klass = Klass.find( @schedule.klass_id )
    @subject = Subject.find( @klass.subject_id )

    room = params[:room]
    klass = params[:klass]
    if ( room[:room_id] == klass )
      resposta = {"status" => "Não mudou"}
    else
      @schedule.room_id = klass

      @new = {:room_id => @schedule.room_id,
              #:room_id_test => @schedule.room_id_test,
              #:created_at => @schedule.created_at,
              :end => @schedule.end,
              :id => @schedule.id,
              :ini => @schedule.ini,
              #:klass_id => @schedule.klass_id,
              #:requireroom => @schedule.requireroom,
              #:updated_at => @schedule.updated_at,
              :weekday => @schedule.weekday
              }

      if ( @schedule.update( @new ) )
        resposta = { "status" => "Ok" }
      else
        resposta = {"status" => "Deu ruim"}
      end
    end

    render :json => resposta
  end


  def changeProf_query
    resposta = []

    resposta = {"status" => "Nem mudou"}

    # @schedule = Schedule.new
    # @schedule = Schedule.find( params[:id] )

    @klass = Klass.find( params[:id] )
    # @professor = Professor.find( @klass.professor_id )

    old_prof = params[:prof_old]
    professor_id = params[:prof_new]
    if ( old_prof == professor_id )
      resposta = {"status" => "Não mudou"}
    else
      @klass.professor_id = professor_id


      # params.require(:klass).permit(:code, :vacancy, :room, :subject_id, :room_id, :course_id)
      @new = {:code => @klass.code,
              :vacancy => @klass.vacancy,
              # :room => @klass.room,
              :subject_id => @klass.subject_id,
              # :room_id => @klass.room_id,
              :course_id => @klass.course_id,
              :professor_id => @klass.professor_id
              }

      if ( @klass.update( @new ) )
        resposta = { "status" => "Ok" }
      else
        resposta = {"status" => "Deu ruim"}
      end
    end

    render :json => resposta
  end




  def operation
      @groups = []

      if current_user.role?('admin') || current_user.role?('committee')
          if ( params.has_key?(:department) )
              department = Department.find(params[:department])
              @department_id = department.id
          else
              @department_id = 0
          end
      else current_user.role?('secretary')
          department = current_user.department
      end

      subjects = []
      if department.present?
          subjects = department.subjects.where(["subject_id is null"]).order(:name)
      end

      subjects.each do |subject|
          list = subject.getRequireRoomKlasses

          if list.size == 0; next; end
          if list.size == 1 && list[0].is_join==false; next; end

          while( list.size > 0 )
              i = 1
              a = 1
              base = list[0]

              group         = Group.new
              group.subject = subject
              group.klasses = []

              group.klasses.push(base)
              while ( i < list.size )
                  klass = list[i]
                  if ( klass_isEqual?(base,klass) )
                      group.klasses.push(klass)
                      list.delete_at(i)
                  else
                      i += 1
                  end
              end
              list.delete_at(0)

              if ( group.klasses.size == 1 )
                  if group.klasses[0].is_join
                      @groups.push( group )
                  end
              elsif group.klasses.size > 1
                  @groups.push( group )
              end
          end

      end
  end



  def subjects
    if current_user.department.present?
        department = current_user.department
        @subjects = department.subjects.allFathers.order(:code)
    else
        @subjects = Subject.allFathers.order(:code)
    end
  end



  def status
    @matrix = Matrix.new
    @matrix.loadDepartment current_user.department
  end



  def changeroom
    @klasses = Klass.allFathers
  end




  def join
    list = params[:klasses]
    if list.size < 2
      redirect_to :back
      return;
    end

    base = nil
    k_join = Klass.new
    k_join.code    = ""
    k_join.vacancy = 0
    k_join.is_join = true
    list.each do |klass_id|
        klass = Klass.find(klass_id.to_i)
        if klass.is_join
            klass.klasses.all.each do |k_sb|
              base = k_sb
              k_join.code    += k_sb.code+";"
              k_join.vacancy += k_sb.vacancy
              k_join.klasses << k_sb
            end
            klass.destroy
        else
            base = klass
            k_join.code    += klass.code+";"
            k_join.vacancy += klass.vacancy
            k_join.klasses << klass
        end
    end
    k_join.requireroom = true;
    k_join.course  = base.course
    k_join.subject = base.subject
    k_join.save

    base.schedules.all.each do |sched|
      n_sched = Schedule.new
      n_sched.ini = sched.ini
      n_sched.end = sched.end
      n_sched.weekday = sched.weekday
      n_sched.klass = k_join
      n_sched.save
    end

      if k_join.new_record?
          redirect_to :back, alert: "Error: "+k_join.save!
      else
          redirect_to :back, notice:"Agrupamento feito com sucesso"
      end
  end

  def divide
    klass = Klass.find(params[:id])
    klass.destroy
    redirect_to :back
  end


  def equivalence
    @equivalences = Subject.where(["is_equivalence = 't'"])
  end


  def add_equivalence

    nom = Subject.where(["code = ?",params[:nom]]).first
    akk = Subject.where(["code = ?",params[:akk]]).first

    if nom.blank?
        flash[:notice] = "A disciplina "+params[:nom]+" não existe"
        redirect_to :back
        return ;
    end
    if akk.blank?
        flash[:notice] = "A disciplina "+params[:akk]+" não existe"
        redirect_to :back
        return ;
    end
    if nom.id == akk.id
        flash[:alert] = "As duas disciplinas tem o mesmo codigo"
        redirect_to :back
        return ;
    end


    if nom.subject.present?
        if akk.subject.present?
            flash[:notice] = "As duas disciplinas já tem equivalencias"
            return ;
        end
        mother = nom.subject
        mother.code += "-" + akk.code;
        mother.name += "-" + akk.name;
        mother.subjects << akk
    elsif akk.subject.present?
        mother = akk.subject
        mother.code += "-" + nom.code;
        mother.name += "-" + nom.name;
        mother.subjects << nom
    else
        mother = Subject.new
        mother.code = nom.code+"-"+akk.code
        mother.name = nom.name+"-"+akk.name
        mother.subjects << nom
        mother.subjects << akk
        mother.department = nom.department

        mother.room_type = nom.room_type
        mother.is_equivalence = true
    end

    if mother.save
        flash[:notice] = "Equivalencia criada"
    else
        flash[:alert] = "error " + mother.errors.full_messages[0]
    end

    redirect_to :back
  end


  def del_equivalence
      subject = Subject.find(params[:id])
      if subject.destroy
          flash[:notice] = "Equivalencia retirada"
      else
          flash[:notice] = "error " + son.errors.full_messages[0]
      end
      redirect_to :back
  end

  def parse_comments
    comments_from_form = params['id']['data-whatever']

    #do your stuff with comments_from_form here
  end


  private
    #Use callbacks to share common setup or constraints before_actiontween actions.
    def set_user
      @usuarioLogado = User.find(params[:id])
    end

    #Never trust parameters from the scary internet, only allow the white list through.
    def spreadsheet_params
      params.permit(:file)
    end


end



def klass_isEqual?(klass_a, klass_b)
    size   = klass_a.schedules.size
    size_b = klass_b.schedules.size
    if size != size_b
        return false
    end
    i = 0
    list_a = klass_a.schedules.order("weekday").order("ini")
    list_b = klass_b.schedules.order("weekday").order("ini")
    while (i<size)
        sched_a = list_a[i]
        sched_b = list_b[i]
        if sched_a.weekday != sched_b.weekday || sched_a.ini != sched_b.ini
            return false
        end
        i += 1
    end
    return true
end


class Group
    attr_accessor :subject, :klasses
end
