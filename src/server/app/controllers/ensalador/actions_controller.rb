class Ensalador::ActionsController < ApplicationController

  def index
    @sectors = Sector.all
  end

  def data
    sectors   = getArrayFromGet(:sectors)
    roomtypes = getArrayFromGet(:roomtypes)
    seasons   = getArrayFromGet(:seasons)

    klasses = []; rooms=[]
    sectors.each do |sector_id|
      sector = Sector.find(sector_id)
      rooms   += sector.classrooms(roomtypes)
      klasses += sector.klasses(seasons)
    end

    #Klass.allRequiredRoom
    #rooms  = Room.getValidRooms
    data = data2ensalamento_yaml_teste(rooms, klasses )

    File.open("public/ensalador/input1", 'w') { |file| file.write(data) }
    #data = rooms[0].toYaml
    #send_file("public/ensalador/input_tmp.txt", type:'text/html; charset=utf-8')
    render text: "ok!"
  end

  def execute
    a = params["prioritize"]

    n = params["yaml"]

    r = params["reverse"]
#render text: "Balas"


    # generate input
    #val = params[:val]
    dist = 10
    free = 1

    #  klasses = Klass.allRequiredRoom
    #  @rooms  = Room.getValidRooms
    #  data2ensalamento_yaml(@rooms, klasses, "public/ensalador/input1")

      # execute ensalador
      input  = "public/ensalador/input1"
    #  input  = "public/ensalador/matexpgraf_pc.yaml"

      #File.open(input, 'w') { |file| file.write(params["text"]) }
      output = "public/ensalador/output1"
      output2 = "public/ensalador/output2"
      options =" "
      if (a == "1")
        options += "-p "
      end
      if (n == "1")
        options += "-y "
      end
      if(r == "1")
        options += "-r "
      end

      cmd = "ensalador "+dist.to_s+" "+free.to_s+" "+input+" "+output+" "+output2+options

      text = ""
      IO.popen cmd do |io|
        io.each do |line|
          text += line
        end
      end

      system ("Relacoes.sh public/ensalador/output2 > public/ensalador/output3")

      redirect_to ensalador_result_path

    #  render text: text


#      if !system( cmd )
#          exitstatus = $?.exitstatus
#          msg = "Erro desconhecido"
#          if exitstatus == 127
#              msg = "Comando ensalador não foi encontrado"
#          else
#              msg = cmd + " <<\n" + File.read(output)
#          end
#          render json: msg
#          return
#      end



      # Generate the Statistics
#      @stats = []
#      file = "public/ensalador/output1"
#      @stats.push Stat.new("MC+CB", file)
    end



    def result
      text  = "<a href='/committee/submit'> submit </a><br>"
      text += File.read( "public/ensalador/output2" ).gsub("\n","<br>")
      text += "<br><br>"
      text += File.read( "public/ensalador/output3" ).gsub("\n","<br>")
      render text: text
    end


    def data2ensalamento_yaml(rooms, klasses, filename)
        text = "rooms:\n"; qtde=0;
        rooms.each do |room|
            text += room.toYaml+"\n"
        end

        File.open(filename, 'w') { |file| file.write(text) }
        text = "schedules:\n"; qtde=0;
        klasses.each do |klass|
            room_type_id = klass.subject.room_type_id
            if (room_type_id.present? and room_type_id>=1&&room_type_id<=4)
                klass.schedules.where(["requireroom='t'"]).each do |sched|
                    text += sched.toYaml+"\n"
                end
            end
        end
        File.open(filename, 'a+') { |file| file.write(text) }
        File.open("public/log.txt", 'w') { |file| file.write(log) }
    end

    def data2ensalamento_yaml_teste(rooms, klasses)
        text = "rooms:\n"; qtde=0;
        rooms.each do |room|
            text += room.toYaml+"\n"
        end

      #  File.open(filename, 'w') { |file| file.write(text) }
        text += "schedules:\n"; qtde=0;
        klasses.each do |klass|
            room_type_id = klass.subject.room_type_id
            if (room_type_id.present? and room_type_id>=1&&room_type_id<=4)
                klass.schedules.where(["requireroom='t'"]).each do |sched|
                    text += sched.toYaml+"\n"
                end
            end
        end
        return text
      #  File.open(filename, 'a+') { |file| file.write(text) }
    #    File.open("public/log.txt", 'w') { |file| file.write(log) }
    end


private
  def getArrayFromGet( field_name )
    if params.include?(field_name)
      res   = params[ field_name ]
      return res.split(",")
    else
      return []
    end
  end

end





class Line
    attr_reader :schedule, :room, :distance, :free

    def load(line)
        raw = line.split(" ");
        room_code = raw[0];
        @schedule = Schedule.find( raw[1].to_i );
        if room_code != "###"
            @room     = Room.findCode( code );
        end
    end

    def isOk?
        return @room != nil
    end
end


class RoomSched
    attr_reader :hour, :room, :klass_code, :subject_code, :distance, :free

    def initialize(line)
        @hour = line.schedule.to_s
        klass = line.schedule.klass
        @klass_code   = klass.code
        @subject_code = klass.subject.getUp.code
        @room = (line.room.present?) ? line.room.code : "*****";
        @free     = line.free
        @distance = line.distance.round(2)
    end
end




class Media
    attr_accessor :name, :dist_sum, :free_sum, :qtde, :not

    def initialize(name)
        @name = name
        @dist_sum  = 0.0
        @free_sum  = 0.0
        @qtde = 0
        @not  = 0
    end

    def dist_mean
        @qtde==0 ? "*" : (dist_sum/qtde).round(2).to_s + "m"
    end

    def free_mean
        @qtde==0 ? "*" : (free_sum/qtde).round(2).to_s
    end

    def add(line)
        if line.isOk?
            @qtde += 1;
            @dist_sum += line.distance
            @free_sum += line.free
        else
            @not += 1
        end
    end
end


class Stat
    attr_reader :name, :departments_mean, :dist_mean, :free_mean, :free_sum, :qtde

    def initialize(name, file)
        @schedules = []
        @departments_mean = departments2hash

        @name = name
        contents = File.read(file)
        lines = contents.split("\n")
        free_sum = 0
        dist_sum = 0
        line = Line.new
        lines.each do |text|
            line.load text
            @schedules.push line.schedule
            department_id = line.schedule.klass.subject.department.id
            @departments_mean[ department_id ].add(line)
            dist_sum += line.distance
            free_sum += line.free
        end
        @qtde = lines.size
        if qtde > 0
            @dist_mean = dist_sum/qtde;
            @free_mean = free_sum/qtde;
            @free_sum  = free_sum;
        end
    end


    def departments2hash
        hash = {}
        Department.all.each do |department|
            hash[ department.id ] = Media.new(department.name)
        end
        return hash
    end


end
