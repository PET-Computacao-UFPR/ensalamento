class RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!
  before_action :is_admin?, only: :create

  def is_admin? 
    unless current_user.nil?
      if current_user.role?('admin')
        return true
      end
    end
    redirect_to root_path, notice: "Apenas administradores!"
  end
end
