class SecsectorController < ManagementController
  before_action :authenticate_secsector

  def authenticate_secsector
    if !@cur_user.is_committee? and !@cur_user.is_secsector?
      redirect_to "/42233.html"
    end
  end
end
