class Secsector::AppointmentsController < SecsectorController
  protect_from_forgery

  def new
    @appointment = Appointment.new
    respond_to do |format|
      format.html
      format.js
    end
  end


  def index
    sector = current_user.sector
    if params[:room_id].blank?
      @appointments_all = Appointment.where("sector_id = ?", sector.id).all
    else
      room_id = params[:room_id].to_i
      @appointments_all = Appointment.where("room_id = ? AND sector_id = ?", room_id, sector.id).all
    end

    @appointments = []
    @appointments_all.each do |appointment|
      if appointment.day >= Date.today 
        @appointments << appointment
      end
    end

    respond_to do |format|
      format.html { @appointments = @appointments.sort_by{|e| [e.day, e.start_at]} }
      format.json { render :json => @appointments }
    end
  end


  def show

  end

  def edit
    if params[:id].blank? || params[:id] == "query"
      @appointments = Appointment.new
    else
      @appointment = Appointment.find(params[:id])
    end

    if @appointment.present?
      change_params = { "was_accepted" => params[:to] }

      if (params[:to] == 'false')
        des = 'des'
      else
        des = ''
      end

      respond_to do |format|
        if @appointment.update(change_params)
          # secsector_appointments_path({:room_id => @appointment.room_id})
          format.html { redirect_to :back, notice: 'Reserva ' + des + 'autorizada com sucesso.' }
          format.json { render :index, status: :ok }
        else
          format.html { redirect_to secsector_appointments_path({:room_id => @appointment.room_id}), alert: 'Reserva não pode ser autorizada.' }
          format.json { render json: @appointment.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to secsector_appointments_path({:room_id => @appointment.room_id}), alert: 'Reserva não pode ser autorizada.' }
        format.json { head :no_content }
      end
    end
  end


  def destroy
    @appointment = Appointment.find(params[:id])
    if @appointment.present?
      @appointment.destroy
      respond_to do |format|
        format.html { redirect_to secsector_appointments_path, notice: 'Reserva deletada com sucesso.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to secsector_appointments_path, notice: 'Reserva não pode ser deletada.' }
        format.json { head :no_content }
      end
    end

  end


  def query
    respond_to do |format|
        format.json {
            @rooms = Room.all.order(:code)
            render :json => @rooms.select(:id, :code)
        }
    end
  end


  def appointment_params
    params.require(:appointment).permit(:title,:requester,:comment,:day,:start_at,:end_at,:room_id)
  end

end
