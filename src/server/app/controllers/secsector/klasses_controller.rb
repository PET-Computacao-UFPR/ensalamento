class Secsector::KlassesController < SecsectorController
    before_action :set_klass, only: [:show, :edit, :update, :destroy]

    def index
      @klasses = current_user.sector.klasses
    end

    def show
    end

    def new
      if params[:klass].present?
        @klass = Klass.new(klass_params)
      else
        @klass = Klass.new
      end
    end

    def create
      @klass = Klass.new(klass_params)
      @klass.subject = Subject.find( klass_params[:subject_id] )

      respond_to do |format|
        if @klass.save
          @cur_user.log("criou #{@klass.name}")
          format.html { redirect_to secsector_klass_path(@klass), notice: 'Classe criada com sucesso.' }
          format.json { render :show, status: :created, location: @klass }
        else
          @cur_user.log("tentou criar #{@klass.name}")
          format.html { render :new }
          format.json { render json: @klass.errors, status: :unprocessable_entity }
        end
      end
    end

    def edit
    end

    def update
      respond_to do |format|
        if @klass.update(klass_params)
          @cur_user.log("modificou #{@klass.name}")
          format.html { redirect_to [:secsector,@klass], notice: 'Classe atualizada com sucesso.' }
          format.json { render :show, status: :ok, location: @klass }
        else
          format.html { render :edit }
          format.json { render json: @klass.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      if @klass.is_join 
        notif1 = 'dividiu as turmas '
        notif2 = 'dividida'
      else
        notif1 = 'excluiu'
        notif2 = 'deletada'
      end
      @cur_user.log(notif1 + " #{@klass.name}")
      @klass.destroy
      respond_to do |format|
        format.html { redirect_to [:secsector,:klasses], notice: 'Classe ' + notif2 + ' com sucesso.' }
        format.json { head :no_content }
      end
    end


    def addSchedule
       schedule = Schedule.new()
       klass = Klass.find(params[:klass_id])
       schedule.klass = klass
       schedule.weekday = 2
       schedule.ini = "00:00"
       schedule.end = "00:00"
       schedule.save
       @cur_user.log("adicionou um horario a #{klass.name}")
       redirect_to :back
    end


private
    def set_klass
        @klass = Klass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def klass_params
        params.require(:klass).permit(:code, :vacancy, :room, :subject_id, :room_id, :course_id, :professor_id, :season_id)
    end

end
