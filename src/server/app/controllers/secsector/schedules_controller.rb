class Secsector::SchedulesController < SecsectorController
  before_action :set_schedule, only: [:edit, :update, :destroy, :setRequireRoom, :empty_schedule]


  # GET /classrooms/new
  def new
    @schedule = Schedule.new
  end

  # GET /classrooms/1/edit
  def edit

  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @schedule = Schedule.new(schedule_params)
    respond_to do |format|
      if @schedule.save
        @cur_user.log("criou um novo horario para #{@schedule.klass.name}")
        format.html { redirect_to :back, notice: 'Horário adicionado com sucesso!' }
        format.json { render :show, status: :created, location: @schedule }

        if defined? params[:klass_id]
          @klass = Klass.find( params[:klass_id] )
          @klass.schedules << @schedule
        end

      else
        format.html { render :new }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update
    respond_to do |format|
      if schedule_params[:ini].to_s.empty? or schedule_params[:end].to_s.empty?
        format.html { redirect_to :back, alert: 'Faltou dados.' }
        format.json { render json: @schedule.errors, status: "Faltou dados." }
      elsif @schedule.update(schedule_params)
        @cur_user.log("alterou horario da #{@schedule.klass.name}")
        format.html { redirect_to :back, notice: 'Horário atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { redirect_to :back, alert: 'Horário não foi atualizado.' }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @cur_user.log("excluiu um horario da #{@schedule.klass.name}")
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Horário deletado com sucesso' }
      format.json { head :no_content }
    end
  end






  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:id, :ini, :end, :weekday, :room_id, :suggested_id)
    end
end
