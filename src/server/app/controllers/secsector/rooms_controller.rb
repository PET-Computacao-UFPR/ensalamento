class Secsector::RoomsController < SecsectorController
  before_action :set_room, only: [:show, :edit, :update, :destroy]

  def index
    @rooms = current_user.sector.rooms
  end

  def show

  end

  def new
    if params[:room].present?
      @room = Room.new( room_params )
    else
      @room = Room.new
    end
  end

  def edit

  end

  def create
    @room = Room.new(room_params)
    respond_to do |format|
      if @room.save
        @cur_user.log("criou @#{@room.rid}")
        format.html { redirect_to secsector_rooms_path, notice: 'Sala criada com sucesso!' }
        format.json { render :show, status: :created, location: @room }
      else
        @cur_user.log("tentou criar uma sala")
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @room.update(room_params)
        @cur_user.log("alterou @#{@room.rid}")
        format.html { redirect_to secsector_rooms_path, notice: 'Sala atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @room }
      else
        @cur_user.log("tentou alterar @#{@room.rid}")
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @room.destroy
    @cur_user.log("destruiu @#{@room.rid}")
    respond_to do |format|
      format.html { redirect_to secsector_rooms_path, notice: 'Sala deletada com sucesso' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.findCode(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:code, :size, :block_id, :room_type_id, :information, :can_use)
    end
end
