class Secsector::DepartmentsController < SecsectorController
  before_action :set_department, only: [:show, :edit, :update, :destroy, :delete_klasses]
  
  def index
      @departments = current_user.sector.departments
      @klasscount = {}
      @departments.each do |department|
        @klasscount[department.id] = Subject.where(department_id: department.id).joins(:klasses).count('klasses.id')
      end
  end

  def show
  end

  def new
    @department = Department.new
  end
  
  def create
    @department = Department.new(department_params)
    @department.sector = Sector.find( current_user.sector )
    #if department_params[:sector_id].present? #verifica se algum foi selecionado?
    #  @department.sector = Sector.find( department_params[:sector_id] )
    #end
    respond_to do |format|
      if @department.save
        format.html { redirect_to secsector_department_path(@department), notice: 'Departamento criado com sucesso!' }
        format.json { render :show, status: :created, location: @department }
      else
        format.html { render :new }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to secsector_departments_url, notice: 'Departamento deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.findCode(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.require(:department).permit(:code, :name, :sector_id, :block_id)
    end
#  private
#    def set_block; @block = Block.find(params[:id]); end
end