class Secsector::BlocksController < SecsectorController
  before_action :set_block, only: [:show, :edit, :update, :destroy]
  before_action :set_last, only: [:index,:show]

  def index
      @blocks = @cur_user.sector.blocks
  end

  def show
      @departments = current_user.sector.departments
      @klasscount = {}
      @departments.each do |department|
        @klasscount[department.id] = Subject.where(department_id: department.id).joins(:klasses).count('klasses.id')
      end
  end

  def new
    @block = Block.new
  end

  def edit
  end

  def create
    @block = Block.new(block_params)
    @block.sector = @cur_user.sector

    respond_to do |format|
      if @block.save
        format.html { redirect_to secsector_block_path(@block), notice: 'Bloco criado com sucesso.' }
        format.json { render :show, status: :created, location: @block }
      else
        format.html { render :new }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @block.update(block_params)
        format.html { redirect_to secsector_block_path(@block), notice: 'Bloco atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @block }
      else
        format.html { render :edit }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @block.destroy
    respond_to do |format|
      format.html { redirect_to secsector_blocks_url, notice: 'Bloco deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_block; @block = Block.findCode(params[:id]); end

    def block_params
      params.require(:block).permit(:id, :code, :name, :latitude, :longitude, :sector_id)
    end
end
