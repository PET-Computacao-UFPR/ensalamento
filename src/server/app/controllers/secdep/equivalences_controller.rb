class Secdep::EquivalencesController < ManagementController
  def index
      @equivalences = current_user.department.subjects.where(["is_equivalence = 't'"])
      department = Department.find(current_user.department);
      @subjects = department.father_subjects.order("LOWER(code)")
  end

  def show
  end

  def create
    nom = Subject.where(["code = ?",params[:nom]]).first
    akk = Subject.where(["code = ?",params[:akk]]).first
    nom_err = params[:nom]
    akk_err = params[:akk]

    #alternativa, com dropdown
    if params[:nom].nil? && params[:akk].nil?
        nom = Subject.where(["id = ?",(params[:nom_col])[:subject_id]]).first
        akk = Subject.where(["id = ?",(params[:akk_col])[:subject_id]]).first
        nom_err = "A"
        akk_err = "B"
    end

    if nom.blank?
        flash[:notice] = "A disciplina "+nom_err+" não existe"
        redirect_to :back
        return ;
    end
    if akk.blank?
        flash[:notice] = "A disciplina "+akk_err+" não existe"
        redirect_to :back
        return ;
    end
    if nom.id == akk.id
        flash[:alert] = "As duas disciplinas tem o mesmo codigo"
        redirect_to :back
        return ;
    end

    if nom.subject.present?
        if akk.subject.present?
            flash[:notice] = "As duas disciplinas já tem equivalencias"
            redirect_to :back
            return ;
        end
        mother = nom.subject
        mother.code += "-" + akk.code;
        mother.name += "-" + akk.name;
        mother.subjects << akk
    elsif akk.subject.present?
        mother = akk.subject
        mother.code += "-" + nom.code;
        mother.name += "-" + nom.name;
        mother.subjects << nom
    else
        mother = Subject.new
        mother.code = nom.code+"-"+akk.code
        mother.name = nom.name+"-"+akk.name
        mother.subjects << nom
        mother.subjects << akk
        mother.department = nom.department

        mother.room_type = nom.room_type
        mother.is_equivalence = true
    end

    if mother.save
        @cur_user.log("criou uma equivalencia com @#{nom.rid} e @#{akk.rid}")
        flash[:notice] = "Equivalencia criada"
    else
        @cur_user.log("tentou criar uma equivalencia com @#{nom.rid} e @#{akk.rid}, mas deu erro"+mother.errors.full_messages[0])
        flash[:alert] = "error " + mother.errors.full_messages[0]
    end

    redirect_to :back
  end


  def destroy
      subject = Subject.find(params[:id])
      @cur_user.log("destruiu a equivalencia #{subject.rid}")
      if subject.destroy
          flash[:notice] = "Equivalencia retirada"
      else
          flash[:notice] = "error " + son.errors.full_messages[0]
      end
      redirect_to :back
  end

end
