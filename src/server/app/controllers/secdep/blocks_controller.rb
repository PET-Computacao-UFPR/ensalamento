class Secdep::BlocksController < ManagementController
  before_action :set_last, only: [:index,:show]

  def index
      @blocks = Block.all.order("LOWER(code)")
  end

  def show
  end

  private
    def set_block; @block = Block.find(params[:id]); end

end
