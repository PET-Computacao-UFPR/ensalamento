
class Secdep::RoomsController < PublicController
  def index
      @rooms = current_user.department.rooms
  end

  def show
      @room = Room.find(params[:id])
      respond_to do |format|
          format.html {}
          format.pdf {}
      end
  end

  private
    def set_block; @room = Block.find(params[:id]); end
end
