class Secdep::SchedulesController < ManagementController
  before_action :set_schedule, only: [:edit, :update, :destroy, :setRequireRoom, :empty_schedule]


  # GET /classrooms/new
  def new
    @schedule = Schedule.new
  end

  # GET /classrooms/1/edit
  def edit

  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @schedule = Schedule.new(schedule_params)

    respond_to do |format|
      if @schedule.save
        @cur_user.log("criou um novo horario para #{@schedule.klass.name}")
        format.html { redirect_to :back, notice: 'Horário adicionado com sucesso!' }
        format.json { render :show, status: :created, location: @schedule }

        if defined? params[:klass_id]
          @klass = Klass.find( params[:klass_id] )
          @klass.schedules << @schedule
        end

      else
        format.html { render :new }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update

      # Alocada
      if !schedule_params[:room_id].blank?
        @room_new = Room.find(schedule_params[:room_id])
      else
        @room_new = nil
      end
      if !@schedule.room_id.blank?
        @room_old = Room.find(@schedule.room_id)
      else
        @room_old = nil
      end

      # Sugerida
      if !schedule_params[:suggested_id].blank?
        @suggested_new = Room.find(schedule_params[:suggested_id])
      else
        @suggested_new = nil
      end
      if !@schedule.suggested_id.blank?
        @suggested_old = Room.find(@schedule.suggested_id)
      else
        @suggested_old = nil
      end


    respond_to do |format|
      if schedule_params[:ini].to_s.empty? or schedule_params[:end].to_s.empty?
        format.html { redirect_to :back, alert: 'Faltou dados.' }
        format.json { render json: @schedule.errors, status: "Faltou dados." }
      elsif @schedule.update(schedule_params)

        # Log
        ok = false
        if @room_new != nil
          if @room_old == nil
            @cur_user.log("alterou horario da #{@schedule.klass.name} %%% sala alocada @#{@room_new.rid}")
          elsif (@room_old.id != @room_new.id)
            @cur_user.log("alterou horario da #{@schedule.klass.name} %%% sala alocada @#{@room_old.rid} -> @#{@room_new.rid}")
          end
        elsif @room_old != nil
          @cur_user.log("alterou horario da #{@schedule.klass.name} %%% sala alocada @#{@room_old.rid} -> Nenhuma Sala")
        else
          ok = true
        end
        if @suggested_new != nil
          if @suggested_old == nil
            @cur_user.log("alterou horario da #{@schedule.klass.name} %%% sala sugerida @#{@suggested_new.rid}")
          elsif (@suggested_old.id != @suggested_new.id)
            @cur_user.log("alterou horario da #{@schedule.klass.name} %%% sala sugerida @#{@suggested_old.rid} -> @#{@suggested_new.rid}")
          end
        elsif @suggested_old != nil
          @cur_user.log("alterou horario da #{@schedule.klass.name} %%% sala sugerida @#{@suggested_old.rid} -> Nenhuma Sala")
        else
          if ok
            @cur_user.log("alterou horario da #{@schedule.klass.name}")
          end
        end

        format.html { redirect_to :back, notice: 'Horário atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { redirect_to :back, alert: 'Horário não foi atualizado.' }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @cur_user.log("excluiu um horario da #{@schedule.klass.name}")
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Horário deletado com sucesso' }
      format.json { head :no_content }
    end
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def get
    sched_id = params[:sched_id]
    sched = Schedule.find( sched_id )
    return sched
  end


  def wrong
     @wrongs = []
     Klass.allRequiredRoom.each do |klass|
         klass.schedules.each do |sched|
             ini = sched.ini
             fim = sched.end
             if ini.hour%2 == 0
                 @wrongs.push sched
             elsif ini.min != 30 || fim.min != 30
                 @wrongs.push sched
             elsif (fim.hour-ini.hour)>2
                 @wrongs.push sched
             end
         end
     end
  end


  def setRequireRoom
    value = params[:value]
    @schedule.requireroom = (value=='f') ? false : true;
    respond_to do |format|
        if @schedule.save
            format.html { redirect_to management_klasses_url, notice: 'Turma alterada com sucesso' }
            format.json { render text: "{\"status\":\"ok\"}" }
        else
            format.json { render json: @schedule.errors, status: :unprocessable_entity }
        end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:id, :ini, :end, :weekday, :room_id, :suggested_id)
    end
end
