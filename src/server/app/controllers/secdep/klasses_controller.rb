class Secdep::KlassesController < ManagementController
    before_action :set_klass, only: [:show, :edit, :update, :destroy]

    def index
      @klasses = current_user.department.klasses
    end

    def show
    end

    def new
      if params[:klass].present?
        @klass = Klass.new(klass_params)
      else
        @klass = Klass.new
      end
    end

    def create
      @klass = Klass.new(klass_params)
      #if klass_params[:subject_id].present? #verifica se algum foi selecionado?
      #  @klass.subject = Subject.find( klass_params[:subject_id] )
      #end

      respond_to do |format|
        if @klass.save
          @cur_user.log("criou #{@klass.name}")
          format.html { redirect_to secdep_klass_path(@klass), notice: 'Classe criada com sucesso.' }
          format.json { render :show, status: :created, location: @klass }
        else
          @cur_user.log("tentou criar #{@klass.name}")
          format.html { render :new }
          format.json { render json: @klass.errors, status: :unprocessable_entity }
        end
      end
    end

    def edit
    end

    def update
      respond_to do |format|
        if @klass.update(klass_params)
          @cur_user.log("modificou #{@klass.name}")
          format.html { redirect_to [:secdep,@klass], notice: 'Classe atualizada com sucesso.' }
          format.json { render :show, status: :ok, location: @klass }
        else
          format.html { render :edit }
          format.json { render json: @klass.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      if @klass.is_join 
        notif1 = 'dividiu as turmas'
        notif2 = 'dividida'
      else
        notif1 = 'excluiu'
        notif2 = 'deletada'
      end
      @cur_user.log(notif1 + " #{@klass.name}")
      @klass.destroy
      respond_to do |format|
        # [:secdep,:klasses]
        format.html { redirect_to :back, notice: 'Classe ' + notif2 + ' com sucesso.' }
        format.json { head :no_content }
      end
    end

    def delAll
        if Klass.destroy_all
            @cur_user.log("excluiu Todas as turmas")
            redirect_to :back, notice: 'Todas as Turmas foram apagadas'
        else
            redirect_to :back, alert: @klass.errors
        end
        # management_klasses_url
    end


    def join
      list_id = params[:klasses]
      if list_id.nil? || list_id.size < 2
        redirect_to :back
        return;
      end

      log = ""
      list = []
      list_id.each do |klass_id|
        klass = Klass.find(klass_id)
        list.push( klass )
        log += klass.name + " "
      end
      Klass.join(list)
      @cur_user.log("agrupou as turmas #{log}")
      redirect_to :back
    end


    def addSchedule
       schedule = Schedule.new()
       klass = Klass.find(params[:klass_id])
       schedule.klass = klass
       schedule.weekday = 2
       schedule.ini = "00:00"
       schedule.end = "00:00"
       schedule.save
       @cur_user.log("adicionou um horario a #{klass.name}")
       redirect_to :back
    end


private
    def set_klass
        @klass = Klass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def klass_params
        params.require(:klass).permit(:code, :vacancy, :room, :subject_id, :room_id, :course_id, :professor_id, :season_id)
    end

end
