class Secdep::ActionsController < ManagementController

    def changeroom
       @klasses = current_user.department.klasses_with_room
    end

    def klassprof
        @klasses = current_user.department.klasses
    end

    def open
        @cur_user.department.is_modifying = true;
        @cur_user.department.save
        redirect_to :back
    end

    def close
      @cur_user.department.is_modifying = false;
      @cur_user.department.save
      redirect_to :back
    end

    def operation
        @groups = []
        department = current_user.department

        subjects = []
        if department.present?
            subjects = department.subjects.where(["subject_id is null"]).order(:name)
        end

        subjects.each do |subject|
            list = subject.klasses_requiredroom

            if list.size == 0; next; end
            if list.size == 1 && list[0].is_join==false; next; end

            while( list.size > 0 )
                i = 1
                a = 1
                base = list[0]

                group         = Group.new
                group.subject = subject
                group.klasses = []

                group.klasses.push(base)
                while ( i < list.size )
                    klass = list[i]
                    if ( base.is_equal?(klass) )
                        group.klasses.push(klass)
                        list.delete_at(i)
                    else
                        i += 1
                    end
                end
                list.delete_at(0)

                if ( group.klasses.size == 1 )
                    if group.klasses[0].is_join
                        @groups.push( group )
                    end
                elsif group.klasses.size > 1
                    @groups.push( group )
                end
            end

        end
    end


    def setRequireRoom
      @schedule = Schedule.find(params[:id])
      @schedule.requireroom = (params[:value]=='f') ? false : true;
      respond_to do |format|
          if @schedule.save
              format.html { redirect_to :back, notice: 'Turma alterada com sucesso' }
              format.json { render text: "{\"status\":\"ok\"}" }
          else
              format.json { render json: @schedule.errors, status: :unprocessable_entity }
          end
      end
    end


    def root
    end


    def statistics
      @matrix = Matrix.new
      @matrix.loadDepartment( @cur_user.department )

      sectors=params[:sectors]
      if sectors.present?
        @others = Matrix.new
        sectors.each do |sector_id|
          sector = Sector.find(sector_id)
          sector.departments.each do |department|
            @others.loadDepartment( department )
          end
        end
      end
    end
end



class Group
    attr_accessor :subject, :klasses
end
