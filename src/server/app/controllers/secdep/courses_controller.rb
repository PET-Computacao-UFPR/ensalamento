class Secdep::CoursesController < ManagementController
  def index
      @courses = current_user.department.courses.sort
  end

  def show
      @course = Course.find(params[:id])
  end

#  private
#    def set_block; @block = Block.find(params[:id]); end
end
