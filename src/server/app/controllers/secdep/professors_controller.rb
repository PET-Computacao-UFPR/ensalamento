class Secdep::ProfessorsController < ManagementController
  before_action :set_professor, only: [:show, :edit, :update, :destroy]
  before_action :set_last, only: [:index,:show]

  def index
      cookies[:last] = request.fullpath
      #@professors = current_user.department.professors.order("LOWER(name)")
      if params[:search].blank?
         @professors = current_user.department.professors.order("LOWER(name)")

      else
        prof_val =  params[:search]
        prof_val_p = "%" + prof_val + "%"

        # para pesquisa parcial (upper - lower cases; uma palavra contendo na string)
        @professors = current_user.department.professors.where("name ILIKE ?", prof_val_p).all.order("LOWER(name)")
      end

      respond_to do |format|
          format.html {@professors = @professors.page(params[:page]).per(75)}
          format.json {render :json => @professors}
      end
  end

  def show
  end

  def new
    @professor = Professor.new
  end

  def edit
  end

  def create
    @professor = Professor.new(professor_params)
    @professor.department = Department.find( current_user.department )

    respond_to do |format|
      if @professor.save
        @cur_user.log("criou @#{@professor.rid}")
        format.html { redirect_to secdep_professor_path(@professor), notice: 'Professor criado com sucesso.' }
        format.json { render :show, status: :created, location: @professor }
      else
        @cur_user.log("tentou criar @#{@professor.rid}")
        format.html { render :new }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @professor.update(professor_params)
        @cur_user.log("alterou @#{@professor.rid}")
        format.html { redirect_to secdep_professor_path(@professor), notice: 'Professor atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @professor }
      else
        @cur_user.log("tentou alterar @#{@professor.rid}")
        format.html { render :edit }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @cur_user.log("excluiu o #{@professor.rid}")
    @professor.destroy
    respond_to do |format|
      format.html { redirect_to secdep_professors_path, notice: 'Professor deletado com sucesso.' }
      format.json { head :no_content }
    end
    # secdep_professors_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_professor
      @professor = Professor.findCode(params[:id])
      if @professor.blank?
        render :file => 'public/404.html', layout:"application", status: 404
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def professor_params
      params.require(:professor).permit(:code,:name,:email,:web,:department_id)
    end

end
