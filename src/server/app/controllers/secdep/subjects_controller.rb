class Secdep::SubjectsController < ManagementController
  before_action :set_subject, only: [:show, :edit, :update, :destroy]

  def index
    @department = Department.find(current_user.department);
    @subjects = @department.father_subjects.order("LOWER(code)")
  end

  def show
  end

  def new
    @subject = Subject.new
  end

  def edit
  end

  def create
    @subject = Subject.new(subject_params)
    @subject.department = @cur_user.department

    respond_to do |format|
      if @subject.save
        @cur_user.log("criou @#{@subject.rid}")
        format.html { redirect_to secdep_subjects_path, notice: 'Disciplina criada com sucesso.' }
        format.json { render :show, status: :created, location: @subject }
      else
        @cur_user.log("tentou criar #{@subject.rid}")
        format.html { render :new }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
      room_type_id = subject_params[:room_type_id]
      requireroom = (subject_params[:room_type_id].blank?) ? false : true;

      @subject.update_requireroom(requireroom)

      respond_to do |format|
      if @subject.update(subject_params)
        @cur_user.log("atualizou @#{@subject.rid}")
        format.html { redirect_to secdep_subjects_path, notice: 'Disciplina atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @subject }
      else
        format.html { render :edit }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    equivalence = @subject.is_equivalence
    if equivalence
      notif1 = 'destruiu a equivalencia'
      notif2 = 'Equivalencia retirada'
      redirecturl = :back
    else
      notif1 = 'excluiu'
      notif2 = 'Disciplina deletada com sucesso.'
      redirecturl = secdep_subjects_url
    end
    @cur_user.log(notif1 + " #{@subject.rid}")
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to redirecturl, notice: notif2 }
      format.json { head :no_content }
    end

  end

  private
    def set_subject; @subject = Subject.findCode(params[:id]); end

    def subject_params
      params.require(:subject).permit(:name, :code, :room_type_id, :requireroom)
    end
end
