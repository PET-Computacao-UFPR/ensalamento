class CommitteeController < ManagementController
  before_action :authenticate_committee

  def authenticate_committee
    if !@cur_user.is_committee?
      redirect_to "/422.html"
    end
  end
end
