class WelcomeController < ApplicationController

  #def index
  #	@user = current_user
  #end

  def show
    @resultados = []
    @tipo = params[:tipo]
    @teste = []

    # Nome da Disciplina
    if @tipo == "nd"
      search_val =  params[:disciplina]
      if search_val == "" || params[:disciplina].blank?
        @resultados = []
      else

        search_val_p = "%" + search_val + "%"
        
        # para pesquisa parcial (upper - lower cases; uma palavra contendo na string)
        subject = Subject.where("name ILIKE ? OR code ILIKE ?", 
                                  search_val_p, search_val_p).all.order("LOWER(name)")

        subject.all.each do |subj|
          subj.klasses.all.each do |klass|
            if klass.klass.nil?
                @resultados << klass
            end
          end
        end

        # ask Assembly
        professors = Professor.where("name ILIKE ?", search_val_p).all.distinct(:id)
        if !professors.blank?
          professors.each do |prof|
            klasses = Klass.where("professor_id = ?", prof.id).all.distinct(:id)
            if !klasses.blank?
              klasses.each do |prof_klass| 
                subject = Subject.where("id = ?", prof_klass.subject_id).all.distinct(:id)
                subject.all.each do |subj|
                  subj.klasses.all.each do |klass|
                    if klass.klass.nil?
                        @resultados << klass
                    end
                  end
                end
              end
            end
          end
          
        end
        # @resultados = @resultados.sort_by{|p| p[:name]}
      end


    elsif params[:sub_section_id].blank?
      @resultados = []

    # Departamento
    elsif @tipo == "d"
        department_id = params[:sub_section_id]
        department = Department.find( department_id )
        @dep = department
        department.subjects.all.order("code").each do |subject|
          subject.klasses.allFathers.order("code").each do |klass|
              @resultados << klass
          end
        end

    # Curso
    elsif @tipo == "c"
        course_id = params[:sub_section_id].to_i
        course = Course.find( course_id )
        @dep = course
        course.subjects.order("code").each do |subject|
          subject.klasses.allFathers.each do |klass|
              if klass.is_join == true
                 klass.klasses.each do |son|
                      if son.course_id == course_id
                          @resultados << klass
                          break;
                      end
                  end
              else
                  if klass.course_id == course_id
                      @resultados << klass
                  end
              end
          end
        end

    # Bloco
    elsif @tipo == "b"
        block_id = params[:sub_section_id]
        block = Block.find( block_id )
        @hashes = []
        @rooms = block.rooms.all.order("LOWER(code)")
        @rooms.each do |room|
            @hashes << room2hash(room)
        end

    # Sala
    elsif @tipo == "s"
        room_id = params[:sub_section_id]
        @room = Room.find( room_id )
        @hash = room2hash(@room)
    end


    respond_to do |format|
        format.html { render :show }
        format.csv {
            response.headers['Content-Disposition'] = 'attachment; filename="' + 'resultado.csv"'
            render text: result2csv(@resultados);
        }
        format.json {
            render :json => @resultados
        }
    end

  end


  def configure_devise_permitted_parameters
    registration_params = [:name, :email, :password, :password_confirmation, :spreadsheet]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) {
          |u| u.permit(registration_params << :current_password)
      }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) {
          |u| u.permit(registration_params)
      }
    end
  end #def



  def query
      @tipo = params[:tipo]

      if @tipo == "nd"
        respond_to do |format|
            format.json {
                @subjects = Subject.allFathers.order("LOWER(name)")
                render :json => @subjects.select(:id, :code, :name)
            }
        end

      elsif @tipo == "d"
        respond_to do |format|
            format.json {
                @departments = Department.all.order("LOWER(name)")
                render :json => @departments.select(:id, :name)
            }
        end

      elsif @tipo == "c"
        respond_to do |format|
            format.json {
                @courses = Course.all.order("LOWER(name)")
                render :json => @courses.select(:id, :name)
            }
        end

      elsif @tipo == "b"
        respond_to do |format|
            format.json {
                @blocks = Block.all.order("LOWER(name)")
                render :json => @blocks.select(:id, :name)
            }
        end

      elsif @tipo == "s"
        respond_to do |format|
            format.json {
                @rooms = Room.all.order("LOWER(code)")
                render :json => @rooms.select(:id, :code)
            }
        end

    end

  end



end #class







def room2hash(room)
    hash = {}
    horarios_sala = []

    horarios_sala = room.schedules
    for i in 0 .. 6
      v_horarios = []
      v_turmas = []
      semana_horarios = []
      for t in 0 .. 23
        semana_horarios[t] = 0
      end

      horarios_sala.each do |horario|
        a = horario.ini_norm .. horario.end_norm-1
        turma = horario.klass
        if horario.weekday == i+1 #aqui tenho um vetor com todo os horarios da sala, de acordo com o dia da semana escolhido por parametro
          v_horarios << a
        end

        if horario.weekday == i+1
          v_turmas << turma
        end
      end
      j = 0
      v_horarios.each do |h|
        h.each do |intervalo|
          semana_horarios[intervalo] = v_turmas[j]  #ocupado
        end
        j = j+1
      end
      if i == 0
        dia = "DOM"
      elsif i == 1
        dia = "SEG"
      elsif i == 2
        dia = "TER"
      elsif i == 3
        dia = "QUA"
      elsif i == 4
        dia = "QUI"
      elsif i == 5
        dia = "SEX"
      elsif i == 6
        dia = "SAB"
      end
      hash[dia] = semana_horarios
    end
    hash
end


def result2csv(resultados)
    text = ""
    @resultados.each do |resultado|
        scheds=resultado.schedules.order("weekday","ini")
        if scheds.size == 0; next; end

        text+=resultado.subject.code+";"
        text+=resultado.subject.name+";"

        size = scheds.size-1

        if resultado.is_join == true
            sons = resultado.klasses
            text += "\""
            if (sons.size > 0 )
                text +=  sons[0].code
                for i in 1..sons.size-1
                    text += "\n"
                    text += sons[i].code
                end
            end
            text += "\";"
        else
            text += resultado.code+";"
        end

        text += "\""
        text += (scheds[0].weekday<7) ? scheds[0].weekday.to_s+"º feira" : "Sábado"
        for i in 1..size
            text += "\n"
            text += (scheds[i].weekday<7) ? scheds[i].weekday.to_s+"º feira" : "Sábado"
        end
        text += "\";"

        text += "\""+scheds[0].ini.hour.to_s + ":" + scheds[0].ini.min.to_s
        for i in 1..size
            text += "\n"
            text += scheds[i].ini.hour.to_s + ":" + scheds[i].ini.min.to_s
        end
        text += "\";"

        text += "\""+scheds[0].end.hour.to_s + ":" + scheds[0].end.min.to_s
        for i in 1..size
            text += "\n"
            text += scheds[i].end.hour.to_s + ":" + scheds[i].end.min.to_s
        end
        text += "\";"


        if resultado.is_join == true
            sons = resultado.klasses
            text += "\""
            if (sons.size > 0 )
                text +=  sons[0].course.code
                for i in 1..sons.size-1
                    text += "\n"
                    text += sons[i].course.code
                end
            end
            text += "\";"
        else
            text += resultado.course.code+";"
        end




        text += "\""
        if scheds[0].room.blank?
            text += (scheds[0].klass.subject.room_type_id==5) ? "Laboratório" : "-"
        else
            text += scheds[0].room.code
        end
        for i in 1..size
            text += "\n"
            if scheds[i].room.blank?
                text += (scheds[i].klass.subject.room_type_id==5) ? "Laboratório" : "-"
            else
                text += scheds[i].room.code
            end
        end
        text += "\"\n"
    end
    text
end
