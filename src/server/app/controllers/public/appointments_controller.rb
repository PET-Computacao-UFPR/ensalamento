class Public::AppointmentsController < PublicController
  protect_from_forgery

  def new
    @appointment = Appointment.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @appointment = Appointment.new( appointment_params )
    @appointment.week = @appointment.day.cweek
    @appointment.was_accepted = false

    @appointments = Appointment.where("room_id = ? AND day = ? AND start_at = ? AND end_at = ? AND was_accepted = 1",
                                        @appointment.room_id, @appointment.day, @appointment.start_at, @appointment.end_at).all

    if (@appointments.present?) and (@appointments.blank? == true) and (@appointment.day >= Date.today) and (@appointment.start_at <= @appointment.end_at)
      room = Room.find(@appointment.room_id)
      @appointment.sector = room.block.sector

      respond_to do |format|
        if @appointment.save
          format.html { redirect_to public_appointments_path({:room_id => @appointment.room_id}), notice: 'Reserva esporádica criada com sucesso.' }
          format.json { render :index, status: :created, location: @appointment }
        else
          format.html { render :new }
          format.json { render json: @appointment.errors, status: :unprocessable_entity }
        end
      end

    else
      respond_to do |format|
        format.html { redirect_to :back, alert: 'Horário já reservado ou Dados Inválidos.' }
        format.json { render json: 'Horário já reservado.', status: :unprocessable_entity }
      end
    end
  end


  def index
    @appointment = Appointment.new

    if params[:room_id].blank?
      @appointments = Appointment.all
      @schedules = []
    else
      room_id = params[:room_id].to_i
      @appointments = Appointment.where("room_id = ?", room_id).all
      @schedules = Schedule.where("room_id = ?", room_id).all
    end

    respond_to do |format|
      format.html
      format.json { render :json => @appointments }
    end
  end


  def show
    @appointments = Appointment.findCode(params[:id])
    if @appointments.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
  end


  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to public_appointments_path, notice: 'Reserva atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @appointment }
      else
        format.html { render :edit }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end


  def query
    respond_to do |format|
        format.json {
            @rooms = Room.all.order(:code)
            render :json => @rooms.select(:id, :code)
        }
    end
  end


  def query_sched

    if ( params['room_id'].blank? )
      @schedules = Schedule.where("room_id IS NOT NULL").all
    else
      @schedules = Schedule.where("room_id = ?", params['room_id']).all
    end

    json_sched = []
    @schedules.each do |sched| 
      a = sched.to_json( params[:this_day] );
      json_sched.push( JSON.parse a ) 
    end

    respond_to do |format|
        format.json { render :json => json_sched }
    end
  end


  def query_appoint
    day = Date.parse(params[:this_day])
    final_day = (day+42).to_s   # 42 == covers a month view

    if ( params['room_id'].blank? )
      @appointments = Appointment.where("day >= ? AND day < ?", day, final_day).all
    else
      @appointments = Appointment.where("room_id = ? AND day >= ? AND day < ?", 
                                        params['room_id'], day, final_day).all
    end

    json_appoint = []
    @appointments.each do |appoint| 
      a = appoint.to_json( day );
      json_appoint.push( JSON.parse a )
    end

    respond_to do |format|
        format.json { render :json => json_appoint }
    end
  end


  def appointment_params
    params.require(:appointment).permit(:title,:requester,:comment,:day,:start_at,:end_at,:room_id)
  end

end
