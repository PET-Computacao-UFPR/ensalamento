class Public::RoomsController < PublicController
  def index
    
    case params[:sortBy]
    when "code"
      @rooms = Room.all.order("LOWER(code)")        
    when "block"
      @rooms = Room.joins(:block).order("blocks.name")
    when "size"
      @rooms = Room.all.order("size DESC")
    when "roomType"
      @rooms = Room.joins(:room_type).order("room_types.name")
    when "canUse"
      @rooms = Room.all.order("can_use DESC")
    when "info"
      @rooms = Room.all.order("information DESC")
    else
      @rooms = Room.all.order("LOWER(code)")
    end

    respond_to do |format|
        format.html {@rooms = @rooms.page(params[:page]).per(75)}
        format.json {render :json => @rooms}
    end
  end

  def show
    
    @room = Room.findCode(params[:id])
    if @room.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
    respond_to do |format|
        format.html {}
        format.pdf {}
    end
  end
end
