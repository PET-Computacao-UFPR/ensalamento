class Public::DbController < PublicController
  def index
    if ( params[:root].blank? )
      return true;
    end

    _root = params[:root];
    root = _root.split(":");

    tables = {}

    tables["departments"] = Department;
    tables["developers"]  = About::Developer;
    tables["klasses"]     = Klass;
    tables["professors"]  = Professor;
    tables["rooms"]       = Room;
    tables["blocks"]      = Block;
    tables["courses"]     = Course;
    tables["schedules"]   = Schedule;
    tables["subjects"]    = Subject;
    tables["sectors"]     = Sector;
    tables["roomtypes"]   = RoomType;

    if root.length == 2
      begin

        root_table = root[0]
        root_id    = root[1]
        root_obj = tables[ root_table ].findCode( root_id );
        response = [{status:"ok"}]
        if params.include?(:edges)
          response += root_obj.get_edges( params[:edges] )
        else
          response.push( root_obj )
        end
        render json: response

      rescue => e
        obj = {status:"error",msg:e.message}
        render json: [obj]
        return false;
      end

    else root.length == 1
      begin
        root_table = root[0]
        search   = tables[ root_table ].all
        response = [{status:"ok"}]
        response += search
        render json: response
      rescue => e
        obj = {status:"error",msg: e.message}
        render json: [obj]
        return ;
      end
    end
  end
end
