class Public::RoomTypesController < PublicController
  def index
    @room_types = RoomType.all.order("LOWER(name)")
    respond_to do |format|
        format.html {}
        format.json {render :json => @room_types}
    end
  end

  def show
      @room_type = RoomType.find(params[:id])
  end
end
