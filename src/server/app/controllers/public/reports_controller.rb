class Public::ReportsController < PublicController
  def index
    text = ""

    all_ini  = Array.new(24){ Array.new(8,0) }
    all_dur  = Array.new(24){ Array.new(8,0) }
    all_size = Array.new(24){ Array.new(8,0) }

    Department.all.each do |department|
        text_extra = "aaaa"
        size = 0
        text += department.name + "\n"
        matrix_ini  = Array.new(24){ Array.new(8,0) }
        matrix_dur  = Array.new(24){ Array.new(8,0) }
        matrix_size = Array.new(24){ Array.new(8,0) }

        department.klasses_with_room.each do |klass|
            klass.schedules.where(["requireroom='t'"]).each do |sched|
                day = sched.weekday
                s_ini = sched.ini_norm
                s_end = sched.end_norm

                if ( sched.ini.min != 30 )
                    text_extra += klass.subject.name
                    text_extra += "," + sched.ini.hour.to_s + ":" + sched.ini.min.to_s
                    text_extra += "," + sched.end.hour.to_s + ":" + sched.end.min.to_s + ",\n"
                end

                all_ini[s_ini][day]     += 1
                all_size[s_ini][day]    += klass.vacancy
                matrix_ini[s_ini][day]  += 1
                matrix_size[s_ini][day] += klass.vacancy

                for i in s_ini+1..s_end-1
                    all_ini[i][day]     += 1
                    all_size[i][day]    += klass.vacancy
                    matrix_ini[i][day]  += 1
                    matrix_size[i][day] += klass.vacancy
                end
            end
            size += klass.schedules.size
        end
        text += "Qtde Total de Turmas," + size.to_s + "\n"

        text += calendar2cvs(matrix_ini, matrix_size)
        text += "\n\n";
    end

    text += "Todos os Departamento\n"
    text += calendar2cvs(all_ini, all_size)

    File.open("public/report_qtde.csv", 'w') { |file| file.write(text) }
    send_file("public/report_qtde.csv", disposition: 'attachment')

  end


private
  def calendar2cvs (matrix_ini, matrix_size)
      max = 0
      text = ""
      for hour in 7..matrix_ini.size-1
          cols = matrix_ini[hour].size
          text += hour.to_s + ":30,"
          for day in 2..cols-1
              text += matrix_ini[hour][day].to_s + " ("
              if matrix_ini[hour][day] > 0
                  text += matrix_size[hour][day].to_s
              end
              text += "),"
              if matrix_ini[hour][day] > max
                  max = matrix_ini[hour][day]
              end
          end
          text += "\n";
      end
      return "Maximo de Turmas por Horario, " + max.to_s + "\n" + text
  end

end
