class Public::DepartmentsController < PublicController
  # pega todos os departamentos e conta todas as turmas abertas pra cada um deles
  def index
    @departments = Department.all.order("LOWER(name)")
    @klasscount = {}
    @departments.each do |department|
      @klasscount[department.id] = Subject.where(department_id: department.id).joins(:klasses).count('klasses.id')
    end
    respond_to do |format|
        format.html {@departments=@departments.page(params[:page]).per(75)}
        format.json {render :json => @departments}
    end
  end

  def show
    @department = Department.findCode(params[:id])
    if @department.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
  end
end
