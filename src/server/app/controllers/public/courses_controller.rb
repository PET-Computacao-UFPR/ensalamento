class Public::CoursesController < PublicController
  def index
    @courses = Course.all.order("LOWER(name)")
    respond_to do |format|
        format.html {@courses=@courses.page(params[:page]).per(75)}
        format.json {render :json => @courses}
    end
  end

  def show
    @course = Course.find(params[:id])
    # @course.subjects = @course.subjects.all.order("LOWER(name)")
    #@matrix = Matrix.new
    #@matrix.loadCourse @course
  end

end
