class Public::BlocksController < PublicController


  def index
    @blocks = Block.all.order("LOWER(code)")
    respond_to do |format|
        format.html {@blocks=@blocks.page(params[:page]).per(75)}
        format.json {render :json => @blocks}
    end
  end

  def show
    @block = Block.findCode(params[:id])
    if @block.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
  end
end
