class Public::WeeksController < PublicController
  def index
    @weeks = Week.all.order(:ini)
    respond_to do |format|
        format.html {}
        format.json {render :json => @weeks}
    end
  end

  def show
    @week = Week.find(params[:id])
  end
end
