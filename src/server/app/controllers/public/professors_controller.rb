class Public::ProfessorsController < PublicController
  def index
  #    @professors = if params[:query].present?
  #      Professor.search(params[:query])
  #    else
  #      offset = params[:page].to_i*20
        #@professors = Professor.all.page(params[:id]).per(75)
  #    end

    if params[:department].present?
      department = Department.find( params[:department] )
      @professors = department.professors.order("LOWER(name)")
    end

    if params[:search].blank?
      if @professors.blank?
        @professors = Professor.all.order("LOWER(name)")
      end
    else
      prof_val =  params[:search]
      prof_val_p = "%" + prof_val + "%"

      # para pesquisa parcial (upper - lower cases; uma palavra contendo na string)
      if @professors.blank?
        @professors = Professor.where("name ILIKE ?", prof_val_p).all.order("LOWER(name)")
      else
        @professors = Professor.where("name ILIKE ? AND department_id LIKE ?", prof_val_p, department.id).all.order("LOWER(name)")
      end
    end

    respond_to do |format|
        format.html {@professors = @professors.page(params[:page]).per(75)}
        format.json {render :json => @professors}
    end

  end

  def show
    @professor = Professor.findCode(params[:id])
    if @professor.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
    respond_to do |format|
        format.html {}
        format.pdf {}
    end

  end
end
