class Public::KlassesController < PublicController
  def index
    @klasses = Klass.all
    respond_to do |format|
        format.html {@klasses = @klasses.page(params[:page]).per(75)}
        format.json {render :json => @klasses}
    end
  end

  def show
    @klass = Klass.findCode(params[:id])
    if @klass.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
  end
end
