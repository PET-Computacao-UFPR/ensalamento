class Public::SectorsController < PublicController
  def index
    @sectors = Sector.all.order("LOWER(name)")

	# array de professores em cada setor
	@professors = []

	@sectors.each do |sector|
		# variavel auxiliar para fazer o somatório
		@sector_prof = 0;
		sector.departments.each do |dept|
			@sector_prof += dept.professors.size
		end
		@professors << @sector_prof
	end

    respond_to do |format|
        format.html {@sectors = @sectors.page(params[:page])}
        format.json {render :json => @sectors}
    end
  end

  def show
    @sector = Sector.findCode(params[:id])
    if @sector.blank?
      render :file => 'public/404.html', layout:"application", status: 404
    end
    @departments = @sector.departments
    @klasscount = {}
    @departments.each do |department|
      @klasscount[department.id] = Subject.where(department_id: department.id).joins(:klasses).count('klasses.id')
    end
  end
end
