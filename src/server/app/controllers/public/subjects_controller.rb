class Public::SubjectsController < PublicController
  def index

    if params[:search].blank?
      @subjects = Subject.allFathers.order("LOWER(code)")

    else
      subject_val =  params[:search]
      subject_val_p = "%" + subject_val + "%"

      # para pesquisa parcial (upper - lower cases; uma palavra contendo na string)
      @subjects = Subject.where("name ILIKE ? OR code ILIKE ?", subject_val_p, subject_val_p).allFathers.order("LOWER(code)")
    end

    respond_to do |format|
        format.html {@subjects = @subjects.page(params[:page]).per(75)}
        format.json {render :json => @subjects}
    end

  end

  def show
      @subject = Subject.findCode(params[:id])
      if @subject.blank?
        render :file => 'public/404.html', layout:"application", status: 404
      end
  end
end
