class Committee::RoomTypesController < CommitteeController
  before_action :set_room_type, only: [:show, :edit, :update, :destroy]

  # GET /classrooms
  # GET /classrooms.json
  def index
      @room_types = RoomType.all.order("LOWER(name)").page(params[:page]).per(75)
  end

  # GET /classrooms/1
  # GET /classrooms/1.json
  def show

  end

  # GET /classrooms/new
  def new
    @room_type = RoomType.new

  end

  # GET /classrooms/1/edit
  def edit

  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @room_type = RoomType.new(room_type_params)

    respond_to do |format|
      if @room_type.save
        format.html { redirect_to committee_room_types_path, notice: 'Sala criada com sucesso!' }
        format.json { render :show, status: :created, location: @room_type }
      else
        format.html { render :new }
        format.json { render json: @room_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update
    respond_to do |format|
      if @room_type.update(room_type_params)
        format.html { redirect_to committee_room_types_path, notice: 'Sala atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @room_type }
      else
        format.html { render :edit }
        format.json { render json: @room_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy

    @room_type.destroy
    respond_to do |format|
      format.html { redirect_to committee_room_types_path, notice: 'Sala deletada com sucesso' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room_type
      @room_type = RoomType.find(params[:id])
    end

    def room_type_params
      params.require(:room_type).permit(:name)
    end
end
