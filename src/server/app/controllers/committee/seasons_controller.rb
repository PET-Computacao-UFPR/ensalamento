class Committee::SeasonsController < CommitteeController
  before_action :set_season, only: [:show, :edit, :update, :destroy]

  # GET /disciplines
  # GET /disciplines.json
  def index
      @seasons = Season.all
  end

  def show
  end

  def new
    @season = Season.new
  end

  # GET /disciplines/1/edit
  def edit
  end

  # POST /disciplines
  # POST /disciplines.json
  def create
    @season = Season.new(season_params)
    @inv_name = 0

    respond_to do |format|
      if Season.exists?(code: @season.code) == false and @season.save
        format.html { redirect_to committee_season_path(@season), notice: 'Periodo criado com sucesso.' }
        format.json { render :show, status: :created, location: @season }
      else
        format.html { render :new }
        format.json { render json: @season.errors, status: :unprocessable_entity }
        @inv_name = 1
      end
    end
  end

  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
    respond_to do |format|
      if @season.update(season_params)
        format.html { redirect_to committee_season_path(@season), notice: 'Periodo atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @season }
      else
        format.html { render :edit }
        format.json { render json: @season.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /disciplines/1
  # DELETE /disciplines/1.json
  def destroy
    @season.destroy
    respond_to do |format|
      format.html { redirect_to committee_seasons_url, notice: 'Periodo deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_season
      @season = Season.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def season_params
      params.require(:season).permit(:code,:ini,:fim)
    end
end

