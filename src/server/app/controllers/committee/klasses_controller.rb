class Committee::KlassesController < CommitteeController
  before_action :set_klass, only: [:show, :edit, :update, :destroy]

  # GET /klasses
  # GET /klasses.json
  def index
      @klasses = Klass.allFathers.order(:subject_id).page(params[:page]).per(75)
  end

  # GET /klasses/1
  # GET /klasses/1.json
  def show
  end

  # GET /klasses/new
  def new
      @klass = Klass.new
      #if ( params[:subject_id].present? ) #verifica se algum foi selecionado?
      #  @klass.subject = Subject.find( params[:subject_id] )
      #end
  end

  # GET /klasses/1/edit
  def edit
  end

  # POST /klasses
  # POST /klasses.json
  def create
    @klass = Klass.new(klass_params)
    #@klass.subject = Subject.find( klass_params[:subject_id] )

    respond_to do |format|
      if @klass.save

        format.html { redirect_to management_klass_path( @klass), notice: 'Classe criada com sucesso.' }
        format.json { render :show, status: :created, location: @klass }
      else
        format.html { render :new }
        format.json { render json: @klass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /klasses/1
  # PATCH/PUT /klasses/1.json
  def update
    respond_to do |format|
      if @klass.update(klass_params)
        format.html { redirect_to :back, notice: 'Classe atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @klass }
      else
        format.html { render :edit }
        format.json { render json: @klass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /klasses/1
  # DELETE /klasses/1.json
  def destroy
    if @klass.is_join 
      notif1 = 'dividiu as turmas '
      notif2 = 'dividida'
    else
      notif1 = 'excluiu'
      notif2 = 'deletada'
    end
    @cur_user.log(notif1 + " #{@klass.name}")
    @klass.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Classe ' + notif2 + ' com sucesso.' }
      format.json { head :no_content }
    end
  end


  def delAll
      if Klass.destroy_all
          redirect_to committee_klasses_url, notice: 'Todas as Turmas foram apagadas'
      else
          redirect_to committee_klasses_url, alert: @klass.errors
      end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_klass
      @klass = Klass.findCode(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def klass_params
      params.require(:klass).permit(:code, :vacancy, :room, :subject_id, :room_id, :course_id)
    end

end
