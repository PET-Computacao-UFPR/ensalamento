  class Committee::ProfessorsController < CommitteeController
  before_action :set_professor, only: [:show, :edit, :update, :destroy]

  # GET /disciplines
  # GET /disciplines.json
  def index
    # @professors = if params[:query].present?
    #   Professor.search(params[:query])
    # else
    #   Professor.all.page(params[:page]).per(75)
    # end

    if params[:search].blank?
      @professors = Professor.all.order(:name)

    else
      prof_val =  params[:search]
      prof_val_p = "%" + prof_val + "%"

      # para pesquisa parcial (upper - lower cases; uma palavra contendo na string)
      @professors = Professor.where("name LIKE ?", prof_val_p).all.order(:name)
    end

    respond_to do |format|
        format.html {@professors = @professors.page(params[:page]).per(75)}
        format.json {render :json => @professors}
    end


  end
  # @professors = Professor.search( params[:query], page: params[:page], match: :word_start )


  # GET /disciplines/1
  # GET /disciplines/1.json
  def show
  end

  # GET /disciplines/new
  def new
    @professor = (params[:professor].present?) ? Professor.new(professor_params) : Professor.new
  end

  # GET /disciplines/1/edit
  def edit
  end

  # POST /disciplines
  # POST /disciplines.json
  def create
    @professor = Professor.new(professor_params)
    #if professor_params[:department_id].present? #verifica se algum foi selecionado?
    #  @professor.department = Department.find( professor_params[:department_id] )
    #end
    respond_to do |format|
      if @professor.save
        format.html { redirect_to committee_professor_path(@professor), notice: 'professor criado com sucesso.' }
        format.json { render :show, status: :created, location: @professor }
      else
        format.html { render :new }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
    respond_to do |format|
      if @professor.update(professor_params)
        format.html { redirect_to committee_professor_path(@professor), notice: 'Professor atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @professor }
      else
        format.html { render :edit }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /disciplines/1
  # DELETE /disciplines/1.json
  def destroy
    @professor.destroy
    respond_to do |format|
      format.html { redirect_to committee_professors_url, notice: 'Professor deletado com sucesso.' }
      format.json { head :no_content }
    end
  end


  def autocomplete
    render json: Professor.search(params[:query], autocomplete: true, load: true, misspellings: {below: 5}).map do |prof|
        { name: prof.name, id: prof.id }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_professor
      @professor = Professor.findCode(params[:id])
      if @professor.blank?
        render :file => 'public/404.html'
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def professor_params
      params.require(:professor).permit(:code,:name,:email,:web,:department_id)
    end
end
