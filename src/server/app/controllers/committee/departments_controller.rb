require 'matrix'

class Committee::DepartmentsController < CommitteeController
  before_action :set_department, only: [:show, :edit, :update, :destroy, :delete_klasses]


  # GET /departments
  # GET /departments.json
  # pega todos os departamentos e conta todas as turmas abertas pra cada um deles
  def index
      @departments = Department.all.order("LOWER(name)")#.page(params[:page]).per(75)
      @klasscount = {}
      @departments.each do |department|
        @klasscount[department.id] = Subject.where(department_id: department.id).joins(:klasses).count('klasses.id')
      end
  end

  # GET /departments/1
  # GET /departments/1.json
  def show
    #@courses  = @department.courses
    #  @users  = @department.users
    #@subjects = @department.subjects.all.order(:code).page(params[:page]).per(75)

    #@matrix =  Matrix.build( 17, 7){ |row, col| 0 }
    #@matrix = Matrix.new
    #@matrix.loadDepartment @department
  end

  # GET /departments/new
  def new
    @department = Department.new
  end

  # GET /departments/1/edit
  def edit
  end

  # POST /departments
  # POST /departments.json
  def create
    @department = Department.new(department_params)
    #if department_params[:sector_id].present? #verifica se algum foi selecionado?
    #  @department.sector = Sector.find( department_params[:sector_id] )
    #end
    respond_to do |format|
      if @department.save
        format.html { redirect_to committee_department_path(@department), notice: 'Departamento criado com sucesso!' }
        format.json { render :show, status: :created, location: @department }
      else
        format.html { render :new }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /departments/1
  # PATCH/PUT /departments/1.json
  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to committee_department_path(@department), notice: 'Departamento atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @department }
      else
        format.html { render :edit }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to committee_departments_url, notice: 'Departamento deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  def reset_modifying
    Department.update_all(is_modifying: true)
    redirect_to :back
  end


  def finalize_modification
    currentuser_set_modifying false
    redirect_to :back
  end


  def reopen_modification
    currentuser_set_modifying true
    redirect_to :back
  end

  def status
      @departments = Department.all.order("name").page(params[:page]).per(75)

  end


  def delete_klasses
    @department.deleteKlasses
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.findCode(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.require(:department).permit(:code, :name, :sector_id, :block_id)
    end


end
