class Committee::ActionsController < CommitteeController
    require 'csv'
    require 'roo'


    def start
    end

    def apagatudo
      Klass.where("klass_id = NULL").destroy_all
      Klass.destroy_all
      redirect_to :back, notice: "Turmas apagadas"
    end

    def upload
          uploaded_io = params[:file]
          if !uploaded_io.nil?
          File.open(Rails.root.join('public', 'uploads', 'arquivo.csv'), 'wb') do |file|
            file.write(uploaded_io.read)
          end
          file = File.new("public/uploads/total", "w")
          file.write("0");
          file.close
          file = File.new("public/uploads/done", "w")
          file.write("0");
          file.close
          file = File.new("public/uploads/log", "w")
          file.close
  
          season = params[:season]
          season_id = season[:season_id]
          cmd = "rake db:upload[#{season_id}] &"
          system(cmd)
          #render text: cmd
        end
        redirect_to :back
    end

    def status
        format = params[:format]
        if format=="json"
            file = File.new("public/uploads/total", "r")
            total = file.gets
            file.close

            file = File.new("public/uploads/done", "r")
            done = file.gets
            file.close

            file = File.open("public/uploads/log", "rb")
            log = file.read
            file.close
            log.gsub! "\n", "<br>"

            json = "{\"total\":"+total+",\"done\":"+done+",\"log\":\""+log+"\"}"
            render text: json
        end
    end


    def submit
      Schedule.update_all(room_id:nil)
      contents = File.read('public/ensalador/output1')
      lines = contents.split("\n")
      line  = Line.new
      lines.each do |text|
            line.load text
            if line.isOk? && line.free >= 0
                line.schedule.room = line.room
                line.schedule.save
            end
      end
      redirect_to [:committee,:klasses], notice:"Publicado!"
    end

    def join_all_same_klasses
      qtde = 0
      Subject.allFathers.each do |subject|
        vetor = {}
        # busca klasses repetidas
        subject.klasses.allFathers.each do |klass|
          if vetor.include?(klass.code)
            vetor[ klass.code ].push(klass)
          else
            vetor[ klass.code ] = [klass]
          end
        end
        # junta as klasses
          vetor.each do |klasspkg|
            if klasspkg[1].size > 1
              begin
                Klass.join(klasspkg[1])
                qtde += 1
              rescue
              end
            end
          end
        end
      redirect_to :back, notice:"#{qtde.to_s} Turmas Juntadas"
    end


    def update_klasses
      log = ""
      uploaded_io = params[:file]
      if !uploaded_io.nil?
        raw = uploaded_io.read.split("\n")
        raw.each.each do |_line|
           line = _line.split(" ")
           klass_code = line[0].split("-")
           suggested_code  = line[1]
           suggested = Room.findCode(suggested_code)
           subject = Subject.findCode( klass_code[0] )
           klass   = subject.klasses.find_by(code: klass_code[1])
           klass.schedules.update_all(["suggested_id=?",suggested.id])
        end
        render text: log
      else
        redirect_to :back
      end
    end


    def teste
      day = params[:day] || 2
      st  = params[:st].to_i || 1

      @schedules = []
      if st == 1
        #@schedules = Schedule.search_by_day(day)
        rooms = Room.getValidRooms

        @schedules = []
        seasons = [1,2]
        sectors = []; rooms = []
        sectors.push ( Sector.findCode("exatas") )
        sectors.push ( Sector.findCode("tecnologia") )
        sectors.push ( Sector.findCode("terra") )
        sectors.each do |sector|
          rooms      += sector.classrooms([1,2,3,4])
          @schedules += sector.schedules(seasons)
        end

        rooms2yaml("public/ensalador/rooms.yaml", rooms);
        schedules2yaml("public/ensalador/schedules.yaml", @schedules);
        redirect_to "/committee/teste?st=2&day=2"
      elsif st == 2
        #if system("cd public/ensalador; ensalador ")
          @schedules = Schedule.load_from_yaml("public/ensalador/output.yaml")
        #  system ("Relacoes.sh public/ensalador/output2 > public/ensalador/output3")
        #end
      end

    end



end


def rooms2yaml(filename, rooms)
  text = "rooms:\n";
  rooms.each do |room|
      text += room.toYaml+"\n"
  end
  File.open(filename, 'w') { |file| file.write(text) }
end

def schedules2yaml(filename, schedules)
    text = "schedules:\n";
    schedules.each do |sched|
        text += sched.toYaml+"\n"
    end
    File.open(filename, 'w') { |file| file.write(text) }
end
