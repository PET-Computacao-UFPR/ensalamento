class Committee::RoomsController < CommitteeController
  before_action :set_room, only: [:show, :edit, :update, :destroy]

  def index
      case params[:sortBy]
      when "code"
        @rooms = Room.all.order("LOWER(code)")        
      when "block"
        @rooms = Room.joins(:block).order("blocks.name")
      when "size"
        @rooms = Room.all.order("size DESC")
      when "roomType"
        @rooms = Room.joins(:room_type).order("room_types.name")
      when "canUse"
        @rooms = Room.all.order("can_use DESC")
      when "info"
        @rooms = Room.all.order("information DESC")
      else
        @rooms = Room.all.order("LOWER(code)")
      end

      @rooms = @rooms.page(params[:page]).per(75)
  end

  def show

  end

  def new
    if params[:room].present?
      @room = Room.new( room_params )
    else
      @room = Room.new
    end
  end

  def edit

  end

  def create
    @room = Room.new(room_params)
    # @room.block     =  Block.find( room_params[:block_id] )
    # @room.room_type =  RoomType.find( room_params[:room_type_id] )

    respond_to do |format|
      if @room.save
        @cur_user.log("criou @#{@room.rid}")
        format.html { redirect_to committee_rooms_path, notice: "Sala criada com sucesso! #{@cur_user.backTo(committee_rooms_path)}" }
        format.json { render :show, status: :created, location: @room }
      else
        @cur_user.log("tentou criar uma sala")
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @room.update(room_params)
        @cur_user.log("alterou @#{@room.rid}")
        format.html { redirect_to committee_rooms_path, notice: 'Sala atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @room }
      else
        @cur_user.log("tentou alterar @#{@room.rid}")
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @room.destroy
    @cur_user.log("destruiu @#{@room.rid}")
    respond_to do |format|
      format.html { redirect_to @cur_user.backTo(committee_rooms_path), notice: 'Sala deletada com sucesso' }
      format.json { head :no_content }
    end
  end

  def free (weekday)
    salas = Room.all
    @hash = {}
    salas.each do |sala|
      v_horarios = []
      horarios = []
      for i in 0 .. 23
        horarios[i] = 0 #livre
      end
      horarios_sala = sala.schedules #verificar
      horarios_sala.each do |horario|
        a = horario.ini.hour..horario.end.hour-1
        v_horarios << a if horario.weekday == weekday #aqui tenho um vetor com todo os horarios da sala, de acordo com o dia da semana escolhido por parametro
      end
      v_horarios.each do |h|
        h.each do |intervalo|
          horarios[intervalo] = 1 #ocupado
        end
      end
      @hash[sala.code] = horarios
    end
    @hash
  end

# a=Array.new(24,1)

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.findCode(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:code, :size, :block_id, :room_type_id, :information, :can_use)
    end
end
