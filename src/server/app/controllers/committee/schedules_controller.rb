class Committee::SchedulesController < CommitteeController
  before_action :set_schedule, only: [:edit, :update, :destroy, :setRequireRoom]


  # GET /classrooms/new
  def new

  end

  # GET /classrooms/1/edit
  def edit

  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @room_type = RoomType.new(room_type_params)

    respond_to do |format|
      if @room_type.save
        format.html { redirect_to committee_room_types_path, notice: 'Sala criada com sucesso!' }
        format.json { render :show, status: :created, location: @room_type }
      else
        format.html { render :new }
        format.json { render json: @room_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update
    respond_to do |format|
      if @schedule.update(schedule_params)
        format.html { redirect_to :back, notice: 'Horario atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { render :edit }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to committee_room_types_path, notice: 'Sala deletada com sucesso' }
      format.json { head :no_content }
    end
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def get
    sched_id = params[:sched_id]
    sched = Schedule.find( sched_id )
    return sched
  end


  def wrong
     @wrongs = []
     Klass.allRequiredRoom.each do |klass|
         klass.schedules.each do |sched|
             ini = sched.ini
             fim = sched.end
             if ini.hour%2 == 0
                 @wrongs.push sched
             elsif ini.min != 30 || fim.min != 30
                 @wrongs.push sched
             elsif (fim.hour-ini.hour)>2
                 @wrongs.push sched
             end
         end
     end
  end


  def setRequireRoom
    value = params[:value]
    @schedule.requireroom = (value=='f') ? false : true;
    respond_to do |format|
        if @schedule.save
            format.html { redirect_to committee_klasses_url, notice: 'Turma alterada com sucesso' }
            format.json { render text: "{\"status\":\"ok\"}" }
        else
            format.json { render json: @schedule.errors, status: :unprocessable_entity }
        end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:id, :ini, :end, :weekday, :room_id)
    end
end
