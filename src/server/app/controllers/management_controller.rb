class ManagementController < ApplicationController
  before_action :authenticate_user!
  layout 'management'

  def set_last
    cookies[:last] = request.fullpath
  end
end
