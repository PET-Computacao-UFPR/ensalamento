class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_devise_permitted_parameters, if: :devise_controller?
  before_action :set_cur_user

protected
  def set_cur_user
      @cur_user = (current_user.blank?) ? User.new : current_user;
      if @cur_user.is_admin?
          if cookies["department_id"].present?
              current_user.department = Department.find(cookies["department_id"]);
          end
          if cookies["sector_id"].present?
              current_user.sector = Sector.find(cookies["sector_id"]);
          end
          if cookies[:last].present?
            @cur_user.backTo = cookies[:last]
          end
      end
  end


  def configure_devise_permitted_parameters
    registration_params = [:name, :email, :password, :password_confirmation, :spreadsheet]

    if params[:action] == 'update'
      devise_parameter_sanitizer.permit(:account_update) {
          |u| u.permit(registration_params << :current_password)
      }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.permit(:sign_up) {
          |u| u.permit(registration_params)
      }
    end
  end

  def after_sign_in_path_for(resource)
    management_root_path
  end
end
