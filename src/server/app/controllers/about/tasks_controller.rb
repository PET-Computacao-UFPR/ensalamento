class About::TasksController < AboutController
  def index
      @tasks = About::Task.all
  end

  def show
      @task  = About::Task.find( params[:id] )
  end
end
