class About::EntitiesController < AboutController
  def index
      @entities = About::Entity.all
      @edges = About::Edge.all
  end

end
