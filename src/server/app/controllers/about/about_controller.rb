class About::AboutController < AboutController
  def index
	  @yaml = YAML.load_file('app/assets/about.yml')
      @developers = @yaml["developers"]
  end

  def license
  end

  def database
      @entities = About::Entity.all
  end
end
