class About::DevelopersController < AboutController
  def index
      @developers = About::Developer.all
  end

  def show
      @developer = About::Developer.find( params[:id] )
  end
end
