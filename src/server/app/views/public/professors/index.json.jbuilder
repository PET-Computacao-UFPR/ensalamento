json.array!(["1"]) do |ok|
  json.class  "Status"
  json.status "ok"
end


json.array!(@professors) do |prof|
  json.class "Professor"
  json.(prof, :gid, :name, :email, :web)
  json.department(prof.department, :gid, :name)
end
