json.Result "OK"
json.TotalRecordCount @subjects.size
json.Records do
  json.array!(@subjects) do |visitor|
    json.id visitor.id
    json.name visitor.name
  end
end
