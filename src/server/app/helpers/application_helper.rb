module ApplicationHelper

  def facebook
    res = ""
    if request.host != "localhost"
      res += "<div id=\"fb-root\"></div>\n"
      res += "<script>(function(d, s, id){\n"
      res += "  var js, fjs = d.getElementsByTagName(s)[0];\n"
      res += "  if (d.getElementById(id)) return;\n"
      res += "  js = d.createElement(s); js.id = id;\n"
      res += "  js.src = \"//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5\";\n";
      res += "  fjs.parentNode.insertBefore(js, fjs);\n"
      res += "}(document, 'script', 'facebook-jssdk'));</script>\n"

      res += "<div class='row'><div class='col-md-12'>\n"
      res += "<div class='fb-comments' data-href='#{request.original_url}' data-numposts='10' style='width=100%'></div>\n"
      res += "</div></div>\n"
    else
      res += "Desativado para Localhost"
    end
    return res.html_safe
  end

  def edit_buttons(role,object)
    res  = link_to "<i class=\"fa fa-edit\"></i>".html_safe  ,[:edit,role,object], class:"btn btn-default"
    res += link_to "<i class=\"fa fa-remove\"></i>".html_safe, [role,object], method: :delete, data: { confirm:"Quer excluir realmente?" }, :class => "btn btn-danger"
    return res.html_safe
  end

  def show_back
    res = ""
    if cookies[:last].present?
      res = link_to("Voltar",cookies[:last],class:"btn btn-info btn-lg")
    end
    return res.html_safe
  end
end
