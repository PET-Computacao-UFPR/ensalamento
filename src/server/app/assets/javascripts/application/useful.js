// useful functions - antes estavam na parte de baixo de app/views/welcome/show.html.erb


$(document).ready(function(){

  $('#searchField').keypress(function (e) {
    if (e.which == 13) {

      buscar();
    }
  });
});


function clean () {
	$("#searchField").val("");
	$("#begindate").val("");
		$("#finaldate").val("");
		$("#horario").html("");
    //  $('#calendar').fullCalendar( 'removeEvents', function(event) {
    //    return true;
    //  });

}


function buscar () {
  var tipo = document.getElementById('tipopesquisa').value;
  var valor = document.getElementById("searchField").value;

  console.log(tipo,valor);

  if (document.getElementById("horario").innerHTML != "") {
    clean();
  };

    if (tipo != "nd") {

      if (tipo == "d") {
        var valor2 = document.getElementById("departamentos").value;
        var ensalamento = document.getElementsByClassName(valor2.toUpperCase());
      }else{
        var ensalamento = document.getElementsByClassName(valor.toUpperCase());
      }

      var eventos = criarEventos(ensalamento);

    } else {
      var eventos = criarEventos(procuraPorNomeDisciplina(valor));
    }

    preencheCalendario(eventos);
}


function procuraPorNomeDisciplina (valor) {
  var ensalamento = document.getElementsByClassName("ensalamento");
  var dados = [];
  var j = 0;
  for (var i = 0; i < ensalamento.length; i++) {
    var aux = ensalamento[i].value.split(',');
    if (removeAcento(aux[2].toUpperCase()).indexOf(removeAcento(valor.toUpperCase())) > -1) {
      dados[j] = ensalamento[i];
      j++;
    }
  }
  return dados;
}


//0 - sala
//1 - cod disciplina
//2 - Nome disciplina
//3 - vagas
//4 - dia semana
//5 - hr inicio
//6 - hr final
//7 - tipo aula
//8 - cod curso
//9 - departamento

function criarEventos (array) {
  var eventos = [];

  for (var i = 0; i < array.length; i++) {

    var dados = array[i].value.split(',');
    if (dados[7].trim() == 'nil') {
      dados[7] = '';
    }else{
      dados[7] = "Aula " + dados[7]
    }
    var objeto = {
      title: dados[2] + " -" + dados[1],
      start: dados[5].trim(),
      end: dados[6].trim(),
      descricao: 'Sala: <b>'+dados[0]+"</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+dados[7]+ "<br>" + dados[9] + "<br>Cod. Curso: <b>"+dados[8] + '</b>',
      className: 'cursorPointer',
      sala: 'Sala: '+dados[0],
      dow: [parseInt(dados[4])]
    };
    eventos.push(objeto);
  };

  return eventos;
}


function preencheCalendario (eventos) {
  //    $('#calendar').fullCalendar( 'removeEvents', function(event) {
  //      return true;
  //    });
  // 		$('#calendar').fullCalendar('addEventSource',{
  // 	   events: eventos,
  // 		});

  // resizeCalendar();
  for (var i = 0; i < eventos.length; i++) {
    var objeto = eventos[i];

    var div = document.createElement('div');
    div.className = 'sala';
    div.innerHTML += objeto.title + "<br>";
    div.innerHTML += objeto.dow+"° feira - às "+objeto.start + " até "+objeto.end + "<br>";
    div.innerHTML += objeto.descricao + "<br>";

    document.getElementById("horario").appendChild(div);

  };
}


function hourFilter (utcTime) {
  var string = utcTime.split(" ")[4]; //17:30:00
  var hour = string.substr(0, 5); //17:30
  return hour;
}


function tipoPesquisa () {
  var tipo = document.getElementById("section_id").value;
  switch(tipo){
    case "hv":
      $("#searchField").hide();
      $("#searchDate").show();
      break;
    /*
    case "nd":
      $("#searchField").show();
      $("#sub_section_id").hide();
      break;
    */
    case "d":
    case "c":
    case "b":
    case "s":
      $("#searchField").hide();
      $("#sub_section_id").show();
      break;
    default:
      $("#searchField").show();
      $("#sub_section_id").hide();
      break;
  }
}


function resizeCalendar () {
    document.getElementsByClassName("fc-scroller")[0].style.height = "791px";//fixa tamanho do calendário
    document.getElementsByClassName("fc-scroller")[0].style.overflow = "hidden";//remove barra rolagem
}



//esta é a opção calendário

// $('#calendar').fullCalendar({
// 	lang: 'pt-br',
// 	defaultView: 'agendaWeek',
//        header: {
//         left: '',
//         center: 'title',
//         right: ''
//    	},
//    	businessHours: {
//    		 start: '6:00',
//    		 end: '24:00'
//    	},
//     eventRender: function(event, element) {//campos adicionais dos eventos
//     	var node = document.createElement("div");
//   			var textnode = document.createTextNode(event.sala);
//         element[0].appendChild(textnode);
//     },
//     eventClick: function(calEvent, jsEvent, view) {
//     	var m = $.fullCalendar.moment.utc(calEvent.start._d);
//     	var start = hourFilter(calEvent.start._d.toUTCString());
//     	var end = hourFilter(calEvent.end._d.toUTCString());

//     	$("#myModalLabel").html(calEvent.title);
//     	$("#modalBody").html(calEvent.descricao +'<br>'+'Início: <b>'+start+'</b><br>'+'Fim: <b>'+end+'</b>');
//     	$("#modalEvent").modal('toggle');
//     },
//    });
//    $("#begindate").datepicker().mask("00/00/0000");
//    $("#finaldate").datepicker().mask("00/00/0000");

//    resizeCalendar();



/* jQuery ajax */

$(document).ready(function() {
  $("select#section_id").change(function() {
    var id_value_string = $(this).val();

    if (id_value_string == "") {
      // if the id is empty remove all the sub_selection options from being selectable and do not do any ajax
      $("select#sub_section_id option").remove();
      var row = "<option value=\"" + "" + "\">" + "" + "</option>";
      $(row).appendTo("select#sub_section_id");
    }

    //else if (id_value_string != 'nd') {
    else {
      // Send the request and update sub category dropdown
      console.log(id_value_string);
      $.ajax({
        dataType: "json",
        cache: false,
        url: '/welcome/query?tipo=' + id_value_string + '&format=json',
        timeout: 2000,
        error: function(XMLHttpRequest, errorTextStatus, error) {
          alert("Failed to submit : "+ errorTextStatus+" ;"+error);
        },
        success: function(data) {
          // Clear all options from sub category select
          $("select#sub_section_id option").remove();

          // //put in a empty default line
          //var row = "<option value=\"" + "" + "\">" + "" + "</option>";
          //$(row).appendTo("select#sub_section_id");

          // // Fill sub category select
          console.log(data.length);

          if ( data && data[0].name && data[0].code) {
            for (i = 0; i < data.length; i++ ) {
              row = "<option value=\"" + data[i].id + "\">" + data[i].code + ' - ' + data[i].name + "</option>";
              $(row).appendTo("select#sub_section_id");
            }
          }
          else if ( data && data[0].name ) {
            for (i = 0; i < data.length; i++ ) {
              row = "<option value=\"" + data[i].id + "\">" + data[i].name + "</option>";
              $(row).appendTo("select#sub_section_id");
            }
          }
          else {
            for (i = 0; i < data.length; i++ ) {
              row = "<option value=\"" + data[i].id + "\">" + data[i].code + "</option>";
              $(row).appendTo("select#sub_section_id");
            }
          }
          //console.log(data);
        }
      });
    };

  });
});
