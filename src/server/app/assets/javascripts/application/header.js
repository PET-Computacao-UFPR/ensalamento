// $(document).ready(function() {
//   /*
//     Search and Autocomplete
//   */
//   var autocomplete_action = null,
//       search_input = $("#query-autocomplete");

//   // search autocomplete
//   search_input.autocomplete({
//     source: function(request, response) {
//       $.getJSON("/search/autocomplete/" + request.term, function(data, status, xhr) {
//         response(data);
//       });
//     },
//     select: function AutoCompleteSelectHandler(event, ui) {
//       autocomplete_action = ui.item.search;
//     },
//     minLength: 2,
//     delay: 500
//   });

//   // remove autocomplete action when type
//   search_input.keyup(function(e) {
//     // keyCode 13: ENTER // search
//     if (e.keyCode != 13) {
//       autocomplete_action = null;
//     }
//   });

//   // search form
//   $("#search-form").submit(function(e) {
//     if (autocomplete_action != null) {
//       e.preventDefault();

//       var array = autocomplete_action.split("-");
//           action = array[0],
//           id = array[1];

//       switch(action) {
//         case "klass":
//           displayModal(id);
//           break;
//         case "room":
//           window.location.href = "/room/" + id;
//           break;
//         case "course":
//           window.location.href = "/course/" + id;
//           break;
//         default:
//           return false
//       }
//     }
//   });

//   /*
//     Class Modal
//   */
//   // class modal
//   var displayModal = function(target) {
//     $.ajax({url: '/class/' + target}).done(function(html) {
//       if ($("#classModal").length) {
//         $('#classModal').empty().append(html);
//       }
//       else {
//         $("body").append('<div id="classModal" class="reveal-modal" data-reveal aria-labelledby="classModal" aria-hidden="true" role="dialog">' + html + '</div>')
//       }
//       $('#classModal').foundation('reveal', 'open');
//     });
//   }

//   // bind displayModal to a.class-modal
//   $('a.class-modal').click(function(e) {
//     e.preventDefault();

//     // hide tooltip
//     $('span.tooltip:visible').hide();
//     $('.button-with-active-tooltip').removeClass('open');

//     // display content
//     var target = $(this).attr('href');
//     displayModal(target);
//   });
// });
