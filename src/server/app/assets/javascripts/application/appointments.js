// Select show script
function getQueryStringValue (key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

function showDropdown( id_value ) {
  $.ajax({
    dataType: "json",
    cache: false,
    url: 'appointments/query?room_id=' + id_value + '&format=json',
    timeout: 2000,
    error: function(XMLHttpRequest, errorTextStatus, error) {
      alert("Failed to submit : "+ errorTextStatus+" ;"+error);
    },
    success: function(data) {
      // Clear all options from sub category select
      $("select#room_id option").remove();

      // //put in a empty default line
      var row = "<option value=\"" + "" + "\">" + "Tudo" + "</option>";
      $(row).appendTo("select#room_id");

      var row = "";
      // if dentro do for... matando um certo alguém do coração.
      for (i = 0; data && i < data.length; i++ ) {
        if ( data[i].id != id_value ) {
          row = "<option value=\"" + data[i].id + "\">" + data[i].code + "</option>";
        }
        else {
          row = "<option selected='selected' value=\"" + data[i].id + "\">" + data[i].code + "</option>";
        }
        $(row).appendTo("select#room_id");
      }
    }

  });
}


// Change date from start and end
var events_json = []

function toDate(selector) {
  var from = selector.split("-");
  return { year: from[0], month: from[1] - 1, day: from[2] };
}

function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:0); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

// Problem with timezone conversion
function check_TimeZone(date) {
  var d = new Date();
    var offset = (d.getTimezoneOffset()/60);
    if ( date.toString().indexOf( 'GMT-0'+offset ) == -1 ) {
      wrong = parseInt(date.toString().substring(30, 31));
      date.setHours ( date.getHours() + wrong );
      date.setHours ( date.getHours() - offset );
    }
    return date;
}

function updateDate(events_json) {
  for ( var i = 0; events_json && i < events_json.length; i++ ) {
    c_day = toDate(events_json[i].day);

    var date = new Date(Date.parse(events_json[i].start));
    var theBigDay = check_TimeZone(date);
    theBigDay.setFullYear(c_day.year, c_day.month, c_day.day);
    events_json[i].start = theBigDay.toUTCString();

    var date = new Date(Date.parse(events_json[i].end));
    var theBigDay = check_TimeZone(date);
    theBigDay.setFullYear(c_day.year, c_day.month, c_day.day);
    events_json[i].end = theBigDay.toUTCString();
  }
}

// Load previous appoitments
function getArrayData(room_id, day, sched_flag) {
  events_json = []

  // This or change to an ajax regular call, with async:false
  jQuery.ajaxSetup({async:false});

  if (sched_flag) {
    $.get('appointments/query_sched?format=json', {room_id: room_id, this_day: day}, function(data) { 
      var sched = (data); 
      events_json.push(sched)
    });
    events_json = events_json[0];
  }

  $.get('appointments/query_appoint?format=json', {room_id: room_id, this_day: day}, function(data) { 
    var appoint = (data); 
    for ( var i = 0; appoint && i < appoint.length; i++ ) {
      events_json.push(appoint[i])
    }
  });

  jQuery.ajaxSetup({async:true});
}


var updateEvent;
var currentLangCode = 'pt-br';


// Calendar script
function renderCalendar(todayDate) {
  var query_value = getQueryStringValue("room_id");
  if ( query_value ) {
    var room_id = query_value;
  }
  else {
    var room_id = $("room_id").val();
  }
  var date = getMonday(new Date());
  // getArrayData(room_id, date);
  // updateDate(events_json);

  $('#calendar').fullCalendar({
    editable: false,
    slotEventOverlap: false,
    columnFormat: {
      month: 'dddd',
      week: 'dddd',
      day: 'ddd'
    },
    minTime: "07:00:00",
    maxTime: "24:00:00",
    lang: currentLangCode,
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    firstDay: 0,

    events: events_json,

    //this section is triggered when the event cell it's clicked
    selectable: false,
    selectHelper: true,
    select: function(start, end) {

      //here I validate that the user can't create an event before today
      if (start < todayDate){
        alert("Você não pode reservar uma data no passado...");
        $("#calendar").fullCalendar("unselect");
        return
      }

      $('#modal_new').modal('show');
      var requester;
      // var eventData = < %= @appointment.to_json.html_safe %>
    },

    eventRender: function(event, element) {
      element.find('.fc-title').append("<br/>Nome: " + event.requester);

      element.attr('href', 'javascript:void(0);');
      element.click(function() {
          $("#startTime").html(moment(event.start).format('h:mm A'));
          $("#endTime").html(moment(event.end).format('h:mm A'));
          $("#eventRoom").html(event.room_name);
          $("#eventRequester").html(event.requester);
          $("#eventInfo").html(event.comment);
          if ( (event.color) != "gray") {
            if ( (event.color) == "green") {
              $("#eventLink").attr('href', 'javascript:void(0);');
              $("#eventLink").html("Autorizado");
            }
            else {
              $("#eventLink").attr('href', 'javascript:void(0);');
              $("#eventLink").html("Não autorizado");
            }
          }
          else {
            $("#eventLink").attr('href', event.url);
            $("#eventLink").html("Mais informações");
          }
          // var full_title = event.title + ' - ' + event.comment;
          $("#eventContent").dialog({ modal: true, title: event.title, width:350});
      });
    },

    viewRender: function (view, element) {
      var view = $('#calendar').fullCalendar('getView');
      console.log("The view's title is " + view.name);
      var b = view.start;

      var sched = true;
      if (!room_id) {
        sched = false;
      }
      if ( view.name == "month" ) {
        sched = false;
      }
      
      if ( view.name == "agendaDay" ) {
        b = getMonday(b.format());
        getArrayData(room_id, b, sched);
      }
      else {
        getArrayData(room_id, b.format(), sched);
      }
      updateDate(events_json);
      console.log("events_after", events_json);
      $('#calendar').fullCalendar('removeEvents');
      $('#calendar').fullCalendar('addEventSource', events_json);
      $('#calendar').fullCalendar('rerenderEvents');
    },

    defaultView: 'agendaWeek',
    allDaySlot: false,
    height: 750,
    slotMinutes: 30,
    timeFormat: 'h:mm t',
    dragOpacity: "0.5"
  });
}

