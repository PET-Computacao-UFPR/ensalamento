



function buscar () {
  var tipo = document.getElementById('tipopesquisa').value;
  var valor = document.getElementById("searchField").value;
  console.log(tipo,valor);

  if (document.getElementById("horario").innerHTML != "") {
    clean();
  };

    // if (tipo != "nd") {

      if (tipo == "d") {
        var valor2 = document.getElementById("departamentos").value;
        var ensalamento = document.getElementsByClassName(valor2.toUpperCase());
        var eventos = criarEventos(ensalamento);
      }
      else if (tipo == "b" || tipo == "c" || tipo == "s" ) {
        var ensalamento = document.getElementsByClassName(valor.toUpperCase());
        var eventos = criarEventos(ensalamento);
      }
    // }
    else {
      var eventos = criarEventos(procuraPorNomeDisciplina(valor));
    }

    preencheCalendario(eventos);
}


function tipoPesquisa () {
  var tipo = document.getElementById("section_id").value;
  switch(tipo){
    case "hv":
      $("#searchField").hide();
      $("#searchDate").show();
      break;
    /*
    case "nd":
      $("#searchField").show();
      $("#sub_section_id").hide();
      break;
    */
    case "d":
    case "c":
    case "b":
    case "s":
      $("#searchField").hide();
      $("#sub_section_id").show();
      break;
    default:
      $("#searchField").show();
      $("#sub_section_id").hide();
      break;
  }
}





$(document).ready(function() {
  $('#searchField').keypress(function (e) {
    if (e.which == 13) {
      buscar();
    }
  });



  /* jQuery ajax */

  $("select#section_id").change(function() {
    var id_value_string = $(this).val();

    console.log("Entrou js");

    if (id_value_string == "") {
      // if the id is empty remove all the sub_selection options from being selectable and do not do any ajax
      $("select#sub_section_id option").remove();
      var row = "<option value=\"" + "" + "\">" + "" + "</option>";
      $(row).appendTo("select#sub_section_id");
    }

    //else if (id_value_string != 'nd') {
    else {
      // Send the request and update sub category dropdown
      console.log(id_value_string);
      $.ajax({
        dataType: "json",
        cache: false,
        url: '/welcome/query?tipo=' + id_value_string + '&format=json',
        timeout: 2000,
        error: function(XMLHttpRequest, errorTextStatus, error) {
          alert("Failed to submit : "+ errorTextStatus+" ;"+error);
        },
        success: function(data) {
          // Clear all options from sub category select
          $("select#sub_section_id option").remove();

          // //put in a empty default line
          //var row = "<option value=\"" + "" + "\">" + "" + "</option>";
          //$(row).appendTo("select#sub_section_id");

          // // Fill sub category select
          console.log(data.length);
          console.log(data);
          if ( data && data[1].name && data[1].code) {
            console.log("name+code");
            for (i = 0; i < data.length; i++ ) {
              row = "<option value=\"" + data[i].id + "\">" + data[i].code + ' - ' + data[i].name + "</option>";
              $(row).appendTo("select#sub_section_id");
            }
          }
          else if ( data && data[1].name ) {
            console.log("name");
            for (i = 0; i < data.length; i++ ) {
              row = "<option value=\"" + data[i].id + "\">" + data[i].name + "</option>";
              $(row).appendTo("select#sub_section_id");
            }
          }
          else {
            console.log("nada?");
            console.log(data);
            // Problema curso com nome null
            for (i = 0; i < data.length; i++ ) {
              row = "<option value=\"" + data[i].id + "\">" + data[i].code + "</option>";
              $(row).appendTo("select#sub_section_id");
            }
          }

        }
      });
    };

  });
});
