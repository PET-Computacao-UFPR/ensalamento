// Funcoes gerais


function removeAcento(strToReplace) {
	str_acento= "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
	str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
	var nova="";
	for (var i = 0; i < strToReplace.length; i++) {
		if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
			nova+=str_sem_acento.substr(str_acento.search(strToReplace.substr(i,1)),1);
		} else {
			nova+=strToReplace.substr(i,1);
		}
	}
	return nova;
}


function unirTuplas () {
  var linha = document.getElementById("selecionados").value;
  linha = linha.split('-');
  document.getElementById("modalidadeExcluir").value = 3;
  x = 0;
  deletarLinha();
  for (var i = 0; i < linha.length; i++) {
    linha[i] = parseInt(linha[i]);
  };
  linha = bubleSort(linha);
  linha.pop();
  linha.shift();

  if (linha.length == 2) {
    document.getElementById("linha-"+linha[0].toString()).value = document.getElementById("lineTemp-"+x).value;
    var string = document.getElementById("lineTemp-"+x).value;
    string = string.split(',');
    vagas = string[3];
    codCurso = string[7];
    x++;
    var line = document.getElementById("line-"+linha[0].toString());
    var colVagas = line.children[4];
    var colCodCurso = line.children[8];
    colCodCurso.innerHTML = codCurso;
    colVagas.innerHTML = vagas;
    var check = line.children[0].children[0].children[0];//check box
    check.click();//clica para desselecionar

    document.getElementById("modalidadeExcluir").value = 2;
    document.getElementById("selecionados").value = "-"+linha[1].toString()+"-";
    deletarLinha();


  }else{
    for (var i = 0; i < 2; i++) {
      document.getElementById("linha-"+linha[i].toString()).value = document.getElementById("lineTemp-"+x).value;
      var string = document.getElementById("lineTemp-"+x).value;
      string = string.split(',');
      vagas = string[3];
      codCurso = string[8 ];
      x++;
      var line = document.getElementById("line-"+linha[i].toString());
      var colVagas = line.children[4];
      colVagas.innerHTML = vagas;
      var colCodCurso = line.children[8];
      colCodCurso.innerHTML = codCurso;
      var check = line.children[0].children[0].children[0];//check box
      check.click();//clica para desselecionar
    };
  }


  document.getElementById("modal-body-unir").innerHTML = "";
  $('#modalUnion').modal('hide');

}

function closePopover (popover) {
  switch(popover) {
    case 1:
        $('#bloco').popover('hide');
        break;
    case 2:
        $('#nbloco').popover('hide');
        break;
    case 3:
        $('#blocos').popover('hide');
        break;
    case 4:
        $('#nblocos').popover('hide');
        break;
    default:
        break;
  }
}


//função dos botões do popover para selecionar os blocos e nblocos
function selecionaBloco (btn) {

    switch(btn.parentNode.id){
      case "popover-bloco":
        atualizaCampo("bloco", btn);
        break;
      case "popover-nbloco":
        atualizaCampo("nbloco", btn);
        break;
      case "popover-blocos":
          atualizaCampo("blocos", btn);
        break;
      case "popover-nblocos":
          atualizaCampo("nblocos", btn);
        break;
    }

}


function atualizaCampo (id, btn) {
  if (btn.className == "block btn btn-default") {
    btn.className += "btn-primary";
    if (document.getElementById(id).value.trim() == "") {
      document.getElementById(id).value = btn.innerHTML;
    }else{
      document.getElementById(id).value += ";" + btn.innerHTML;
    }
  } else{
    btn.className = "block btn btn-default";
    if (document.getElementById(id).value.length > 2) {
      document.getElementById(id).value = document.getElementById(id).value.replace(";" + btn.innerHTML, "");
      document.getElementById(id).value = document.getElementById(id).value.replace(btn.innerHTML + ";", "");
    }else{
      document.getElementById(id).value = document.getElementById(id).value.replace(btn.innerHTML, "");
    }
  }
}



//seleciona tudo
function selectAll () {

	//$('#modalLoad').modal('show');

	//setTimeout(function(){
    var check = $("tbody tr").find("input");
    var checkAll = document.getElementById("checkAll");

    if (checkAll.checked == true) {
      for (var i = 0; i < check.length; i++) {
        check[i].checked = true;
        $("tbody tr").css("background-color", "rgb(208, 208, 208)");
        document.getElementById('selecionados').value += '-' + check[i].parentNode.parentNode.parentNode.id.split('-')[1];
      };
    }else{
      for (var i = 0; i < check.length; i++) {
        check[i].checked = false;
        $("tbody tr").css("background-color", "");
        document.getElementById('selecionados').value = '-';
      };
    }
		//$('#modalLoad').modal('hide');
		//}, 500);

		//console.log(document.getElementById('selecionados').value);

}


//linhas checkadas são marcadas em 'selecionados' para edição em conjunto
function checkAction(objectToCheck, objectToPaint){
  var linha = objectToPaint.id.split('-')[1];
  if (objectToCheck.checked == true) {
    objectToCheck.checked = false;
    objectToPaint.style.backgroundColor =  "";
    var selects = document.getElementById('selecionados').value;
    selects = selects.replace('-'+linha+'-', '-');
    document.getElementById('selecionados').value = selects;
    //console.log(document.getElementById('selecionados').value);
  } else{
    objectToCheck.checked = true;
    objectToPaint.style.backgroundColor = "rgb(208, 208, 208)";
    document.getElementById('selecionados').value += linha + '-';
    //console.log(document.getElementById('selecionados').value);
  }
}



//preenche o modal de acordo com as informaões do input
//  line-x é a linha da tabela
//  linha-x é o input que contem a string que contém os dados
function preencherModalEdit (id) {

  $("#line-"+id).addClass('background-color-gray');

  var input = document.getElementById("linha-"+id).value;
  input = input.split(',');

  document.getElementById("myModalLabel").innerHTML = "#" + input[0] + " - " + input[1];
  document.getElementById("codDisc").value = input[0];
  document.getElementById("nameDisc").value = input[1];
  document.getElementById("codTurm").value = input[2];
  document.getElementById("vagas").value = input[3];
  document.getElementById("diaSem").value = input[4];
  document.getElementById("hora").value = input[5].trim().slice(0, 5) + "~" + input[6].trim().slice(0,5);
  document.getElementById("tipo").value = input[7];
  document.getElementById("codCurso").value = input[8];
  document.getElementById("departamento").value = input[9];
  document.getElementById('sala').value = input[15];
  document.getElementById('bloco').value = input[16];
  document.getElementById('nbloco').value = input[17];
  document.getElementById('tipoCarteira').value = input[18];
  document.getElementById("linhaEmEdicao").value = id;
}


//joga os dados nos inputs
function ensalarSelecionados () {
  var linha = document.getElementById("selecionados").value;
  linha = linha.split('-');

  var sala = document.getElementById('salas').value;
  var bloco = document.getElementById('blocos').value;
  var nbloco = document.getElementById('nblocos').value;
  var tipoCarteira = document.getElementById('tipoCarteiras').value;

  for (var i = 1; i < linha.length - 1; i++) {
    if (linha[i] != "") {


      var string = $("#linha-"+linha[i]).val();
      string = string.split(",");
      string[string.length - 1] = tipoCarteira;
      string[string.length - 2] = nbloco;
      string[string.length - 3] = bloco;
      string[string.length - 4] = sala;

      $("#linha-"+linha[i]).val(string.join(','));
    }
  }

  $('#modalEnsalar').modal('hide');
  limparModal();
}


//joga os dados no input
function salvarDadosLinha() {
  var id = document.getElementById('linhaEmEdicao').value;
  var sala = document.getElementById('sala').value;
  var bloco = document.getElementById('bloco').value;
  var nbloco = document.getElementById('nbloco').value;
  var tipoCarteira = document.getElementById('tipoCarteira').value;

  var string = document.getElementById('linha-' + id).value;
  string = string.split(',');
  string[string.length - 1] = tipoCarteira;
  string[string.length - 2] = nbloco;
  string[string.length - 3] = bloco;
  string[string.length - 4] = sala;

  document.getElementById('linha-' + id).value = string.join(',');
  $('#modalEdit').modal('hide');
  limparModal();

}


function preencherModalDelete (id) {
  document.getElementById('modalidadeExcluir').value = 1;
  document.getElementById("linhaEmEdicao").value = id;

  $("#line-"+id).addClass('background-color-red');

  var input = document.getElementById("linha-"+id).value;
  input = input.split(',');
  document.getElementById("myModalLabelDelete").innerHTML = "Deletar #" + input[0] + " - " + input[1];
  document.getElementById("modal-body-delete").innerHTML = "Tem certeza que deseja excluir esta linha?";


}

//deleta a(s) linha(s) da tabela e o(s) input(s) específico
function deletarLinha () {
  var valor = document.getElementById("modalidadeExcluir").value;

  if (valor == 1) {//exclui 1 linha
    var id = document.getElementById('linhaEmEdicao').value;

    document.getElementById('selecionados').value = document.getElementById('selecionados').value.replace('-'+id+'-', '-');

    $("#line-"+id).addClass('fadeStrong');
    setTimeout(function(){

      $("#linha-"+id).remove();
      $("#line-"+id).remove();

    }, 900);
  } else if (valor == 2) {//exclui várias linhas
    var ids = document.getElementById("selecionados").value;
    ids = ids.split('-');
    for (var i = 0; i < ids.length; i++) {
      $("#line-"+ids[i]).addClass('fadeStrong');
    };

    setTimeout(function(){

      for (var i = 0; i < ids.length; i++) {
        $("#line-"+ids[i]).addClass('fadeStrong');
        $("#linha-"+ids[i]).remove();
        $("#line-"+ids[i]).remove();
      };

    }, 900);

  } else if (valor == 3) {//excluir linhas da uniao
      var linha = document.getElementById("selecionados").value;
      ids = linha.split('-');
      for (var i = 0; i < ids.length; i++) {
        ids[i] = parseInt(ids[i]);
      };

      ids = bubleSort(ids);

      ids.pop();
      ids.shift();
      for (var i = 2; i < ids.length; i++) {
        $("#line-"+ids[i].toString()).addClass('fadeStrong');
      };
      setTimeout(function(){

        for (var i = 2; i < ids.length; i++) {
          $("#line-"+ids[i]).addClass('fadeStrong');
          $("#linha-"+ids[i]).remove();
          $("#line-"+ids[i]).remove();
        };

      }, 900);
  };

  document.getElementById('selecionados').value = '-';

  $('#modalDelete').modal('hide');

  limparModal();
}


function preencherModalExcluirLinhas () {
  document.getElementById('modalidadeExcluir').value = 2;
  var ids = document.getElementById("selecionados").value;

  ids = ids.split('-');

  //primeiro e o último valor são vazios
  for (var i = 1; i < ids.length - 1; i++) {
    $("#line-"+ids[i]).addClass('background-color-red');
  };

  document.getElementById("myModalLabelDelete").innerHTML = "Deletar linhas";
  document.getElementById("modal-body-delete").innerHTML = "Tem certeza que deseja excluir as linhas selecionadas?";
}


function limparModal () {

  document.getElementById("linhaEmEdicao").value = "";

  document.getElementById("myModalLabel").innerHTML = "";
  document.getElementById("codDisc").value = "";
  document.getElementById("nameDisc").value = "";
  document.getElementById("codTurm").value = "";
  document.getElementById("vagas").value = "";
  document.getElementById("diaSem").value = "";
  document.getElementById("hora").value = "";
  document.getElementById("codCurso").value = "";
  document.getElementById("tipo").value = "";
  document.getElementById("departamento").value = "";
  document.getElementById('sala').value = "";
  document.getElementById('bloco').value = "";
  document.getElementById('nbloco').value = "";
  document.getElementById('tipoCarteira').value = "Q";
  document.getElementById('salas').value = "";
  document.getElementById('blocos').value = "";
  document.getElementById('nblocos').value = "";
  document.getElementById('tipoCarteiras').value = "Q";


  document.getElementById("myModalLabelDelete").innerHTML = "";


  $("#bloco").popover('hide');
  $("#nbloco").popover('hide');
  $("#blocos").popover('hide');
  $("#nblocos").popover('hide');

}

//sort do javascript dando problema, implementado o próprio sort ~ trocar depois para um sort melhor
function bubleSort (a) {
  var swapped;
  do {
      swapped = false;
      for (var i=0; i < a.length-1; i++) {
          if (a[i] > a[i+1]) {
              var temp = a[i];
              a[i] = a[i+1];
              a[i+1] = temp;
              swapped = true;
          }
      }
  } while (swapped);

  return a;
}




//================== end === funções para edição das planilhas === end =======================================
