function StreamShow( html) {
  var template = html.innerHTML

  this.render = function(data){
    html.innerHTML += Mustache.render(template,data);
  }

  this.clear = function(){
    html.innerHTML = "";
  }
}




function Stream(html) {
  var self     = this;
  var page     = 0;
  var show_pkg = null;

  htmlshow_pkg = html.getElementsByClassName("stream-show");
  for (i=0; i<htmlshow_pkg.length; i++){
    console.log("criando!!!"+ htmlshow_pkg.length);
    show_pkg = new StreamShow( htmlshow_pkg[i] );
  }



  this.listTo = function(params=""){

    url = "/public/db.json?root=departments"
    console.log( url );

    var xhttp  = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {

      if (this.readyState == 4 && this.status == 200) {
        var data = eval(this.responseText);
        var status = data[0]
        if ( status["status"] != "ok" ){
          alert( status["msg"] )
        }

        show_pkg.clear();
        for (var i=1; i<data.length; i++){
          show_pkg.render(data[i]);
        }
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  }



  this.show = function(data){
    show_pkg.clear();
    console.log(data.length)
    for (var i=0; i<data.length; i++){
      console.log(data[i]);
      show_pkg.render(data[i]);
    }
  }




  this.listTo2 = function(params=""){
    root  = html.getAttribute("dt-root");
    args = ""
    edges = html.getAttribute("dt-edges")
    if ( edges ){
      args += "&edges="+edges
    }

    if ( html.getAttribute("dt-params") ){
      _params += html.getAttribute("dt-params");
    }

    url = "/public/db.json?root="+root+args
    console.log( url );

    var xhttp  = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var data = eval(this.responseText);
        html.innerHTML = "";
        var status = data[0]
        if ( status["status"] != "ok" ){
          alert( status["msg"] )
        }
        for (var i=1; i<data.length; i++){
          html.innerHTML += Mustache.render(template,data[i]);
        }
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  }



  this.cmd = function(params=""){
    root  = html.getAttribute("dt-root");

    args = ""
    edges = html.getAttribute("dt-edges")
    if ( edges ){
      args += "&edges="+edges
    }

    if ( html.getAttribute("dt-params") ){
      _params += html.getAttribute("dt-params");
    }

    url = "/public/db.json?root="+root+args
    console.log( url );



    var xhttp  = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xhttp.open("POST", url, true);
    xhttp.send();
  }





  this.prev = function(){
    if (page > 0) page -= 1;
    this.listTo("page="+page);
  }

  this.next = function(){
    page += 1;
    this.listTo("page="+page);
  }

}


function StreamSystem(url){
  var   global=[];

  var stream_pkg = document.getElementsByClassName("stream");
  for (var i=0; i<stream_pkg.length; i++){
    var html = stream_pkg[i];
    var loader = new Stream( html );
    global[ i ] = loader;
    html.setAttribute("dt-stream",i);

    data = html.getElementsByClassName("stream-data");
    if ( data.length > 0 ){
      for (var j=0; j<data.length; j++){
        loader.show(data[j]);
      }
    } else {
      loader.listTo();
    }
  }



  this.getStream = function(node){
    if ( node.hasAttribute("dt-stream") ){
//console.log( node );
      return global[ node.getAttribute("dt-stream") ]
    }

    parent = node
    for (i=0; i<10; i++){
      if ( parent.className == "stream" ){
        dt_stream = parent.getAttribute("dt-stream");
//console.log(dt_stream);
        node.setAttribute("dt-stream", dt_stream);
        return global[ dt_stream ];
      }
      parent = parent.parentNode
    }
  }



  $( ".stream-search" ).keyup(function(event){
    if(event.keyCode == 13){
      var dt_id = $(this).attr('dt-stream');
      loader  = GBL_STREAMS[ dt_id ];
      pattern = $(this).val()
      loader.listTo("query="+pattern)
    }
  });

  $( ".stream-next" ).click(function(){
      console.log("stream-next");
      stream  = getStream( $(this).context );
      stream.next();
  });

  $( ".stream-prev" ).click(function(){
      console.log("stream-prev");
      stream  = getStream( $(this).context );
      stream.prev();
  });


  $(document).on('click', '.stream-cmd', function () {
      console.log("stream-cmd");
      stream  = getStream( $(this).context );
      stream.cmd();
  });

}
