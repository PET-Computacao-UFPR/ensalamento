class Department < ActiveRecord::Base
  include(Anode)

  #has_and_belongs_to_many   :courses,  -> { uniq }
  has_many    :subjects,    :dependent => :destroy
  has_many    :users,       :dependent => :destroy
  has_many    :professors,  :dependent => :nullify
  belongs_to  :block
  belongs_to  :sector


  #searchkick callbacks: :async

  validates_presence_of :code, :name, :sector_id, :block_id
  #validates_uniqueness_of :name

  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end


  def courses
    vet = {}
    self.subjects.each do |subject|
      subject.courses.each do |course|
        vet[ course.id ] = course
      end
    end
    res = []
    vet.each do |course|
      res.push( course[1] )
    end
    res
  end

  def father_subjects
     return self.subjects.where(["subject_id is null"])
  end

  def countturmas
      total     = 0
      sem_sala  = 0
      self.subjects.each do |subject|
          if subject.room_type.present?
              subject.klasses.allFathers.each do | klass|
                  schedules  = klass.schedules.where(["requireroom='t'"])
                  total     += schedules.size
                  sem_sala  += schedules.where(["room_id is NULL"]).size;
                  #if klass.schedules.requireroom.present?
                  #   i+=1
                  #end
              end
          end
      end
      return [total,total-sem_sala,sem_sala]
  end

  def klasses
    res = []
    self.subjects.order(:code).each do |subject|
        res += subject.klasses.allFathers.order(:code)
    end
    res
  end

  def klasses_with_room
    res = []
    self.subjects.each do |subject|
        res += subject.klasses_requiredroom
    end
    res
  end


  def deleteKlasses
    res = []
    self.subjects.each do |subject|
      subject.klasses.where(["klass_id = NULL"]).each do |klass|
        klass.destroy
      end
      subject.klasses.each do |klass|
        klass.destroy
      end
    end
    res
  end

  def rooms
      res = []
      self.block.rooms.each do |room|
          res.push room
      end
      res
  end


  def get_edges(funcao)
    allowed=["rooms","klasses","professors"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      return "error #{funcao} not found in #{rid}"
    end
  end

end
