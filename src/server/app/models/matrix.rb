class Matrix

  attr_accessor :data

  def initialize
    @data = Array.new(24){ Array.new(8,0) }
  end

  def size
    @data.size
  end

  def loadDepartment(department)
    department.subjects.all.each do |subject|
      if subject.room_type.present?
        subject.klasses.allFathers.each do |klass|
            klass.schedules.where(["requireroom='t'"]).each do |sched|
                load sched
            end
        end
      end
    end
  end

  def loadCourse(course)
      course.klasses.all.each do |klass|
          klass.schedules.allFathers.each do |sched|
              load sched
          end
      end
  end

  def load(sched)
    day = sched.weekday
    s_ini = sched.ini_norm
    s_end = sched.end_norm
    @data[s_ini][day] += 1
    for i in s_ini+1..s_end-1
        @data[i][day] += 1
    end
  end

end
