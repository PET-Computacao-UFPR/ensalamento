class Appointment < ActiveRecord::Base
  has_one :room
  belongs_to :sector

  # scope :between, lambda {|start_time, end_time| {:conditions => ["? < starts_at and starts_at < ?", Event.format_date(start_time), Event.format_date(end_time)] }}
  def self.between(start_time, end_time)
    where('start_at > :lo and start_at < :up',
      :lo => Appointment.format_date(start_time),
      :up => Appointment.format_date(end_time)
    )
  end

  def room_code
    return (room_id.blank?) ? "" : Room.find(room_id).code
  end

  def self.format_date(date_time)
   Time.at(date_time.to_i).to_formatted_s(:db)
  end

  def as_json(options = {})
    if was_accepted == true
      color = "green"
    else
      color = "blue"
    end

    {
      :id         => self.id,
      :title      => self.title,
      :start      => start_at,
      :end        => end_at,
      :requester  => self.requester,
      :comment    => self.comment,
      :day        => self.day,
      :room_id    => self.room_id,
      :room_name  => self.room_code,
      :color      => color,
      :url        => "appointments/" + self.id.to_s
    }
  end
end
