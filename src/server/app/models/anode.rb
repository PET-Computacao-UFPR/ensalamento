module Anode
  def rid
    res = self.class.table_name+"/"
    res += (code.present?) ? code : id.to_s
    return res;
  end

  def create_unique_code(table,base)
    i=1
    code = base
    while table.find_by(code:code).present?
      i += 1
      code = base + i.to_s
    end
    return code
  end

  # def clodovil(code)
  #   return code.to_s.downcase!
  # end

end
