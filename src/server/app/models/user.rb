class User < ActiveRecord::Base
  include Anode

  #has_and_belongs_to_many :roles
  belongs_to :department
  belongs_to :sector

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :registerable

  validates_presence_of :email


  def is_admin?; self.eh_admin; end
  def is_committee?; self.eh_committee || self.eh_admin; end
  def is_secdep?; !self.eh_admin && self.department_id != nil end
  def is_secsector?; !self.eh_admin && self.sector_id != nil end

  def role?(role)
    return true#!!(self.role.name == role.to_s)
  end

  def log(text)
    log = Log.new
    log.user = self
    log.time = DateTime.now.utc
    log.text = text
    log.save
  end

  def backTo=(url)
    @back_url = url;
  end

  def backTo(url)
    return (@back_url.present?) ? @back_url : url
  end

  private

end
