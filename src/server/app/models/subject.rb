class Subject < ActiveRecord::Base
  include(Anode)

  belongs_to :room_type
  belongs_to :department
  has_many   :klasses,    :dependent => :destroy
  has_and_belongs_to_many :courses

  #searchkick callbacks: :async

  belongs_to :subject
  has_many   :subjects,    :dependent => :nullify

  validates_presence_of :name, :code
  validates_uniqueness_of :code

  #after_commit :reindex_dependencies

  #def reindex_dependencies
  #  course.reindex unless course.blank?
  #end

  def self.allFathers
      return Subject.where(["subject_id is null"])
  end

  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end


  def getUp
      return ( self.subject_id.blank? ) ? self : self.subject;
  end

  def fullname
      if @is_equivalence
          name = ""
          self.subjects.each do |son|
              name += son.name
          end
          return name
      else
          return @name
      end
  end

  def fullcode
      if @is_equivalence
          code = ""
          self.subjects.each do |son|
              code += son.code
          end
          return code
      else
          return @code
      end
  end


  def allklasses
      if self.is_equivalence
          list = []
          self.subjects.each do |sub|
              list += sub.klasses
          end
          return list
      else
          return self.klasses
      end
  end

  def getValidKlasses
      list = []
      if self.is_equivalence?
        self.subjects.each do |subject|
          list += subject.klasses.where(["klass_id is null"]).order(code: :asc, course_id: :asc)
        end
      else
        list += self.klasses.where(["klass_id is null"]).order(code: :asc, course_id: :asc)
      end
      #self.klasses.where(["klass_id is null"]).order(code: :asc, course_id: :asc).each do |klass|

      return list
  end


  def getRequireRoomSchedules
      list = []
      if self.is_equivalence
          self.subjects.each do |son|
              son.klasses.allFathers.order(code: :asc, course_id: :asc).each do |klass|
                  list += klass.schedules.where("requireroom='t'")
              end
          end
      else
          self.klasses.allFathers.order(code: :asc, course_id: :asc).each do |klass|
              list += klass.schedules.where("requireroom='t'")
          end
      end
      return list
  end


  def klasses_requiredroom
      list = []
      if self.is_equivalence
          self.subjects.each do |son|
              son.klasses.allFathers.order(code: :asc, course_id: :asc).each do |klass|
                  if klass.schedules.where("requireroom='t'").size > 0
                    list.push(klass)
                  end
              end
          end
      else
          self.klasses.allFathers.order(code: :asc, course_id: :asc).each do |klass|
              if klass.schedules.where("requireroom='t'").size > 0
                  list.push(klass)
              end
          end
      end
      return list
  end


  def update_room_type_for_equivalences(room_type_id)
    if self.is_equivalence
      self.subjects.update_all([room_type:room_type_id]);
    end
  end

  def update_requireroom(requireroom)
    if is_equivalence
      subjects.each do |subject|
        subject.klasses.each do |klass|
          klass.schedules.update_all({requireroom:requireroom})
        end
      end
    else
      klasses.each do |klass|
          klass.schedules.update_all({requireroom:requireroom})
      end
    end
  end


  def get_edges(funcao)
    allowed=["groups"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      raise "error #{funcao} not found"
    end
  end


  def metadatas
    begin
      meta = YAML.load_file("db/metadatas/subjects/#{code}.yml")
      return meta
    rescue
      return []
    end
  end

end
