class Schedule < ActiveRecord::Base
  belongs_to :klass
  belongs_to :room
  belongs_to :suggested, class_name: "Room"

  def to_s
    text = ""
    #if self.ini.present? and self.end.present?
      text  = weekday.to_s + "º -"
      text += self.ini_text + " - "
      text += self.end_text
    #end
    return text
  end

  def self.findCode(code)
    return find(code)
  end

  def self.search_by_time(ini,fim,day)
    res = []
    schedules = Schedule.where(["\"end\">? and ini<? and weekday=? and requireroom='t'",ini,fim,day]).order("ini")
    schedules.each do |sched|
      if sched.klass.klass_id.blank?
        res.push(sched)
      end
    end
    return res
  end

  def self.search_by_day(day)
    res = []
    schedules = Schedule.where(["weekday=? and requireroom='t'",day]).order("ini")
    schedules.each do |sched|
      if sched.klass.klass_id.blank?
        res.push(sched)
      end
    end
    return res
  end


  def self.load_from_yaml(filename)
    schedulespkg = YAML.load_file(filename)
    res = []
    schedulespkg["schedules"].each do |yaml_sched|
      sched = Schedule.find(yaml_sched["id"])
      if (yaml_sched["assigned"].present?)
        sched.room = Room.findCode(yaml_sched["assigned"])
      end
      res.push(sched)
    end
    return res
  end


  def ini_text
      ini.hour.to_s.rjust(2, "0")  +":"+ ini.min.to_s.rjust(2, "0")
  end

  def end_text
      self.end.hour.to_s.rjust(2, "0")+":"+self.end.min.to_s.rjust(2, "0")
  end

  def ini_norm
      if self.ini.blank?
        return 0
      end
      return (self.ini.min < 30) ? self.ini.hour-1 : self.ini.hour
  end

  def end_norm
      if self.end.blank?
        return 0
      end
      return (self.end.min > 30) ? self.end.hour+1 : self.end.hour
  end

  def suggested_code(symbol="")
      return (suggested.present?) ? suggested.code : symbol
  end

  def room_code(symbol="")
      return (room.present?) ? room.code : symbol
  end


  def free_rooms
    salas = Room.all.order("code")
    @hash = []
    if ( self.room.present?)
        @hash.push(self.room)
    end

    salas.each do |sala|
      horarios = []
      for i in 0 .. 23
        horarios[i] = 0 #livre
      end
      horarios_sala = sala.schedules.where(["weekday = ?",self.weekday])
      horarios_sala.each do |horario|
        for i in horario.ini_norm .. horario.end_norm-1
          horarios[i] = 1 #ocupado
        end
      end
      is_free=true;
      for i in self.ini_norm .. self.end_norm-1
          if horarios[i] == 1
              is_free=false;
              break;
          end
      end
      if is_free
          @hash.push(sala)
      end
    end
    @hash
  end

  def subject_name
    return (subject.present?) ? subject.name : "none";
  end

  def toTiObj
      _code = klass.subject.code.gsub(" ","_") + ":" + klass.code
      kl_text = "Schedule{code='"+_code + "';";
      kl_text += "id="+self.id.to_s + ";";
      kl_text += "day="+self.weekday.to_s + ";";
      kl_text += "ini="+self.ini_norm.to_s + ";";
      kl_text += "end="+self.end_norm.to_s + ";";
      kl_text += "size="+self.klass.vacancy.to_s + ";";
      kl_text += "klass_id="+self.klass.id.to_s + ";";

      if !self.room_id.blank?
          kl_text += "room_id="+self.room_id.to_s+";"
      end

      subject = self.klass.subject;
      kl_text += "type="+subject.room_type_id.to_s+";"

      block   = subject.department.block;
      kl_text += "lat="+block.latitude.to_s + ";"
      kl_text += "log="+block.longitude.to_s + ";"
      kl_text += "}"
      return kl_text
  end

  def toYaml
      #kl_text = "   Schedule: {code: \'" + self.klass.name + "\', "
      kl_text = "   - {code: \'" + self.klass.name + "\', "
      kl_text += "id: " + self.id.to_s + ", "
      kl_text += "day: " + self.weekday.to_s + ", "
      kl_text += "ini: " + self.ini_norm.to_s + ", "
      kl_text += "end: " + self.end_norm.to_s + ", "
      kl_text += "size: " + self.klass.vacancy.to_s + ", "
      kl_text += "klass_id: " + self.klass.id.to_s + ", "

      if !self.room_id.blank?
        kl_text += "assigned: '#{self.room.code}', "
      end

      if !self.suggested.blank?
        kl_text += "suggested: '#{self.suggested.code}', "
      end

      subject = self.klass.subject;
      kl_text += "type: " + subject.room_type_id.to_s + ", "
      kl_text += "department: '#{subject.department.code}', "

      block = subject.department.block;
      if block.present?
        kl_text += "block: '#{block.code}', "
        kl_text += "lat: #{block.latitude.to_s}, "
        kl_text += "log: #{block.longitude.to_s}"
      end
      kl_text += "}"
      return kl_text
  end

  def toText
      day   = self.weekday
      s_ini = self.ini_norm
      s_end = self.end_norm

      kl_text = ""
      kl_text += self.klass.subject.code.gsub(" ","_") + " ";
      kl_text += self.id.to_s + " ";
      #text += "0 ";

      hour_id = ( s_ini+(s_ini%2) )/2 - 4;
      kl_text += ( hour_id + (day-2) * 8 ).to_s + " "

      kl_text += self.klass.vacancy.to_s + " ";

      block = self.klass.subject.department.block;
      kl_text += block.latitude.to_s + " "
      kl_text += block.longitude.to_s + " "

      return kl_text
  end


  def isNormal
      s_ini = self.ini_norm
      s_end = self.end_norm
      return s_ini%2 == 1 && s_end - s_ini <= 2
  end


  def get_edges(funcao)
    allowed=["klass","free_rooms"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      return "error "
    end
  end


  def days_week()


  end

  def as_json(options = {}, day)

    # if day.blank?
    #   day = "2017-04-03"
    # end
    date = Date.parse(day.to_s);

    klass = self.klass
    room = self.room

    {
      :id         => self.id,
      :title      => klass.subject.code,
      :start      => self.ini,
      :end        => self.end,
      :requester  => klass.professor_name,
      :comment    => klass.subject.name,
      :day        => date.next_day(self.weekday-1),
      :room_id    => room.id,
      :room_name  => room.code,
      :color      => "gray",
      :url        => "subjects/" + klass.subject.id.to_s
    }
  end

end
