class Course < ActiveRecord::Base
  has_many   :klasses,    :dependent => :destroy
  has_and_belongs_to_many :subjects,    -> { uniq }
  has_and_belongs_to_many :department,  -> { uniq }
  belongs_to :block

  #searchkick callbacks: :async, text_middle: ['name']

  #validates_presence_of :name, :code, :department
  validates_presence_of :code#, :block_id
  validates_uniqueness_of :code

  #after_commit :reindex_dependencies

  #def reindex_dependencies
  #  department.reindex unless department.blank?
  #end

  def klass_by_department(department)
    res = []
    subjects.where(department_id:department.id).each do |subject|
      subject.klasses.where(course_id:self.id).each do |klass|
        res.push klass
      end
    end
    return res
  end


  def get_edges(funcao)
    allowed=["subjects","klasses"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      return "error #{funcao} not found in #{gid}"
    end
  end

end
