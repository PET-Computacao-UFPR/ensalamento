class Klass < ActiveRecord::Base
  
  require 'net/http'
  
  belongs_to :course
  belongs_to :subject
  belongs_to :season
  belongs_to :klass
  belongs_to :professor

  has_many   :klasses,  :dependent => :nullify
  has_many :schedules,  :dependent => :destroy
  #has_one  :professor



  #searchkick callbacks: :async, text_middle: ['discipline_name', 'professor']

  #validates_presence_of :name, :vacancy, :situation, :date_initial, :date_final, :period, :professor, :course
  validates_presence_of :subject_id, :course_id, :code, :season_id, :vacancy, :professor_id
  #validates_uniqueness_of :name, scope: [:discipline]

  def name
      name = ""
      if self.is_join
        self.klasses.each do |klass|
          sub_name = (klass.subject.present?) ? klass.subject.code+"-" : "none-"
          name += sub_name+klass.code+";"
        end
      else
        name = (self.subject.present?) ? self.subject.code+"-"+self.code : "none-"+self.code
      end
      return name
  end

  def code_hash
    res = "ens17-"+name.gsub(";","-")
    password = (ENV["ETHERPAD_PASSWORD"].present?) ? ENV["ETHERPAD_PASSWORD"] : "senhaqualquer"
    res += Digest::MD5.hexdigest(password+self.id.to_s)[0,8]
    return res
  end

  def courses
    if self.is_join
      courses = []
      self.klasses.each do |klass|
        courses.push( klass.course.name )
      end
      return courses
    else
      return [self.course.name]
    end
  end

  def season_name
    return (season.present?) ? season.code : "nenhum"
  end

  def professor_name
    return (professor.present?) ? professor.name : "nenhum"
  end

  def course_name
    return (course.present?) ? course.name : "nenhum"
  end

  def subject_name
    return (subject.present?) ? subject.name : "nenhum"
  end

  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      #code = _code.split("-");
      #subject = Subject.findCode(code[0])
      #subject.klasses.find_by(code:code[1])
      Klass.find_by(code:code)
    end
  end


  def self.allFathers
      return Klass.where(["klass_id is null"])
  end

  def self.allRequiredRoom
      return Klass.where(["klass_id is null and vacancy>0"])
  end


  def self.join(list)
    if list.size < 2
      return false;
    end

    # Valida se todas as klasses tem o mesmo horario
    base = list[0]
    for i in 1..(list.size-1)
      if !base.is_equal?(list[i])
        raise ("As turmas sao incompativeis #{base.name}")
      end
    end


    base = nil
    k_join = Klass.new
    k_join.code    = ""
    k_join.vacancy = 0
    k_join.is_join = true

    list.each do |klass|
        #klass = Klass.find(klass_id.to_i)
        if klass.is_join
            klass.klasses.all.each do |k_sb|
              base = k_sb
              k_sb.remove_rooms
              k_join.code    += k_sb.code+";"
              k_join.vacancy += k_sb.vacancy
              k_join.klasses << k_sb
            end
            klass.destroy
        else
            base = klass
            klass.remove_rooms
            k_join.code    += klass.code+";"
            k_join.vacancy += klass.vacancy
            k_join.klasses << klass
        end
    end

    if ( base == nil )
      raise("Error na unificacao")
    end

    k_join.requireroom = true;
    k_join.course    = base.course
    k_join.subject   = base.subject
    k_join.season    = base.season
    k_join.professor = base.professor
    k_join.save

    base.schedules.where(["requireroom='t'"]).each do |sched|
      n_sched = Schedule.new()
      n_sched.ini = sched.ini
      n_sched.end = sched.end
      n_sched.weekday = sched.weekday
      n_sched.klass = k_join
      n_sched.save
    end

    if k_join.new_record?
        raise ("Error na unificacao")
    end
    return true;
  end



  def is_equal?(klass_b)
    list_a = self.schedules.where("requireroom='t'").order("weekday").order("ini")
    list_b = klass_b.schedules.where("requireroom='t'").order("weekday").order("ini")
    if list_a.size != list_b.size
        return false
    end
    i = 0

    while (i<list_a.size)
        sched_a = list_a[i]
        sched_b = list_b[i]
        if sched_a.weekday != sched_b.weekday || sched_a.ini != sched_b.ini
            return false
        end
        i += 1
    end
    return true
  end


  def get_edges(funcao)
    allowed=["schedules"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      return "error #{funcao} not found in #{gid}"
    end
  end

  def remove_rooms
    self.schedules.update_all("room_id=NULL");
  end

  def to_telegram
    res = self.name + "(#{self.professor_name}) :\n"
    self.schedules.each do |sched|
        res += "  - #{sched.to_s} => #{sched.room_code}\n"
    end
    return res
  end


  def meta
      def valid_json(json)
        JSON.parse(json)
          return true
        rescue JSON::ParserError => e
          return false
      end
      if @days.blank?
          text  = ""
          @days = []
          data = Net::HTTP.get('etherpad.c3sl.ufpr.br', "/p/#{self.code_hash}/export/txt")
          File.open("public/teste.json", 'w') { |file| file.write(data) }
          text = File.read "public/teste.json"
          if text.present? && valid_json(text)
            json = JSON.parse text
          end

          season = self.season
          if season.present?
            now = season.ini
            id=1;
            while now < season.fim
                self.schedules.each do |sched|
                    meta = Meta.new
                    meta.date = now+(sched.weekday-1)
                    if json.present?
                      json_obj = json.dig("aula#{id}", 0)
                      if json_obj.present?
                        meta.theme = json_obj["tema"]
                        meta.slide = json_obj["slide"]
                        meta.video = json_obj["video"]
                        meta.exerc = json_obj["exerc"]
                      end
                    end

                    meta.category = (sched.category.present?) ? sched.category : ""
                    meta.room = (sched.room.present?) ? sched.room.code : ""

                    @days.push(meta)
                    id += 1;
                end
                now += 7;
            end
          end
      end
      return @days
  end




end

class Meta
    attr_accessor :date, :room, :theme, :category, :slide, :video, :exerc
end
