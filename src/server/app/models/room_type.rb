class RoomType < ActiveRecord::Base
  has_many   :rooms,       :dependent => :nullify
  has_many   :subjects,    :dependent => :nullify

  #searchkick callbacks: :async, text_middle: ['name']

  #validates_presence_of :name, :code, :department
  validates_presence_of   :name
  validates_uniqueness_of :name

  #after_commit :reindex_dependencies

  #def reindex_dependencies
  #  department.reindex unless department.blank?
  #end

  def get_edges(funcao)
    allowed=["rooms","subjects"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      return "error #{funcao} not found in #{gid}"
    end
  end

  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end
end
