class Log < ActiveRecord::Base
  belongs_to :user

  def html
    split = self.text.split "@"
    res = split[0]
    for i in 1..split.size-1
      text = split[i].split(" ")
      res += "<a href='/public/#{text[0]}'>#{text[0]}</a>"
      for i in 1..text.size-1
        res += " " + text[i] + " "
      end
    end
    return res
  end

end
