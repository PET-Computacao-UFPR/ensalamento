module About
class Edge
    include ActiveModel::AttributeMethods
#    extend ActiveModel::Callbacks
#ActiveRecord::Base.establish_connection(adapter: 'mysql3',pool: 5,username: 'me',password: 'mypassword',database: "teste")

    attr_accessor :from, :to, :name

    def path
        return ""
    end

    def self.all
        res  = []
        json = self.load_data
        json["Edge"].each do |obj|
          edge = Edge.new;
          edge.name = obj["name"]
          edge.from = obj["nom"]
          edge.to   = obj["akk"]
          res.push edge
        end
        return res
    end


    def self.load_data
        text = File.read "project_src/database.json"
        json = JSON.parse text
        return json
    end

end
end
