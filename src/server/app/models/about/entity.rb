module About
class Entity
    include ActiveModel::AttributeMethods
#    extend ActiveModel::Callbacks
#ActiveRecord::Base.establish_connection(adapter: 'mysql3',pool: 5,username: 'me',password: 'mypassword',database: "teste")

    attr_accessor :id, :code, :name

    def path
        return "/about/entities/#{@id}"
    end

    def self.all
        res  = []
        json = self.load_data
        json["Entity"].each do |obj|
          entity = Entity.new;
          entity.code = obj["code"]
          entity.name = obj["name"]
          res.push entity
        end
        return res
    end


    def self.load_data
        text = File.read "project_src/database.json"
        json = JSON.parse text
        return json
    end

end
end
