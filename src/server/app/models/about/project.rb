module About
class Project
    include ActiveModel::AttributeMethods

    attr_accessor :name, :image, :code

    def self.all
        text = ""
        IO.popen("ti2json project_src/projects") {|fp|
             text += fp.read
        }
        json = JSON.parse text
        res  = []
        json["box"].each do |obj|
            project = Project.new;
            project.name  = obj["name"]
            project.image = obj["image"]
            project.code  = obj["code"]
            res.push project
        end
        return res
    end


end
end
