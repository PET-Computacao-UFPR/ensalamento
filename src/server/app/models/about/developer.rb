module About
class Developer
    include ActiveModel::AttributeMethods
#    extend ActiveModel::Callbacks
#ActiveRecord::Base.establish_connection(adapter: 'mysql3',pool: 5,username: 'me',password: 'mypassword',database: "teste")

    attr_accessor :name, :web, :email, :id

    def path
        return "/about/developers/#{@id}"
    end



    def self.all
        res  = []
        json = self.load_data
        json["box"].each do |obj|
            developer = Developer.new;
            developer.id  = obj["id"]
            developer.name  = obj["name"]
            developer.email = obj["email"]
            res.push developer
        end
        return res
    end


    def self.find(id)
        json = self.load_data
        json["box"].each do |obj|
            if obj["id"] == id
                developer = Developer.new;
                developer.id  = obj["id"]
                developer.name  = obj["name"]
                developer.email = obj["email"]
                return developer
            end
        end
        return nil
    end



    def self.load_data
        text = ""
        IO.popen("ti2json project_src/developers") {|fp|
             text += fp.read
        }
        json = JSON.parse text
        return json
    end

end
end
