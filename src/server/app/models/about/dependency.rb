module About
class Dependency
    include ActiveModel::AttributeMethods

    attr_accessor :name, :image, :code

    def self.all
        text = ""
        IO.popen("ti2json project_src/dependencies") {|fp|
             text += fp.read
        }
        json = JSON.parse text
        res  = []
        json["box"].each do |obj|
            dep = Dependency.new;
            dep.name  = obj["name"]
            dep.image = obj["image"]
            dep.code  = obj["code"]
            res.push dep
        end
        return res
    end


end
end
