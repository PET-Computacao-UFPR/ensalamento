module About
class Task
    include ActiveModel::AttributeMethods

    attr_accessor :id, :name, :description, :box

    def initialize(raw={})
        @id          = raw["id"]
        @name        = raw["brief"]
        @description = raw["description"]
        @box         = raw["box"]
    end

    def path
        return "/about/tasks/#{@id}"
    end

    def tasks
        if @tasks.blank?
            @tasks = []
            @box.each do |obj|
                @tasks.push Task.new(obj)
            end
        end
        return @tasks
    end



    def self.all
        res  = []
        json = self.load_data
        json["box"].each do |obj|
            res.push Task.new(obj);
        end
        return res
    end


    def self.find(id)
        json = self.load_data
        json["box"].each do |obj|
            if obj["id"] == id
                return Task.new(obj)
            end
        end
        return nil
    end



    def self.load_data
        text = ""
        IO.popen("ti2json project_src/tasks") {|fp|
             text += fp.read
        }
        json = JSON.parse text
        return json
    end


end
end
