class Block < ActiveRecord::Base
  include(Anode)

  # searchkick
  #searchkick autocomplete: ['code']

  has_many   :departments, :dependent => :nullify
  has_many   :courses,     :dependent => :nullify
  has_many   :rooms,       :dependent => :destroy
  belongs_to :sector

  #searchkick callbacks: :async, text_middle: ['name']
  #searchkick callbacks: :async

  # Block.reindex

  # def search_query
  #   if params[:query].present?
  #     @blocks = Block.search(params[:query], page: params[:page]).map(&:code)
  #   else
  #     @blocks = Block.all.page params[:page]
  #   end
  # end


  # before_save :call_clodovil

  #validates_presence_of :name, :code, :department
  validates_presence_of :code
  validates_uniqueness_of :code


  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end

  # def call_clodovil
  #   code = clodovil(:code)
  # end


#  searchkick callbacks: :async, text_middle: ['name']
#  after_commit :reindex_dependencies
#  def reindex_dependencies
#    department.reindex unless department.blank?
#  end

  def self.distances
      blocks = Block.all
      distances = []
      for i in 0..blocks.size-1
          line = []
          lat1 = blocks[i].latitude
          log1 = blocks[i].longitude
          for j in 0..blocks.size-1
              lat2 = blocks[j].latitude
              log2 = blocks[j].longitude
              lat2 = (lat1 - lat2) * (lat1 - lat2)
              log2 = (log1 - log2) * (log1 - log2)
              dist = Math.sqrt(lat2+log2) * 100000.0
              if ( dist < 10000.0 )
                  line.push dist.to_i
              else
                  line.push "*"
              end
          end
          distances.push line
      end
      return distances
  end


  def get_edges(funcao)
    allowed=["rooms"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      raise "Edge not defined"
    end
  end


end
