class Sector < ActiveRecord::Base
  include(Anode)

  has_one  :user
  has_many :departments
  has_many :blocks

  validates_presence_of :name
  validates_uniqueness_of :code

  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end

  def classrooms(roomtypes=[])
    res = []
    self.blocks.each do |block|
      if roomtypes.size > 0
        roomtypes.each do |roomtype|
          res += block.rooms.where(["can_use='t' and room_type_id=?",roomtype])
        end
      else
        res += block.rooms.where(["can_use='t'"])
      end
    end
    return res
  end

  def klasses(seasons=[])
    res = []
    departments.each do |department|
      department.subjects.each do |subject|
        if seasons.size > 0
          seasons.each do |season|
            res += subject.klasses.where(["season_id=? AND klass_id is null",season])
          end
        else
          res += subject.klasses.where(["klass_id is null"])
        end
      end
    end
    return res
  end


    def schedules(seasons=[],day)
      schedules = []
      klassespkg = self.klasses(seasons)
      klassespkg.each do |klass|
        if ( klass.vacancy > 1 && klass.subject.room_type.present? )
          if ( klass.subject.room_type_id < 5 and klass.vacancy > 3 )
            klass.schedules.where(["requireroom='t'"]).each do |sched|
              schedules.push(sched)
            end
          end
        end
      end
      return schedules
    end

  def rooms
      res = []
      self.blocks.each do |block|
        block.rooms.each do |room|
          res.push room
        end
      end
      res
  end

  def get_edges(funcao)
    allowed=["blocks"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      raise "error #{funcao} not found"
    end
  end


end
