class Professor < ActiveRecord::Base
  include(Anode)

  #searchkick
  #searchkick autocomplete: ['name'], word_start: [:name]
  #searchkick text_start: [:name], suggest: [:name]
  #searchkick word_start: [:name]
  #searchkick text_start: [:name], autocomplete: ['name']

  #searchkick autocomplete: ['name']



  belongs_to :department
  has_many   :klasses,  :dependent => :nullify


  # Block.reindex
  #searchkick callbacks: :async, text_middle: ['discipline_name', 'professor']

  # Run: $ rake searchkick:reindex CLASS=Professor
  #after_commit :reindex_dependencies

  validates_presence_of :name, :code
  validates_uniqueness_of :code


  # def reindex_dependencies
  #  Professor.reindex unless Professor.blank?
  # end

  def self.create(params={})
    professor = Professor.new(params)
    if professor.code.blank?
      vetname = professor.name.downcase.strip.split(" ")
      code = ""
      for i in 0..vetname.size()-2
        code += vetname[i][0]
      end
      code += vetname.last
      professor.code = professor.create_unique_code(Professor,code)
    end
    professor.save
  end


  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end


  def get_edges(funcao)
    allowed=["klasses"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      return "error #{funcao} not found in #{rid}"
    end
  end

  def professor_path
    return ""
  end

end
