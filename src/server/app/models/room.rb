class Room < ActiveRecord::Base
  include(Anode)

  belongs_to :block
  belongs_to :room_type
  has_many   :appointments
  has_many   :klasses, through: :appointments

  has_many   :schedules,  :dependent => :nullify

  #searchkick callbacks: :async, autocomplete: ['code', 'block', 'sector']

  validates_presence_of :code, :size, :block_id
  validates_uniqueness_of :code


  def self.getValidRooms
      return Room.where("(room_type_id>=1 and room_type_id<=4) and size>0 and can_use='t'")
  end

#  def rid
#    res = "rooms/"
#    res += (code.present?) ? code : id.to_s
#    return res;
#  end

  def schedules_by_week()
    ini = Date.today-6
    fim = ini + 7;
    res = []
    schedules = self.schedules   #Schedule.where("room_id = ?", room_id).all
    schedules.each do |sched|
      #if sched.session.ini<=ini || sched.session.end>=fim
        res.push sched.to_json(ini)
      #end
    end
    return res
  end



  def select_view
     text = self.code + "("+self.size.to_s
     if !self.can_use
         text += " - Restrita"
     end
     return text + ")"
  end

  def toTiObj
      text  = "Room{code='" + self.code.gsub(" ","_")+"';"
      text += "id="+self.id.to_s+";"
      text += "type="+self.room_type_id.to_s+";"
      text += "size="+self.size.to_s+";"
      text += "lat="+self.block.latitude.to_s+";"
      text += "log="+self.block.longitude.to_s+"}"
      return text
  end

  def toText
      text = ""
      text += self.code.gsub(" ","_")+" "
      text += self.id.to_s+" "
      text += self.size.to_s+" "
      text += self.block.latitude.to_s+" "
      text += self.block.longitude.to_s+" "
      return text
  end

  def toYaml
      text = "   "
      text += "- {code: \'" + self.code + "\', "
      text += "id: " + self.id.to_s + ", "
      text += "type: " + self.room_type_id.to_s + ", "
      text += "size: " + self.size.to_s + ", "
      text += "block: '#{block.code}', "
      text += "lat: " + self.block.latitude.to_s + ", "
      text += "log: " + self.block.longitude.to_s + "}"
      return text
  end



  def week()
      week = Array.new(17){ Array.new(0,7) }
      for i in 0..16
        for j in 0..6
          week[i].push(nil)
        end
      end

      self.schedules.each do |sched|
        for i in sched.ini_norm .. sched.end_norm-1
          week[ i-7 ][ sched.weekday-2 ] = sched.klass
        end
      end
      return week
  end


  def klasses
    res = []
    self.klasses_hash.each do |pair|
      if pair[1].present?
        res.push( pair[1] )
      end
    end
    return res
  end


  def professors
    res = []
    klasses_hash.each do |pair|
      if pair[1].present?
        res.push( pair[1].professor )
      end
    end
    res
  end



  def get_edges(funcao)
    allowed=["schedules", "week", "klasses", "professors"]
    if allowed.include?(funcao)
      return send( funcao )
    else
      raise "error #{funcao} not found"
    end
  end

  def self.findCode(code)
    begin
      if Integer(code); find(code); end
    rescue
      find_by(code:code)
    end
  end

  def klasses_hash
    data = {}
    self.schedules.each do |sched|
      data[ sched.klass_id ] = sched.klass
    end
    data
  end
end
