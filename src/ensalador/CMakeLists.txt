cmake_minimum_required(VERSION 2.6)
project(ensalador)

SET(CMAKE_C_COMPILER  gcc)

add_executable(${PROJECT_NAME} src/main.c src/kuhn.c src/list.c)

target_link_libraries(${PROJECT_NAME} m)

install(TARGETS ${PROJECT_NAME} DESTINATION bin)
