

/*=====================================  HEADER  ======================================*/

#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kuhn.h"

/*-------------------------------------------------------------------------------------*/


/*======================================  SALAS  ======================================*/ 
//lista de sala

void inicLista(listaSalas *lista){//inicializacao da lista de salas
	lista->inicio = (Sala *) malloc (sizeof(Sala));
	lista->ultimo = lista->inicio;
	lista->inicio->prox = NULL;
	lista->Tam = 0;
}

void insereSalas(listaSalas *lista){//insere as salas lidas da entrada na lista 
	int i,p;
	scanf("%d\n",&p);
	for (i =0; i < p;i++){
		lista->ultimo->prox = malloc (sizeof(Sala));
		lista->ultimo = lista->ultimo->prox;
		scanf("%s %d %d %f %f\n",&lista->ultimo->numSala,&lista->ultimo->COD_S,&lista->ultimo->capacidade,&lista->ultimo->lat,&lista->ultimo->lon);
		// le a informacao da enrada
		lista->ultimo->prox = NULL;
		lista->Tam ++;
		if (lista->ultimo->lat == 0.0000000){//verificacao da corretude de entrada, nao podem haver salas nas coordenadas (0.0,0.0)
			printf("Latitide da sala %s eh 0\n",lista->ultimo->numSala);//indica qual sala possui latitude errada
			exit(EXIT_FAILURE);//para o programa
		}
		if (lista->ultimo->lon == 0.0000000){
			printf("Longitude da sala %s eh 0\n",lista->ultimo->numSala);// indica qual sala tem a longitude errada
			exit(EXIT_FAILURE);//para o programa
		}
	}
}

void EncerraL(listaSalas *lista){//libera todos os elementos da lista com execao do nodo inicial
    Sala *aux;
    aux = lista->inicio;
    while (aux != NULL){
        lista->inicio = lista->inicio->prox;
        free(aux);
        aux = lista->inicio;
    }
}

void imprimeLista(listaSalas lista){//imprime a lista de salas, usada principalmente para debugacao
	Sala *aux;
    aux = lista.inicio->prox;
    while(aux != NULL){
	printf("%s %d %d %f %f\n",aux->numSala,aux->COD_S,aux->capacidade,aux->lat,aux->lon); 
        aux = aux->prox;
    }
}

void RemoveSala(Sala* s,listaSalas* cln){//remove uma sala da lista de salas, necessario para tirar salas ocupadas/reservadas antes de realizar o ensalamento
	if (cln->Tam <=0){
	}
	else{
		Sala *aux,*aux2;
		aux = cln->inicio;
		while (aux->prox != NULL &&  strcmp(aux->prox->numSala,s->numSala)  )
			aux = aux->prox;
		aux2 = aux->prox;
		if (strcmp(aux->numSala,s->numSala)){
			cln->Tam --;
			aux->prox = aux2->prox;
			free(aux2);
		}
	}
}

void clone (listaSalas* s,listaSalas* clonada){//cria um clone da lista de salas para ser modificado sem estagar o original
	Sala *Aux = s->inicio->prox;
	int i;
	for (i = 0; i < s->Tam;i++){ 
		clonada->ultimo->prox = malloc (sizeof(Sala));
		clonada->ultimo = clonada->ultimo->prox;
		//copia as informacoes necessarias 
		strcpy(clonada->ultimo->numSala,Aux->numSala);
		clonada->ultimo->COD_S = Aux->COD_S;
		clonada->ultimo->capacidade = Aux->capacidade;
		clonada->ultimo->lat = Aux->lat;
		clonada->ultimo->lon = Aux->lon;

		clonada->ultimo->prox = NULL;
		Aux = Aux->prox;
	}
	clonada->Tam = s->Tam;
}



/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/



/*=====================================  TURMAS  ======================================*/ 
//Funções das listas de turmas

void inicListaT(listaTurmas *lista){//mesma coisa que a lista de salas porem para as turmas
	lista->inicio = (Turma *) malloc (sizeof(Turma));
	lista->ultimo = lista->inicio;
	lista->inicio->prox = NULL;
	lista->Tam = 0;
}


void insereTurmas(listaTurmas *lista[]){//le as turmas da entrada de dados e as separa por horario, cada horario eh referente a uma lista diferente de turmas
	int i,p;
	Turma T;
	scanf("%d\n",&p);
	for (i =0; i < p;i++){
		scanf("%s %d %d %d %f %f  ",&T.COD_D,&T.COD_T,&T.Horario,&T.vagas,&T.lat,&T.lon);//como n eh possivel saber o horario da turma pre-leitura os dados sao guardados em uma turma temporaria
		lista[T.Horario]->ultimo->prox = malloc (sizeof(Turma));
		lista[T.Horario]->ultimo = lista[T.Horario]->ultimo->prox;
		//copiam os dados do temporario para a lista correta
		strcpy (lista[T.Horario]->ultimo->COD_D,T.COD_D);
		lista[T.Horario]->ultimo->COD_T = T.COD_T;
		lista[T.Horario]->ultimo->Horario = T.Horario;
		lista[T.Horario]->ultimo->vagas = T.vagas;
		lista[T.Horario]->ultimo->lat = T.lat;		
		lista[T.Horario]->ultimo->lon = T.lon;

		lista[T.Horario]->ultimo->prox = NULL;
		lista[T.Horario]->Tam ++;
		//verificam corretude da entrada de dados
		if (lista[T.Horario]->ultimo->lat == 0.0000000){
			printf("Latitide da turma %s eh 0\n",lista[T.Horario]->ultimo->COD_D);
			exit(EXIT_FAILURE);
		}
		if (lista[T.Horario]->ultimo->lon == 0.0000000){
			printf("Longitude da turma %s eh 0\n",lista[T.Horario]->ultimo->COD_D);
			exit(EXIT_FAILURE);
	}
	}
}

void EncerraLT(listaTurmas *lista){//libera todos os nodos de uma lista com execacao do inicial
    Turma *aux;
    aux = lista->inicio;
    while (aux != NULL){
        lista->inicio = lista->inicio->prox;
        free(aux);
        aux = lista->inicio;
    }
}

void imprimeListaT(listaTurmas lista){//imprime uma lista de turmas, usado principalemnte para debugacao 
    Turma *aux;
    aux = lista.inicio->prox;
    while(aux != NULL){
	printf("%s|%d|%d|%d|%f|%f\n",aux->COD_D,aux->COD_T,aux->Horario,aux->vagas,aux->lat,aux->lon); 
        aux = aux->prox;
    }
}

void imprimeListaTV(listaTurmas lista){
    Turma *aux;
    aux = lista.inicio->prox;
    while(aux != NULL){
	printf("%d|%d|%f|%d\n",aux->COD_T,-1,0.0,0); 
        aux = aux->prox;
    }
}

