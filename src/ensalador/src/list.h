

/*=====================================  HEADER  ======================================*/

#pragma once

/*-------------------------------------------------------------------------------------*/

/*======================================  SALAS  ======================================*/ 
//Funções e estruturas referentes a lista de salas

typedef struct _Sala{
	char numSala[32];//4
	int COD_S;
	int capacidade;
	float lat;
	float lon;
	struct _Sala *prox ;
}Sala;

typedef struct _listaSalas{
    Sala *inicio,*ultimo;
	int Tam;
}listaSalas;

void inicLista(listaSalas *);
void insereSalas(listaSalas *);
void EncerraL(listaSalas *);
void imprimeLista(listaSalas);
void clone (listaSalas*,listaSalas*);
void RemoveSala(Sala*,listaSalas*);
/*-------------------------------------------------------------------------------------*/


/*=====================================  TURMAS  ======================================*/ 
//Funções e estrururas referentes as listas de turmas

typedef struct _turma{
	char COD_D[8];
        int COD_T;
	int Horario;//usa o horario pra definir em q vetor de horario estara a turma
	int vagas;
	float lat;
	float lon;
	struct _turma *prox;
 }Turma;

typedef struct _listaTurmas{
    Turma *inicio,*ultimo;
	int Tam;
}listaTurmas;

void inicListaT(listaTurmas *);
void insereTurmas(listaTurmas *[]);
void EncerraLT(listaTurmas *);
void imprimeListaT(listaTurmas);
void imprimeListaTV(listaTurmas);


/*-------------------------------------------------------------------------------------*/

