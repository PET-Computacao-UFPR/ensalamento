/*=====================================  HEADER  ======================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "kuhn.h"
#include "list.h"


char* HORARIOS[] = {
	"Segunda		 7:30 -  9:30",
	"Segunda		 9:30 - 11:30",
	"Segunda		11:30 - 13:30",
	"Segunda		13:30 - 15:30",
	"Segunda		15:30 - 17:30",
	"Segunda		17:30 - 19:30",
	"Segunda		19:30 - 21:30",
	"Segunda		21:30 - 23:30",
	"Terça		 7:30 -  9:30",
	"Terça		 9:30 - 11:30",
	"Terça		11:30 - 13:30",
	"Terça		13:30 - 15:30",
	"Terça		15:30 - 17:30",
	"Terça		17:30 - 19:30",
	"Terça		19:30 - 21:30",
	"Terça		21:30 - 23:30",
	"Quarta		 7:30 -  9:30",
	"Quarta		 9:30 - 11:30",
	"Quarta		11:30 - 13:30",
	"Quarta		13:30 - 15:30",
	"Quarta		15:30 - 17:30",
	"Quarta		17:30 - 19:30",
	"Quarta		19:30 - 21:30",
	"Quarta		21:30 - 23:30",
	"Quinta		 7:30 -  9:30",
	"Quinta		 9:30 - 11:30",
	"Quinta		11:30 - 13:30",
	"Quinta		13:30 - 15:30",
	"Quinta		15:30 - 17:30",
	"Quinta		17:30 - 19:30",
	"Quinta		19:30 - 21:30",
	"Quinta		21:30 - 23:30",
	"Sexta		 7:30 -  9:30",
	"Sexta		 9:30 - 11:30",
	"Sexta		11:30 - 13:30",
	"Sexta		13:30 - 15:30",
	"Sexta		15:30 - 17:30",
	"Sexta		17:30 - 19:30",
	"Sexta		19:30 - 21:30",
	"Sexta		21:30 - 23:30",
	"Sabado		 7:30 -  9:30",
	"Sabado		 9:30 - 11:30",
	"Sabado		11:30 - 13:30",
	"Sabado		13:30 - 15:30",
	"Sabado		15:30 - 17:30",
	"Sabado		17:30 - 19:30",
	"Sabado		19:30 - 21:30",
	"Sabado		21:30 - 23:30"
};



#define NHorarios sizeof(HORARIOS)/sizeof(char*)
#define PUNISH 50000 //custo de colocar uma turma em uma sala ficticia 
#define NOMOREROOM 30000 //custo de colocar uma turma em uma sala em que ela nao cabe

#ifdef DEBUG
#  define debug(X) fprintf(stderr, "\033[31m%s\033[m\n", X)
#else
#  define debug(X) 
#endif


double FATOR_DIST = 10.0;
double FATOR_FREE = 10.0;
double MAX_DISTANCE = 500.0;

/*-------------------------------------------------------------------------------------*/



/*=====================================  Ligação  =====================================*/

double CalcDist(Sala* s, Turma* t){//uses real world coordinates to calculate the distance betwen classrooms
	double X,X1,X2,Y,Y1,Y2,Z,Z1,Z2;

	X1 = s->lat;
	X2 = t->lat;
	Y1 = s->lon;
	Y2 = t->lon;

	X = X1 - X2;
	Y = Y1 - Y2;
		
	Z1 = X * X;
	Z2 = Y * Y;
	
	//Z1 = Z1 * 10000000000000.0;
	//Z2 = Z2 * 10000000000000.0;
	Z = Z1 + Z2;	

	double dist = sqrt(Z) * 100000.0;   // (*100000.0) Convert the distance to meters 
	return dist;
}

#define NORM_FATOR 1000.0

double CalcFree(Sala* s, Turma* t){//calculates how free the room is
	double free =  s->capacidade - t->vagas;
	double norm = (free/(double)s->capacidade) * NORM_FATOR;
	return norm;
}

int CalcCusto(Sala* s, Turma* t){//calculates the cost of allocating classroom s to class t based on distance and size of the classroom
	if (s->capacidade < t->vagas)
		return NOMOREROOM;

	double dist = CalcDist(s,t);
	double dist_norm = (dist/MAX_DISTANCE) * NORM_FATOR;
	double free = CalcFree(s,t);

//printf("a: %f %f\n", FATOR_DIST, FATOR_FREE);
//printf("f: %f %f\n", dist_norm*FATOR_DIST, free);
	return dist_norm*FATOR_DIST + free*FATOR_FREE;
}



void ImprimeElemLista(int i,int j, listaSalas sala, listaTurmas turma,int f){//prints a specifc pair of class/classroom pair from the given lists
	int l,k;
	Sala *auxS = sala.inicio->prox;
	Turma *auxT;
	auxT = turma.inicio->prox;

	for (l = 0;l < i;l++)
		auxS = auxS->prox;

	if(j < turma.Tam){
		//printf("%s %d - ",auxS->numSala,auxS->COD_S);
		for (k=0;k<j;k++)
			auxT = auxT->prox;
		//printf("%s %d - %s %d\n",auxS->numSala,auxS->COD_S,auxT->COD_D,auxT->COD_T);

		int tam = auxS->capacidade - auxT->vagas;
		float dist = CalcDist(auxS,auxT);
		if (f == 1)
			fprintf(stderr, "turma ensalada ( %d  ) grande demais para a sala ( %d )\n",auxT->COD_T,auxS->COD_S);
		printf("%d|%d|%f|%d\n",auxS->COD_S,auxT->COD_T,dist,tam);
	}
//	else
//		printf("%d|%d|%f|%d\n",auxS->COD_S,-1,0.0,0);//sala esta vazia
}

void print(cell** t, size_t n, size_t m, ssize_t** assignment,listaSalas* sala,listaTurmas* turma){//semi-black box function, used to print the result of the classroom asignment, moddified to print only the assigned pair of class/classroom instead of the cost of every class to every classroom
    size_t i, j;    
    ssize_t** assigned = malloc(n * sizeof(ssize_t*));
    for (i = 0; i < n; i++){
        *(assigned + i) = malloc(m * sizeof(ssize_t));
		for (j = 0; j < m; j++)
			*(*(assigned + i) + j) = 0;
    }

    if (assignment != null)
        for (i = 0; i < n; i++)
	    (*(*(assigned + **(assignment + i)) + *(*(assignment + i) + 1)))++;    
    for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
		    	if (*(*(assigned + i) + j)){
					if ((cell)(*(*(t+i)+j)) == NOMOREROOM)
						ImprimeElemLista(i,j,*sala,*turma,1);
					else
						ImprimeElemLista(i,j,*sala,*turma,0);
						
					
				}
        }
		free(*(assigned + i));
    }
    free(assigned);
}


void ALGHung(listaSalas* sala,listaTurmas* turma){//Hungarian algorithim, calculates the optimal assignment for the class/classroom pairs
	Sala *AuxS = sala->inicio->prox;
	Turma *AuxT = turma->inicio->prox;
    size_t i, j,k;
    size_t n = sala->Tam;  
    size_t m = n;


    cell** t = malloc(n * sizeof(cell*));
    cell** table = malloc(n * sizeof(cell*));
	i=0;
	while (i<sala->Tam)
	{
	    *(t + i) = malloc(n * sizeof(cell));
	    *(table + i) = malloc(n * sizeof(cell));

		j=0;
		k=turma->Tam;
		AuxT = turma->inicio->prox;
		while(j< turma->Tam){
	        	*(*(table + i) + j) = *(*(t + i) + j) = (cell) CalcCusto(AuxS,AuxT);//(random() & 63);			
			if (AuxT->prox != NULL)
				AuxT=AuxT->prox;
			j++;
		}
		while (k < sala->Tam){
			*(*(table + i) + j) = *(*(t + i) + j) = (cell) PUNISH;//(random() & 63);			
			k++;
			j++;
		}


		AuxS = AuxS->prox;
	    i++;
	}

    ssize_t** assignment = kuhn_match(table, n, m);
    print(t, n, m, assignment,sala,turma);
   
    free(assignment);
    free(table);
    free(t);
}

void imprimeH(int z){//prints the scheduled time for a certain group of classes to happen
	if ( z >= NHorarios ){
		fprintf(stderr, "Horario nao existente\n");
		exit(0);
	}
	printf("%s\n",HORARIOS[z]);
}


/*-------------------------------------------------------------------------------------*/

void RemoveSalas(listaSalas *cln){//remove ocupied classrooms from the clones, used so ocupied classroom are not asigned to any classes
	int i,ND;
	Sala *RM = (Sala*)malloc (sizeof(Sala));
//	printf("\n\n\n");
	scanf("%d\n",&ND);
	for (i = 0; i < ND;i++){
		scanf("%s %d %d %f %f\n",RM->numSala,&RM->COD_S,&RM->capacidade,&RM->lat,&RM->lon);
		RemoveSala(RM,cln);
	}

	free(RM);
}

/*======================================  MAIN  =======================================*/

int main(int argc, char* argv[]){
	int z,i;
	if ( argc < 3 ){
		printf("syntax: %s distance:double free:double",argv[0]);
		return 1;
	}

	FATOR_DIST = atof(argv[1]);
	FATOR_FREE = atof(argv[2]);
	
	
	listaSalas *sala;
	listaSalas *clones[NHorarios];
	sala = (listaSalas *) malloc (sizeof(listaSalas));
	inicLista(sala);
	insereSalas (sala);
	
	for (z = 0; z < NHorarios;z++){
		clones[z] = (listaSalas *) malloc (sizeof(listaSalas));
		inicLista(clones[z]);
		clone(sala,clones[z]);
	}

	listaTurmas *turma[NHorarios];
	for(z=0;z<NHorarios;z++){
		turma[z] = (listaTurmas *) malloc (sizeof(listaTurmas));
		inicListaT(turma[z]);
	}
	insereTurmas (turma);


	
//	imprimeLista(*sala);
	for(z = 0; z < NHorarios; z++){
		RemoveSalas(clones[z]);
//		imprimeLista(*clones[z]);
	}
	/*printf("\n\n trocando de lista \n\n");
		printf("\n\n");	
	}
*/
	for(i=0;i<NHorarios;i++){
		printf("\n\n\n");
		if (turma[i]->Tam > sala->Tam){
			fprintf(stderr,"nao eh possivel ensalar, mais turmas do que salas disponiveis");
			imprimeListaTV(*turma[i]);
		} else {
			if (turma[i]->Tam >0)
				ALGHung(clones[i],turma[i]);
			//else
				//printf("Sem turmas neste horario\n");
		}
	}

	EncerraL(sala);
	free(sala);
	for(z=0;z<NHorarios;z++){
		EncerraL(clones[z]);
		free(clones[z]);
		EncerraLT(turma[z]);
		free(turma[z]);
	}

	return(0);
}

/*-------------------------------------------------------------------------------------*/
