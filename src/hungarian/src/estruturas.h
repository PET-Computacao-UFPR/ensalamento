/*=========================================================================*/

#pragma once

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <iostream>
#include <yaml-cpp/yaml.h>
#include <cmath>
#include "../src/munkres.h"
#include "../src/matrix.h"

#define NORM_FATOR 1.0
#define NOMOREROOM 30000 
#define DAYSIZE 24
#define WEEKSIZE 8
#define OCCUPIED 50000

extern double FATOR_DIST;
extern double FATOR_FREE;
extern double MAX_DISTANCE;
extern bool WithPrio;

/*-------------------------------------------------------------------------*/

/*########################################################################

ANY AND ALL COMMENTS MENTIONIG CLASS REFER TO A SCHEDULED CLASS, NOT THE CLASS DATA FORMAT

########################################################################*/

class Sala{//class that stores information about a classroom
	std::string numSala;//classroom name
	int COD_S;//code
	int size;
	int type;//mesa com braço, mesa e carteira, prancheta, etc
	double lat;//latitude coordinate for the classroom
	double lon;//longitude coordinate for the classroom
	bool occupied [WEEKSIZE][DAYSIZE];//for each classroom there's a matrix that indicates  wheter the classroom is occupied in a certain timeslot
public:
	Sala(){//when a classroom is created the ocupation matrix starts with it beeing unoccupied for all existing timeslots 
		for (int i = 0; i < WEEKSIZE;i++){
			for(int j=0;j < DAYSIZE;j++){
				occupied[i][j] = false;	
			}
		}	
	}
	void set (std::string nome, int codigo, int capacidade,int tipo, double lati,double longi){//set the values in a classroom read from elsewhere
		numSala = nome;
		COD_S = codigo;
		type = tipo;
		size = capacidade;
		lat = lati;
		lon = longi;
	}
	void print(std::ofstream& out){//print the needded information from the classroom
		out << COD_S;
	}
	void printEst(std::ofstream& out){//print info for statistics generation
		out <<" "<< numSala;
	}
	int getID(){
		return COD_S;
	}
	double getLat(){
		return lat;
	}
	std::string getName(){
		return numSala;
	}

	double getLon(){
		return lon;
	}
	int getCap(){
		return size;
	}
	bool getOcc(int day,int hour){//see wether the classroom is occupied at day "day" and hour "hour"
		return occupied[day][hour];
	}
	void setOcc(int day, int ini, int end){//set the classroom as occuppied in a specified timeslot
		for (int j = ini; j < end; j++){
			occupied[day][j] = true;
		}
	}

};

class Turma{// stores information about a class
	std::string COD_D;//disipline name as well as class code, e.g (ci055:A,te154:B:C)
	int COD_T;//individual ID for each class
	int dia;//day in which the class meets
	int ini;//hour at which the meeting begins
	int end;//hour at which the meeting ends
	int vagas;//maximum amount of students the class can have
	int K_id;//shared ID for diferent classes from the same discipline
	int type;
	double lat;//latitude coordinates
	double lon;//longitude coordinates
	Sala * assigned;//classroom that has been assigned to the class
	int suggested;//code of the classroom suggested for this class to be assigned to
public:
	Turma(){}
	Turma(std::string C_D, int C_T, int day,int I,int E,int nv,int Id,int tipo, double lati,double longi,int sug = 0){//initialize classes same way as classrooms 
		Turma turma;
		COD_D = C_D;
		COD_T = C_T;
		dia = day;
		ini = I;
		end = E;
		vagas = nv;
		K_id = Id;
		type = tipo;
		lat = lati;
		lon = longi;
		suggested = sug;
		assigned = NULL;
	}
	Turma(YAML::Node n){//initate class values based on a YAML node read from input
		this->COD_D = n["code"].as<std::string>();
		this->COD_T = n["id"].as<unsigned int>();
		this->dia = n["day"].as<unsigned int>();
		this->ini = n["ini"].as<unsigned int>();
		this->end = n["end"].as<unsigned int>();
		this->vagas = n["size"].as<unsigned int>();
		this->K_id = n["klass_id"].as<unsigned int>();
		this->type = n["type"].as<unsigned int>();
		this->lat = n["lat"].as<double>();
		this->lon = n["log"].as<double>();
		this->assigned = NULL;
		if(n["suggested"]){
			this->suggested = n["suggested"].as<unsigned int>();
		}
		else{
			this->suggested = 0;
		}
	}
	void print(std::ofstream& out){//print necessary information about the class
		out << COD_T;
	}
	void printEst(std::ofstream& out){
		out << COD_D;
	}
	void setAssigned(Sala * s){//set a classroom as the one assinged to this class
		assigned = s;
	}
	Sala * getAssigned(){//
		return assigned;
	}
	void setNewIni(int NI){//set new time to start the class, left unused
		ini = NI;
	}
	void setNewEnd(int NE){//set new time to end the class, left unused
		end = NE;
	}
	double getLat(){
		return lat;
	}
	double getLon(){
		return lon;
	}
	double getNv(){
		return vagas;
	}
	int  getD(){
		return dia;
	}
	int getIni(){
		return ini;
	}
	int getEnd(){
		return end;
	}
	int getId(){
		return K_id;
	}
	int getSuggestion(){
		return suggested;
	}
	int getCOD(){
		return COD_T;
	}
	std::string getName(){
		return COD_D;
	}
};

class Turmas{//used to facilitate declarations in Semana
	public:
	std::vector <Turma> turmas;
};
class Semana{//creates a matrix of class vectors, each class in the vector relative to the day of the class as well as the starting time of the class
	Turmas data[8][24];
public:
	Turmas& at(int day,int hour){//returns a vector of classes at a certain day and hour of the week
		return (data[day][hour]);
	}
};

class TurmasIndex{//map showing the general occupation of classrooms by classes, used to assure classes meeting more than once a week meet in the same classroom
	std::map <int, std::vector<Turma*> > share;
public:
	void mark(Turma* t){//mark in the map another classroom assigned to a class
		share[t->getId()].push_back(t);
	}
	std::vector<Turma*> findSala(Turma& t){//returns the vector containing class t, for comparison on the assignments on the other times the class has met in the week
 		std::map<int,std::vector<Turma*> >::iterator it;
		std::vector<Turma*> empty;
		it = share.find(t.getId());
  		if (it != share.end()){
  			return (it->second);
		} else{
			return(empty);
		}	
	}  
	std::map<int,std::vector<Turma*> >& getShared(){
		return share;
	}
};

class Ensalador{//makes the classroom assignment all classes needed
public:
	std::vector <Sala> RoomTypeList;//vector containing the classrooms
	Semana week;//matrix containig the classes
	TurmasIndex Index;//map containing the assignements for each class
	bool loadClassrooms(std::vector<Sala>* ,std::string);//load the classroom vector with data from input

	bool LoadClasses(Semana *,std::string );//load class matrix with info from input

	void FillMap(TurmasIndex* ,Semana*,int ,int);//fill the map with pointers to the classes to be assigned
	
	double CalcDist(Sala , Turma );//calculates the euclidian distance between the classroom and the central location of the class
	
	double CalcFree(Sala , Turma );//calculates how much free space the classroom will have if assigned to a class

	double isAvaliable(Sala , Turma);//verify if the classroom is avaliable for use in the timeslot needed by the class
	
	double CalcCusto(Sala& , Turma& ,TurmasIndex* );//calculates the cost of assigning a class to a classroom

	bool MakeAssignment(int,char*[]);//makes the assignment using the Munkres algorithm
	void display(int, int, int, int,Matrix<double>, std::ofstream&, std::ofstream&);//display solved assignment
};

