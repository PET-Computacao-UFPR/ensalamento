#include "estruturas.h"
#include "../src/munkres.h"
#include "../src/matrix.h"
#include "../src/estruturas.h"
#include <cstring>
double FATOR_DIST = 10.0;
double FATOR_FREE = 10.0;
double MAX_DISTANCE = 500.0;
bool WithPrio = false;

using namespace std;

bool Ensalador::loadClassrooms(vector<Sala>* RoomTypeList,string salas){//function that adds the classroms from the input into the vector of classroms to be assigned
	Sala Teste;//classroom to receive information from input and be pushed do the vector of classrooms
	string numSala;
	int COD_S;
	int cap;
	int type;
	double lat;
	double lon;

	YAML::Node baseNode = YAML::LoadFile(salas);//YAML base node, in this case, the input file
	if (baseNode.IsNull()) return false;

	YAML::Node salaTeste = baseNode["rooms"];//YAML node, in this case, the first node in the "rooms" sections of the input
	if (salaTeste.IsNull()) return false;
	
	for(YAML::const_iterator it = salaTeste.begin(); it != salaTeste.end(); ++it){//iterator that runs the "rooms" section of the input and fills in the information about the room
 	numSala = it->second["code"].as<std::string>();
	COD_S = it->second["id"].as<unsigned int>();
	cap = it->second["size"].as<unsigned int>();
	type = it->second["type"].as<unsigned int>();
	lat = it->second["lat"].as<double>();
	lon = it->second["log"].as<double>();

	Teste.set(numSala,COD_S,cap,type,lat,lon);//set the classroom with it's values
	RoomTypeList->push_back(Teste);//and adds it to the classroom vector
	}	
return true;
}


bool Ensalador::LoadClasses(Semana *week,string turmas){//Function that reads the information about the class from input and puts it in the week matrix
	int dia;
	int ini;
	int end;

	YAML::Node baseNode2 = YAML::LoadFile(turmas);//YAML base node, the input file
	if (baseNode2.IsNull()) return false;

	YAML::Node turmaTeste = baseNode2["schedules"];//YAML node, the "schedules" section of the input file
	if (turmaTeste.IsNull()) return false;

	for(YAML::const_iterator it = turmaTeste.begin(); it != turmaTeste.end(); ++it){//iterator that runs through the "schedules" 
		dia = it->second["day"].as<unsigned int>();				// section of the input and sets the information on the
		ini = it->second["ini"].as<unsigned int>();				// week matrix
		end = it->second["end"].as<unsigned int>();
		Turma turma( it->second ); 

		if (WithPrio == true){//if assignement is set to run while prioritizing assigning longer classes first, the longer classes are put in a position of the matrix that assure they'll be assigned first
			if (end-ini >= 4){
				week->at(dia,0).turmas.push_back(turma);
			}
			else if (end - ini == 3){
				week->at(dia,1).turmas.push_back(turma);
			}
			else{
				week->at(dia,ini).turmas.push_back(turma);
			}
		}
		else{//else they're assigned to theire respective time of the week
			week->at(dia,ini).turmas.push_back(turma);
		}
	}
return true;
}

double Ensalador::CalcDist(Sala s, Turma t){//uses real world coordinates to calculate the euclidian distance betwen 2 classrooms
	double X,X1,X2,Y,Y1,Y2,Z,Z1,Z2,dist;
	X1 = s.getLat();
	X2 = t.getLat();
	Y1 = s.getLon();
	Y2 = t.getLon();

	X = X1 - X2;
	Y = Y1 - Y2;
		
	Z1 = X * X;
	Z2 = Y * Y;

	Z = Z1 + Z2;	

	dist = sqrt(Z) * 100000.0;//(*100000.0) Convert the distance to meters 
	return dist;
}

double Ensalador::CalcFree(Sala s, Turma t){//calculates how much of the classroom is filled by the supposed size of the class scheduled
	double free =  s.getCap() - t.getNv();
	double norm = (free/(double)s.getCap()) * NORM_FATOR;
	return norm;
}

double Ensalador::isAvaliable(Sala s, Turma t){//Verifys if the classroom is empty at the time needed for the scheduled class
	int i = t.getD(),k=t.getEnd();
	bool isNot=false;//initiate by assuming the classroom is avaliable
	for (int j= t.getIni();j<k;j++){//for as long as the class will need the room
		isNot = isNot || s.getOcc(i,j);//should the classroom be unavaliable, at some point s.getocc() will return true, and is not will perpetualy become true
	}
	if(isNot){//if it's not avaliable, return a high assignment cost
		return OCCUPIED;
	}
	else{//else there's no cost to the assignment in terms of avaliability
		return 0.0;
	}
}

double Ensalador::CalcCusto(Sala& s, Turma& t,TurmasIndex* I){//calculates the cost of allocating classroom s to class t based on distance between classroom and geografical center of the class, capacity/size of the classroom/class, avaliability of the classroom and classroom sugestions
	
	double suggested;
	if (s.getCap() < t.getNv())
		return NOMOREROOM;
	double custo;
	double dist = CalcDist(s,t);
	double dist_norm = (dist/MAX_DISTANCE) * NORM_FATOR;
	double free = CalcFree(s,t);
	double avaliable = isAvaliable(s,t);
	if (s.getID() == t.getSuggestion()){//if the classroom is the one suggested for a class, the cost of the assignment will be lower
		suggested = 0;
	}
	else{//otherwise it'll become more expensive to make the assignment
		suggested = 10;
	}
	custo =  dist_norm*FATOR_DIST + free*FATOR_FREE + avaliable + suggested;
	vector<Turma*> inUse = I->findSala(t);
	
	for ( size_t i=0; i<inUse.size(); ++i){
		Turma* it = inUse[i];//takes a class on the map of assignments already done
		
		if( it->getCOD() != t.getCOD() ){
			if(it->getAssigned() != NULL){
				if (it->getAssigned()->getID() == s.getID()){//if the class has already been assigned to the same classroom in another day, the cost of assinment is lower, thus helping assure the assignments will remain the same through the week
					custo -= 5;
				}
			}
		}	
	}

	if (custo < 0){
		return 0;
	} else{
		return custo;
	}
}

void Ensalador::FillMap(TurmasIndex* I,Semana* week,int weeksize,int daysize){//after the classes are set in the week the ocupation map is filled with the classes in the assignment
	int s;
	for (int dia = 0; dia < weeksize;++dia){
		for (int hora = 0; hora < daysize;++hora){
			s = week->at(dia,hora).turmas.size();
			for(int i = 0; i < s;++i){
				Turma* turma_ptr = &week->at(dia,hora).turmas[i];
				I->mark(turma_ptr);
			}
		}	
	}
}

void Ensalador::display(int nrows, int ncols, int k, int i, Matrix<double> matrix, std::ofstream& output, std::ofstream& Est){
	for ( int row = 0 ; row < nrows ; row++ ) {
		for ( int col = 0 ; col < ncols ; col++ ) {
			if(matrix(row,col) == 0){
				this->RoomTypeList[row].print(output);
				this->RoomTypeList[row].printEst(Est);
				output << "  ";
				Est << "  ";
				this->week.at(k,i).turmas[col].print(output);
				this->week.at(k,i).turmas[col].printEst(Est);
				output << endl;
				Est << endl;
				//after assignment is done, classroom is marked as occupied for the duration of the class
				this->RoomTypeList[row].setOcc(this->week.at(k,i).turmas[col].getD(),this->week.at(k,i).turmas[col].getIni(),this->week.at(k,i).turmas[col].getEnd() );
				//and the class store the information about the classroom it was assigned to
				this->week.at(k,i).turmas[col].setAssigned(&this->RoomTypeList[row]);
			}
		}
	}
	output << endl;
	Est << endl;
}

bool Ensalador::MakeAssignment(int argc,char* argv[]){//reads the input file, organizes the data, makes the assignments and prints it in the output file
	int nrows,ncols;
	if ( argc < 6 ){
		printf("syntax: %s distance:double free:double input:*.YAML   output:PATH_to_output est:PATH_TO_STATISTICS_OUTPUT;optional: -p:set use priority to true\n",argv[0]);
		return 1;
	}
	if (argc == 7){
		if (!strcmp(argv[6], "-p")){
			WithPrio = true;
		}else{
		printf("syntax: %s distance:double free:double input:*.YAML   output:PATH_to_output est:PATH_TO_STATISTICS_OUTPUT;optional: -p:set use priority to true\n",argv[0]);
		return 1;
		}
	}

	ofstream output;
	ofstream Est;
	FATOR_DIST = atof(argv[1]);
	FATOR_FREE = atof(argv[2]);
	this->loadClassrooms(&this->RoomTypeList,argv[3]);
	this->LoadClasses(&this->week,argv[3]);
	this->FillMap(&this->Index,&this->week,WEEKSIZE,DAYSIZE);
	output.open(argv[4],ios::trunc);//if the file has something writen on it already, it'll be overwriten
	Est.open(argv[5],ios::trunc);//file used to generate statistics based on assignment results

	for(int k = 0; k < WEEKSIZE;k++){//for all days of the week
		for(int i = 0;i< DAYSIZE;i++){//for all times of one day
			nrows = this->RoomTypeList.size(); 
			ncols = this->week.at(k,i).turmas.size();
			if(nrows > 0 && ncols > 0){//if there are classes to assign
				Matrix<double> matrix(nrows, ncols);// Initialize cost matrix with calculated assignment cost
				for ( int row = 0 ; row < nrows ; row++ ) {
					for ( int col = 0 ; col < ncols ; col++ ) {
						matrix(row,col) = this->CalcCusto(this->RoomTypeList[row] ,this->week.at(k,i).turmas[col],&this->Index);
					}
				}
				Munkres<double> m;
				m.solve(matrix);// Apply Munkres algorithm to matrix.
				this->display(nrows,ncols,k,i,matrix,output,Est);// Display solved assignment.
			}
		}
	}
		/*		vagas << "dia: " << k << " hora: " << i << endl;
				for ( int row = 0 ; row < nrows ; row++ ) {
					int rowcount = 0;
						for ( int col = 0 ; col < ncols ; col++  ) {
							if ( matrix(row,col) == 0 )
								rowcount++;
							}
							if ( rowcount != 1 ){
								//cerr << "Row " << row << " has " << rowcount << " columns that have been matched." << endl;
								RoomTypeList[row].print(vagas);//matrix(row,col) ;
								//cout << "   -  ";
								vagas << "   ---->> IS EMPTY on day-> " <<k << "  at time-> "<<i;
								//week.at(k,i).turmas[col].print(vagas);
								//cout << endl;
								vagas << endl;
							}				
				}
				vagas << endl;

				for ( int col = 0 ; col < ncols ; col++ ) {
					int colcount = 0;
					for ( int row = 0 ; row < nrows ; row++ ) {
						if ( matrix(row,col) == 0 )
							colcount++;
					}
						if ( colcount != 1 )
						std::cerr << "Column " << col << " has " << colcount << " rows that have been matched." << std::endl;
				}
			}
		}
	}*/
	return true;
}
