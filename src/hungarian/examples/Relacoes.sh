#!/bin/bash
arq=$1
########################################################
##	Arquitetura		########################
TOTAL=$(grep " ta" $arq | wc -l )
CT=$(grep " ta" $arq| grep " CT"|wc -l )
EQ=$(grep " ta" $arq| grep " EQ"|wc -l )
PA=$(grep " ta" $arq| grep " PA"|wc -l )
PC=$(grep " ta" $arq| grep " PC"|wc -l ) 
PD=$(grep " ta" $arq| grep " PD"|wc -l )
PE=$(grep " ta" $arq| grep " PE"|wc -l )
PF=$(grep " ta" $arq| grep " PF"|wc -l )
PG=$(grep " ta" $arq| grep " PG"|wc -l )
PH=$(grep " ta" $arq| grep " PH"|wc -l ) 
PK=$(grep " ta" $arq| grep " PK"|wc -l )
PL=$(grep " ta" $arq| grep " PL"|wc -l )
PM=$(grep " ta" $arq| grep " PM"|wc -l )
PQ=$(grep " ta" $arq| grep " PQ"|wc -l )
PR=$(grep " ta" $arq| grep " PR"|wc -l )
echo -n "Arquitetura: Total -> $TOTAL  | PD(centro) -> $PD "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Construção Civil	#############################
TOTAL=$(grep " tc" $arq | wc -l )
CT=$(grep " tc" $arq| grep " CT"|wc -l )
EQ=$(grep " tc" $arq| grep " EQ"|wc -l )
PA=$(grep " tc" $arq| grep " PA"|wc -l )
PC=$(grep " tc" $arq| grep " PC"|wc -l ) 
PD=$(grep " tc" $arq| grep " PD"|wc -l )
PE=$(grep " tc" $arq| grep " PE"|wc -l )
PF=$(grep " tc" $arq| grep " PF"|wc -l )
PG=$(grep " tc" $arq| grep " PG"|wc -l )
PH=$(grep " tc" $arq| grep " PH"|wc -l ) 
PK=$(grep " tc" $arq| grep " PK"|wc -l )
PL=$(grep " tc" $arq| grep " PL"|wc -l )
PM=$(grep " tc" $arq| grep " PM"|wc -l )
PQ=$(grep " tc" $arq| grep " PQ"|wc -l )
PR=$(grep " tc" $arq| grep " PR"|wc -l )

echo -n "Construção civil: Total -> $TOTAL  | PF(centro) -> $PF "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Engenharia Ambiental	#########################
TOTAL=$(grep " tea" $arq | wc -l )
CT=$(grep " tea" $arq| grep " CT"|wc -l )
EQ=$(grep " tea" $arq| grep " EQ"|wc -l )
PA=$(grep " tea" $arq| grep " PA"|wc -l )
PC=$(grep " tea" $arq| grep " PC"|wc -l ) 
PD=$(grep " tea" $arq| grep " PD"|wc -l )
PE=$(grep " tea" $arq| grep " PE"|wc -l )
PF=$(grep " tea" $arq| grep " PF"|wc -l )
PG=$(grep " tea" $arq| grep " PG"|wc -l )
PH=$(grep " tea" $arq| grep " PH"|wc -l ) 
PK=$(grep " tea" $arq| grep " PK"|wc -l )
PL=$(grep " tea" $arq| grep " PL"|wc -l )
PM=$(grep " tea" $arq| grep " PM"|wc -l )
PQ=$(grep " tea" $arq| grep " PQ"|wc -l )
PR=$(grep " tea" $arq| grep " PR"|wc -l )
echo -n "Engenharia Ambiental: Total -> $TOTAL  | PF(centro) -> $PF "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#############################################
##	Engenharia de Bioprocessos e biotecnologia	#########################
TOTAL=$(grep " teb" $arq | wc -l )
CT=$(grep " teb" $arq| grep " CT"|wc -l )
EQ=$(grep " teb" $arq| grep " EQ"|wc -l )
PA=$(grep " teb" $arq| grep " PA"|wc -l )
PC=$(grep " teb" $arq| grep " PC"|wc -l ) 
PD=$(grep " teb" $arq| grep " PD"|wc -l )
PE=$(grep " teb" $arq| grep " PE"|wc -l )
PF=$(grep " teb" $arq| grep " PF"|wc -l )
PG=$(grep " teb" $arq| grep " PG"|wc -l )
PH=$(grep " teb" $arq| grep " PH"|wc -l ) 
PK=$(grep " teb" $arq| grep " PK"|wc -l )
PL=$(grep " teb" $arq| grep " PL"|wc -l )
PM=$(grep " teb" $arq| grep " PM"|wc -l )
PQ=$(grep " teb" $arq| grep " PQ"|wc -l )
PR=$(grep " teb" $arq| grep " PR"|wc -l )
echo -n "Engenharia de Bioprocessos e Biotecnologia: Total -> $TOTAL  | EQ(centro) -> $EQ "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#############################################
##	Engenharia de Produção		########################
TOTAL=$(grep " tp" $arq | wc -l )
CT=$(grep " tp" $arq| grep " CT"|wc -l )
EQ=$(grep " tp" $arq| grep " EQ"|wc -l )
PA=$(grep " tp" $arq| grep " PA"|wc -l )
PC=$(grep " tp" $arq| grep " PC"|wc -l ) 
PD=$(grep " tp" $arq| grep " PD"|wc -l )
PE=$(grep " tp" $arq| grep " PE"|wc -l )
PF=$(grep " tp" $arq| grep " PF"|wc -l )
PG=$(grep " tp" $arq| grep " PG"|wc -l )
PH=$(grep " tp" $arq| grep " PH"|wc -l ) 
PK=$(grep " tp" $arq| grep " PK"|wc -l )
PL=$(grep " tp" $arq| grep " PL"|wc -l )
PM=$(grep " tp" $arq| grep " PM"|wc -l )
PQ=$(grep " tp" $arq| grep " PQ"|wc -l )
PR=$(grep " tp" $arq| grep " PR"|wc -l )
echo -n "Engenharia de Produção: Total -> $TOTAL  | PM(centro) -> $PM "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Engenharia Elétrica		########################
TOTAL=$(grep " te" $arq | wc -l )
CT=$(grep " te" $arq| grep " CT"|wc -l )
EQ=$(grep " te" $arq| grep " EQ"|wc -l )
PA=$(grep " te" $arq| grep " PA"|wc -l )
PC=$(grep " te" $arq| grep " PC"|wc -l ) 
PD=$(grep " te" $arq| grep " PD"|wc -l )
PE=$(grep " te" $arq| grep " PE"|wc -l )
PF=$(grep " te" $arq| grep " PF"|wc -l )
PG=$(grep " te" $arq| grep " PG"|wc -l )
PH=$(grep " te" $arq| grep " PH"|wc -l ) 
PK=$(grep " te" $arq| grep " PK"|wc -l )
PL=$(grep " te" $arq| grep " PL"|wc -l )
PM=$(grep " te" $arq| grep " PM"|wc -l )
PQ=$(grep " te" $arq| grep " PQ"|wc -l )
PR=$(grep " te" $arq| grep " PR"|wc -l )
echo -n "Engenharia Elétrica: Total -> $TOTAL  | PK(centro) -> $PK "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Engenharia Mecanica		########################
TOTAL=$(grep " tm" $arq | wc -l )
CT=$(grep " tm" $arq| grep " CT"|wc -l )
EQ=$(grep " tm" $arq| grep " EQ"|wc -l )
PA=$(grep " tm" $arq| grep " PA"|wc -l )
PC=$(grep " tm" $arq| grep " PC"|wc -l ) 
PD=$(grep " tm" $arq| grep " PD"|wc -l )
PE=$(grep " tm" $arq| grep " PE"|wc -l )
PF=$(grep " tm" $arq| grep " PF"|wc -l )
PG=$(grep " tm" $arq| grep " PG"|wc -l )
PH=$(grep " tm" $arq| grep " PH"|wc -l ) 
PK=$(grep " tm" $arq| grep " PK"|wc -l )
PL=$(grep " tm" $arq| grep " PL"|wc -l )
PM=$(grep " tm" $arq| grep " PM"|wc -l )
PQ=$(grep " tm" $arq| grep " PQ"|wc -l )
PR=$(grep " tm" $arq| grep " PR"|wc -l )
echo -n "Engenharia Mecanica: Total -> $TOTAL  | PG(centro) -> $PG "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Engenharia Quimica	########################
TOTAL=$(grep " tq" $arq | wc -l )
CT=$(grep " tq" $arq| grep " CT"|wc -l )
EQ=$(grep " tq" $arq| grep " EQ"|wc -l )
PA=$(grep " tq" $arq| grep " PA"|wc -l )
PC=$(grep " tq" $arq| grep " PC"|wc -l ) 
PD=$(grep " tq" $arq| grep " PD"|wc -l )
PE=$(grep " tq" $arq| grep " PE"|wc -l )
PF=$(grep " tq" $arq| grep " PF"|wc -l )
PG=$(grep " tq" $arq| grep " PG"|wc -l )
PH=$(grep " tq" $arq| grep " PH"|wc -l ) 
PK=$(grep " tq" $arq| grep " PK"|wc -l )
PL=$(grep " tq" $arq| grep " PL"|wc -l )
PM=$(grep " tq" $arq| grep " PM"|wc -l )
PQ=$(grep " tq" $arq| grep " PQ"|wc -l )
PR=$(grep " tq" $arq| grep " PR"|wc -l )
echo -n "Engenharia Quimica: Total -> $TOTAL  | EQ(centro) -> $EQ "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Estatistica		########################
TOTAL=$(grep " ce" $arq | wc -l )
CT=$(grep " ce" $arq| grep " CT"|wc -l )
EQ=$(grep " ce" $arq| grep " EQ"|wc -l )
PA=$(grep " ce" $arq| grep " PA"|wc -l )
PC=$(grep " ce" $arq| grep " PC"|wc -l ) 
PD=$(grep " ce" $arq| grep " PD"|wc -l )
PE=$(grep " ce" $arq| grep " PE"|wc -l )
PF=$(grep " ce" $arq| grep " PF"|wc -l )
PG=$(grep " ce" $arq| grep " PG"|wc -l )
PH=$(grep " ce" $arq| grep " PH"|wc -l ) 
PK=$(grep " ce" $arq| grep " PK"|wc -l )
PL=$(grep " ce" $arq| grep " PL"|wc -l )
PM=$(grep " ce" $arq| grep " PM"|wc -l )
PQ=$(grep " ce" $arq| grep " PQ"|wc -l )
PR=$(grep " ce" $arq| grep " PR"|wc -l )
echo -n "Estatistica: Total -> $TOTAL  | PA(centro) -> $PA "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Expresão Grafica		########################
TOTAL=$(grep " cd" $arq | wc -l )
CT=$(grep " cd" $arq| grep " CT"|wc -l )
EQ=$(grep " cd" $arq| grep " EQ"|wc -l )
PA=$(grep " cd" $arq| grep " PA"|wc -l )
PC=$(grep " cd" $arq| grep " PC"|wc -l ) 
PD=$(grep " cd" $arq| grep " PD"|wc -l )
PE=$(grep " cd" $arq| grep " PE"|wc -l )
PF=$(grep " cd" $arq| grep " PF"|wc -l )
PG=$(grep " cd" $arq| grep " PG"|wc -l )
PH=$(grep " cd" $arq| grep " PH"|wc -l ) 
PK=$(grep " cd" $arq| grep " PK"|wc -l )
PL=$(grep " cd" $arq| grep " PL"|wc -l )
PM=$(grep " cd" $arq| grep " PM"|wc -l )
PQ=$(grep " cd" $arq| grep " PQ"|wc -l )
PR=$(grep " cd" $arq| grep " PR"|wc -l )
echo -n "Expressão Grafica: Total -> $TOTAL  | PC(centro) -> $PC "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Fisica	########################
TOTAL=$(grep " cf" $arq | wc -l )
CT=$(grep " cf" $arq| grep " CT"|wc -l )
EQ=$(grep " cf" $arq| grep " EQ"|wc -l )
PA=$(grep " cf" $arq| grep " PA"|wc -l )
PC=$(grep " cf" $arq| grep " PC"|wc -l ) 
PD=$(grep " cf" $arq| grep " PD"|wc -l )
PE=$(grep " cf" $arq| grep " PE"|wc -l )
PF=$(grep " cf" $arq| grep " PF"|wc -l )
PG=$(grep " cf" $arq| grep " PG"|wc -l )
PH=$(grep " cf" $arq| grep " PH"|wc -l ) 
PK=$(grep " cf" $arq| grep " PK"|wc -l )
PL=$(grep " cf" $arq| grep " PL"|wc -l )
PM=$(grep " cf" $arq| grep " PM"|wc -l )
PQ=$(grep " cf" $arq| grep " PQ"|wc -l )
PR=$(grep " cf" $arq| grep " PR"|wc -l )
echo -n "Fisica: Total -> $TOTAL  | PE(centro) -> $PE "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Geografia		########################
TOTAL=$(grep " gb" $arq | wc -l )
CT=$(grep " gb" $arq| grep " CT"|wc -l )
EQ=$(grep " gb" $arq| grep " EQ"|wc -l )
PA=$(grep " gb" $arq| grep " PA"|wc -l )
PC=$(grep " gb" $arq| grep " PC"|wc -l ) 
PD=$(grep " gb" $arq| grep " PD"|wc -l )
PE=$(grep " gb" $arq| grep " PE"|wc -l )
PF=$(grep " gb" $arq| grep " PF"|wc -l )
PG=$(grep " gb" $arq| grep " PG"|wc -l )
PH=$(grep " gb" $arq| grep " PH"|wc -l ) 
PK=$(grep " gb" $arq| grep " PK"|wc -l )
PL=$(grep " gb" $arq| grep " PL"|wc -l )
PM=$(grep " gb" $arq| grep " PM"|wc -l )
PQ=$(grep " gb" $arq| grep " PQ"|wc -l )
PR=$(grep " gb" $arq| grep " PR"|wc -l )
echo -n "Geografia: Total -> $TOTAL  | CT(centro) -> $CT "
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Geologia		########################
TOTAL=$(grep " gc" $arq | wc -l )
CT=$(grep " gc" $arq| grep " CT"|wc -l )
EQ=$(grep " gc" $arq| grep " EQ"|wc -l )
PA=$(grep " gc" $arq| grep " PA"|wc -l )
PC=$(grep " gc" $arq| grep " PC"|wc -l ) 
PD=$(grep " gc" $arq| grep " PD"|wc -l )
PE=$(grep " gc" $arq| grep " PE"|wc -l )
PF=$(grep " gc" $arq| grep " PF"|wc -l )
PG=$(grep " gc" $arq| grep " PG"|wc -l )
PH=$(grep " gc" $arq| grep " PH"|wc -l ) 
PK=$(grep " gc" $arq| grep " PK"|wc -l )
PL=$(grep " gc" $arq| grep " PL"|wc -l )
PM=$(grep " gc" $arq| grep " PM"|wc -l )
PQ=$(grep " gc" $arq| grep " PQ"|wc -l )
PR=$(grep " gc" $arq| grep " PR"|wc -l )
echo -n "Geologia: Total -> $TOTAL  | PH(centro) -> $PH "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Geomatica		########################
TOTAL=$(grep " ga" $arq | wc -l )
CT=$(grep " ga" $arq| grep " CT"|wc -l )
EQ=$(grep " ga" $arq| grep " EQ"|wc -l )
PA=$(grep " ga" $arq| grep " PA"|wc -l )
PC=$(grep " ga" $arq| grep " PC"|wc -l ) 
PD=$(grep " ga" $arq| grep " PD"|wc -l )
PE=$(grep " ga" $arq| grep " PE"|wc -l )
PF=$(grep " ga" $arq| grep " PF"|wc -l )
PG=$(grep " ga" $arq| grep " PG"|wc -l )
PH=$(grep " ga" $arq| grep " PH"|wc -l ) 
PK=$(grep " ga" $arq| grep " PK"|wc -l )
PL=$(grep " ga" $arq| grep " PL"|wc -l )
PM=$(grep " ga" $arq| grep " PM"|wc -l )
PQ=$(grep " ga" $arq| grep " PQ"|wc -l )
PR=$(grep " ga" $arq| grep " PR"|wc -l )
echo -n "Geomatica: Total -> $TOTAL  | CT(centro) -> $CT "
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Hidraulica e Saneamento		########################
TOTAL=$(grep " th" $arq | wc -l )
CT=$(grep " th" $arq| grep " CT"|wc -l )
EQ=$(grep " th" $arq| grep " EQ"|wc -l )
PA=$(grep " th" $arq| grep " PA"|wc -l )
PC=$(grep " th" $arq| grep " PC"|wc -l ) 
PD=$(grep " th" $arq| grep " PD"|wc -l )
PE=$(grep " th" $arq| grep " PE"|wc -l )
PF=$(grep " th" $arq| grep " PF"|wc -l )
PG=$(grep " th" $arq| grep " PG"|wc -l )
PH=$(grep " th" $arq| grep " PH"|wc -l ) 
PK=$(grep " th" $arq| grep " PK"|wc -l )
PL=$(grep " th" $arq| grep " PL"|wc -l )
PM=$(grep " th" $arq| grep " PM"|wc -l )
PQ=$(grep " th" $arq| grep " PQ"|wc -l )
PR=$(grep " th" $arq| grep " PR"|wc -l )
echo -n "Hidraulica e saneamento: Total -> $TOTAL  | PH(centro) -> $PH "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Informatica	########################
TOTAL=$(grep " ci" $arq | wc -l )
CT=$(grep " ci" $arq| grep " CT"|wc -l )
EQ=$(grep " ci" $arq| grep " EQ"|wc -l )
PA=$(grep " ci" $arq| grep " PA"|wc -l )
PC=$(grep " ci" $arq| grep " PC"|wc -l ) 
PD=$(grep " ci" $arq| grep " PD"|wc -l )
PE=$(grep " ci" $arq| grep " PE"|wc -l )
PF=$(grep " ci" $arq| grep " PF"|wc -l )
PG=$(grep " ci" $arq| grep " PG"|wc -l )
PH=$(grep " ci" $arq| grep " PH"|wc -l ) 
PK=$(grep " ci" $arq| grep " PK"|wc -l )
PL=$(grep " ci" $arq| grep " PL"|wc -l )
PM=$(grep " ci" $arq| grep " PM"|wc -l )
PQ=$(grep " ci" $arq| grep " PQ"|wc -l )
PR=$(grep " ci" $arq| grep " PR"|wc -l )
echo -n "Informatica: Total -> $TOTAL  | PA(centro) -> $PA "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################
##	Matematica	########################
TOTAL=$(grep " cm" $arq | wc -l )
CT=$(grep " cm" $arq| grep " CT"|wc -l )
EQ=$(grep " cm" $arq| grep " EQ"|wc -l )
PA=$(grep " cm" $arq| grep " PA"|wc -l )
PC=$(grep " cm" $arq| grep " PC"|wc -l ) 
PD=$(grep " cm" $arq| grep " PD"|wc -l )
PE=$(grep " cm" $arq| grep " PE"|wc -l )
PF=$(grep " cm" $arq| grep " PF"|wc -l )
PG=$(grep " cm" $arq| grep " PG"|wc -l )
PH=$(grep " cm" $arq| grep " PH"|wc -l ) 
PK=$(grep " cm" $arq| grep " PK"|wc -l )
PL=$(grep " cm" $arq| grep " PL"|wc -l )
PM=$(grep " cm" $arq| grep " PM"|wc -l )
PQ=$(grep " cm" $arq| grep " PQ"|wc -l )
PR=$(grep " cm" $arq| grep " PR"|wc -l )
echo -n "Matematica: Total -> $TOTAL  | PC(centro) -> $PC "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo"" 
#####################################################
##	Quimica		########################
TOTAL=$(grep " cq" $arq | wc -l )
CT=$(grep " cq" $arq| grep " CT"|wc -l )
EQ=$(grep " cq" $arq| grep " EQ"|wc -l )
PA=$(grep " cq" $arq| grep " PA"|wc -l )
PC=$(grep " cq" $arq| grep " PC"|wc -l ) 
PD=$(grep " cq" $arq| grep " PD"|wc -l )
PE=$(grep " cq" $arq| grep " PE"|wc -l )
PF=$(grep " cq" $arq| grep " PF"|wc -l )
PG=$(grep " cq" $arq| grep " PG"|wc -l )
PH=$(grep " cq" $arq| grep " PH"|wc -l ) 
PK=$(grep " cq" $arq| grep " PK"|wc -l )
PL=$(grep " cq" $arq| grep " PL"|wc -l )
PM=$(grep " cq" $arq| grep " PM"|wc -l )
PQ=$(grep " cq" $arq| grep " PQ"|wc -l )
PR=$(grep " cq" $arq| grep " PR"|wc -l )
echo -n "Quimica: Total -> $TOTAL  | PQ(centro) -> $PQ "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PH > 0 )); then
	echo -n "| PH -> $PH "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo"" 
#####################################################
##	Transporte		########################
TOTAL=$(grep " tt" $arq | wc -l )
CT=$(grep " tt" $arq| grep " CT"|wc -l )
EQ=$(grep " tt" $arq| grep " EQ"|wc -l )
PA=$(grep " tt" $arq| grep " PA"|wc -l )
PC=$(grep " tt" $arq| grep " PC"|wc -l ) 
PD=$(grep " tt" $arq| grep " PD"|wc -l )
PE=$(grep " tt" $arq| grep " PE"|wc -l )
PF=$(grep " tt" $arq| grep " PF"|wc -l )
PG=$(grep " tt" $arq| grep " PG"|wc -l )
PH=$(grep " tt" $arq| grep " PH"|wc -l ) 
PK=$(grep " tt" $arq| grep " PK"|wc -l )
PL=$(grep " tt" $arq| grep " PL"|wc -l )
PM=$(grep " tt" $arq| grep " PM"|wc -l )
PQ=$(grep " tt" $arq| grep " PQ"|wc -l )
PR=$(grep " tt" $arq| grep " PR"|wc -l )
echo -n "Transporte: Total -> $TOTAL  | PH(centro) -> $PH "
if (( $CT > 0 )); then
	echo -n "| CT -> $CT "
fi
if (( $EQ > 0 )); then
	echo -n "| EQ -> $EQ "
fi
if (( $PA > 0 )); then
	echo -n "| PA -> $PA "
fi
if (( $PC > 0 )); then
	echo -n "| PC -> $PC "
fi
if (( $PD > 0 )); then
	echo -n "| PD -> $PD "
fi
if (( $PE > 0 )); then
	echo -n "| PE -> $PE "
fi
if (( $PF > 0 )); then
	echo -n "| PF -> $PF "
fi
if (( $PG > 0 )); then
	echo -n "| PG -> $PG "
fi
if (( $PK > 0 )); then
	echo -n "| PK -> $PK "
fi
if (( $PL > 0 )); then
	echo -n "| PL -> $PL "
fi
if (( $PM > 0 )); then
	echo -n "| PM -> $PM "
fi
if (( $PQ > 0 )); then
	echo -n "| PQ -> $PQ "
fi
if (( $PR > 0 )); then
	echo -n "| PR -> $PR "
fi
echo  "";echo "" 
#####################################################

