/*=====================================  HEADER  ======================================*/

#pragma once

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <iostream>
#include <yaml-cpp/yaml.h>
#include <cmath>
#include <munkres/munkres.h>
#include <munkres/matrix.h>


#define NORM_FATOR 1.0
#define NOMOREROOM 30000
#define DAYSIZE 24
#define WEEKSIZE 8
#define OCCUPIED 50000

extern double FATOR_DIST;
extern double FATOR_FREE;
extern double MAX_DISTANCE;
extern bool   WithPrio;
extern bool   doYaml;
extern bool   reverse;

/*-------------------------------------------------------------------------------------*/



/*=====================================================================================*/
/*########################################################################

ANY AND ALL COMMENTS MENTIONIG CLASS REFER TO A SCHEDULED CLASS, NOT THE CLASS STRUCTURE

########################################################################*/

struct Sala{//class that stores information about a classroom
	std::string code;  //classroom name
	int id;            //code
	int size;
	int type;          //mesa com braço, mesa e carteira, prancheta, etc
	double lat;        //latitude coordinate for the classroom
	double lon;        //longitude coordinate for the classroom
	int occupied[WEEKSIZE][DAYSIZE];//for each classroom there's a matrix that indicates  wheter the classroom is occupied in a certain timeslot
	std::string block;

	Sala(){//when a classroom is created the ocupation matrix starts with it beeing unoccupied for all existing timeslots
		this->setFree();
	}

	Sala(YAML::Node node){
		id      = node["id"].as<unsigned int>();
		code  = node["code"].as<std::string>();
		size    = node["size"].as<unsigned int>();
		type    = node["type"].as<unsigned int>();
		lat     = node["lat"].as<double>();
		lon     = node["log"].as<double>();
		block   = node["block"].as<std::string>();
		this->setFree();
	}

	void setFree(){
		for (int i=0; i<WEEKSIZE; i++){
			for(int j=0; j<DAYSIZE; j++){
				occupied[i][j] = 0;
			}
		}
	}

	/*void set (std::string code, int id, int size,int tipo, double lati,double longi){//set the values in a classroom read from elsewhere
		this->code = code;
		this->id   = id;
		this->type = tipo;
		this->size = size;
		this->lat  = lati;
		this->lon  = longi;
	}*/

	int getID(){
		return this->id;
	}
	double getLat(){
		return lat;
	}
	std::string getName(){
		return code;
	}

	double getLon(){
		return lon;
	}
	int getCap(){
		return size;
	}


	bool isFree(int day, int ini, int _size){
		int size = ini+_size;
		size = (size>=DAYSIZE) ? DAYSIZE : size;
		for (int i=ini; i<size; i++){
			if ( occupied[day][i] > 0 )
				return false;
		}
		return true;
	}


	bool isOk(){
		bool res = true;
		for (int j=0; j<WEEKSIZE; j++){
			for (int i=0; i<DAYSIZE; i++){
				if ( occupied[j][i] > 1 ){
					std::cout << "[ERROR-conflito]: " << this->code  << " em " << j << " " << i << ":30\n";
					res = false;
				}
			}
		}
		return res;
	}

	bool getOcc(int day, int hour){
		return occupied[day][hour];
	}

	void setOcc(int day, int ini, int end){//set the classroom as occuppied in a specified timeslot
		for (int j = ini; j < end; j++){
			occupied[day][j] += 1;
		}
	}

	void print(std::ostream& out){
		out << this->code;
	}

	friend std::ostream& operator<<(std::ostream& out, Sala& room){
		out << room.code;
		return out;
	}

};


/********************/


class Rooms : public std::vector <Sala*> {
	bool is_indexed;
	std::map <std::string,Sala*> index;

  public:
	Rooms(){
		is_indexed = false;
	}

	Sala* find(int id){
		for(unsigned int i = 0; i < this->size(); ++i){
			if ( this->at(i)->id == id){
				return this->at(i);
			}
		}
		return NULL;
	}

	Sala* find(std::string code){
		this->create_index();
		std::map<std::string,Sala*>::iterator it = index.find(code);
		return (it != index.end()) ? (it->second) : NULL;
	}

	void setAllFree(){
		for (size_t i=0; i<this->size(); i++){
			this->at(i)->setFree();
		}
	}

	Rooms getFree(int day, int hour, int size){
		Rooms free;
		for (size_t i=0; i<this->size(); i++){
			if ( this->at(i)->isFree(day, hour, size) ){
				free.push_back(this->at(i));
			}
		}
		return free;
	}



	friend std::ostream& operator<<(std::ostream& out, Rooms& rooms){
		for (size_t i=0; i<rooms.size(); i++){
			out << *rooms[i] << std::endl;
		}
		return out;
	}

  private:
	void create_index(){
		if ( is_indexed == false ){
			for (size_t i=0; i<this->size(); i++){
				index[ this->at(i)->code ] = this->at(i);
			}
			is_indexed = true;
		}
	}

};



/*-------------------------------------------------------------------------------------*/



/*=====================================================================================*/

struct Turma{// stores information about a class
	std::string code;  //disipline name as well as class code, e.g (ci055:A,te154:B:C)
	int     id;        //individual ID for each class
	int    dia;        //day in which the class meets
	int    ini;        //hour at which the meeting begins
	int    end;        //hour at which the meeting ends
	int    vagas;      //maximum amount of students the class can have
	int    klass_id;       //shared ID for diferent classes from the same discipline
	int    type;
	double lat;                //latitude coordinates
	double lon;                //longitude coordinates
	Sala*  assigned;           //classroom that has been assigned to the class
	std::string suggested;     //code of the classroom suggested for this class to be assigned to
	std::string block;
	std::string department;
	bool do_malloc_room;

	YAML::Node yaml;

	Turma(){}

	Turma(YAML::Node n){//initate class values based on a YAML node read from input
		this->id    = n["id"].as<unsigned int>();
		this->code  = n["code"].as<std::string>();
		this->dia   = n["day"].as<unsigned int>();
		this->ini   = n["ini"].as<unsigned int>();
		this->end   = n["end"].as<unsigned int>();
		this->vagas = n["size"].as<unsigned int>();
		this->klass_id  = n["klass_id"].as<unsigned int>();
		this->type  = n["type"].as<unsigned int>();
		this->lat   = n["lat"].as<double>();
		this->lon   = n["log"].as<double>();
		this->department   = n["department"].as<std::string>();
		this->assigned  = NULL;
		this->block     = (n["block"]) ? n["block"].as<std::string>() : "";
		this->suggested = (n["suggested"]) ? n["suggested"].as<std::string>() : "";
		this->yaml  = n;
		this->do_malloc_room = (n["do_malloc"]) ? true : false;
	}

	void setAssigned(Sala* room){//set a classroom as the one assinged to this class
		if (!room) return;
		this->assigned = room;
		room->setOcc( this->dia, this->ini, this->end );
		this->yaml["assigned"] = room->code;
	}

	Sala * getAssigned(){//
		return assigned;
	}
	void setNewIni(int NI){//set new time to start the class, left unused
		ini = NI;
	}
	void setNewEnd(int NE){//set new time to end the class, left unused
		end = NE;
	}
	double getLat(){
		return lat;
	}
	double getLon(){
		return lon;
	}
	double getNv(){
		return vagas;
	}

	/*int getSuggestion(){
		return suggested;
	}*/

	void print(std::ostream& out){
		out << this->code;
	}
	friend std::ostream& operator<<(std::ostream& out, Turma& sched){
		out << sched.code;
		return out;
	}
};


/***************/


struct Turmas : std::vector <Turma*> {//used to facilitate declarations in Semana

	void statistics(std::ostream& out, int& out_total, int& out_wi_room, int& out_no_room){
		std::map <std::string,int> smap;
		int no_room = 0;

		for (size_t i=0; i<this->size(); i++){
			Turma* sched = this->at(i);
			if ( sched->assigned ){
				smap[ sched->assigned->block ] += 1;
			} else {
				no_room += 1;
			}
		}

		std::map<std::string,int>::iterator it;
		for (it = smap.begin(); it!=smap.end(); it++) {
			out << "    " << it->first << " " << it->second << std::endl;
		}

		out_total   += this->size();
		out_wi_room += this->size() - no_room;
		out_no_room += no_room;
	}

	void stat_suggest(){
		int total=0, ok=0;

		for (size_t i=0; i<this->size(); i++){
			Turma* sched = this->at(i);
			if ( sched->assigned && sched->suggested != "" ){


				total += 1;
				if ( sched->assigned->code == sched->suggested )
					ok += 1;
				else
					std::cout << "[!] " << sched->suggested << " = " << sched->assigned->code << std::endl;
			}
		}
		std::cout << "Sugestionadas: " << total << "; Acertadas: " << ok << std::endl;
	}


	friend std::ostream& operator<<(std::ostream& out, Turmas& schedules){
		out << schedules.size() << std::endl;
		for (size_t i=0; i<schedules.size(); i++){
			out << *schedules[i] << std::endl;
		}
		return out;
	}
};


/*-------------------------------------------------------------------------------------*/


/*=====================================================================================*/

class Semana{//creates a matrix of class vectors, each class in the vector relative to the day of the class as well as the starting time of the class
	Turmas data[DAYSIZE];
  public:
	inline Turmas& at(int hour){//returns a vector of classes at a certain day and hour of the week
		return (data[hour]);
	}

	inline Turmas& operator[](int hour){
		return this->at(hour);
	}

	void clear(){
		for (int i=0; i<DAYSIZE; i++){
			this->data[i].clear();
		}
	}

	friend std::ostream& operator<<(std::ostream& out, Semana& week){
		for (int i=0; i<DAYSIZE; i++){
			out << week.data[i].size() << std::endl;
		}
		return out;
	}
};

/*-------------------------------------------------------------------------------------*/



/*=====================================================================================*/

class TurmasIndex{//map showing the general occupation of classrooms by classes, used to assure classes meeting more than once a week meet in the same classroom
	std::map <int,Turmas> share;

  public:
	void mark(Turma* sched){//mark in the map another classroom assigned to a class
		share[sched->klass_id].push_back(sched);
	}

	Turmas find(int id){//returns the vector containing class t, for comparison on the assignments on the other times the class has met in the week
		std::map<int,Turmas>::iterator it = share.find(id);
		if (it != share.end()){
			return (it->second);
		} else {
			Turmas empty;
			return empty;
		}
	}

	std::map<int,Turmas>& getShared(){
		return share;
	}
};

/*-------------------------------------------------------------------------------------*/


/*=====================================================================================*/

struct Database {//makes the classroom assignment all classes needed
	Rooms      rooms;
	Turmas schedules;
	TurmasIndex index;//map containing the assignements for each class


	YAML::Node yaml_rooms;
	YAML::Node yaml_schedules;

	double max_dist;
	double factor_dist;
	double factor_free;


	Database(){}
	Database(std::string rooms_file, std::string schedules_file){
		this->loadRooms( rooms_file );
		this->loadSchedules( schedules_file );
	}

	inline void setFactors(double dist, double free, double max){
		this->factor_dist = dist;
		this->factor_free = free;
		this->max_dist = max;
	}

	bool loadRooms(std::string rooms_file);//load the classroom vector with data from input
	bool loadSchedules(std::string filename);


	void FillMap(Semana* week);//fill the map with pointers to the classes to be assigned


	double CalcDist(Sala& r , Turma& t);//calculates the euclidian distance between the classroom and the central location of the class
	double CalcFree(Sala& r, Turma& t);//calculates how much free space the classroom will have if assigned to a class
	double isAvaliable(Sala& r, Turma& t);//verify if the classroom is avaliable for use in the timeslot needed by the class
	double CalcCusto(Sala& r, Turma& t);//calculates the cost of assigning a class to a classroom


	bool  isOk();


	void resting(){
		int wi_room = 0, no_room = 0;
		for (size_t i=0; i<this->schedules.size(); i++){
			if ( this->schedules[i]->assigned == NULL )
				no_room += 1;
			else
				wi_room += 1;
		}
		std::cout << "With: " << wi_room << "; NoRoom: " << no_room << std::endl;
	}


	void statistics(){
		std::map <std::string,Turmas> smap;

		for (size_t i=0; i<this->schedules.size(); i++){
			Turma* sched = this->schedules[i];
			smap[ sched->department ].push_back(sched);
		}

		/*std::map<std::string,int>::iterator it;
		for (it = smap.begin(); it!=smap.end(); it++) {
			std::cout << "    " << it->first << std::endl;
			it->second.statistics(std::cout);
		}*/
	}




	void save(std::string filename){
		std::ofstream fout( filename, std::fstream::trunc );
		fout << this->yaml_schedules;
		fout.close();
	}
};

/*-------------------------------------------------------------------------------------*/




/*=====================================================================================*/

struct Ensalador {
	Database* database;

	int day;
	Rooms  rooms;
	Turmas schedules;
	Semana week2, week3, week4;//matrix containig the classes


	Ensalador(Database& database, int day, std::string block=""){
		this->day = day;
		this->database = &database;
		this->loadSchedules(day, block);
		this->loadRooms(block);
	}


	Ensalador(Database& database, int day, std::vector<std::string> blocks){
		this->day = day;
		this->database = &database;
		if ( blocks.size() == 0 ){
			this->loadSchedules(day);
			this->loadRooms();
		} else {
			for (int i=0; i<blocks.size(); i++){
				std::string block = blocks[i];
				this->loadSchedules(day, block);
				this->loadRooms(block);
			}
		}
	}


	void loadSchedules(int day, std::string block="");//load class matrix with info from input
	void loadRooms(std::string block="");

	void execute(int size);

  private:
	Matrix<double> buildMatrix(Rooms& rooms, Turmas& turmas);
	void          assignMatrix(Matrix<double>& matrix, Rooms& rooms, Turmas& turmas);
};

/*-------------------------------------------------------------------------------------*/




/*=====================================================================================*/

struct Department {
	std::string name;
	std::string center;
	Turmas schedules;

	Department(std::string name, std::string center){
		this->name = name;
		this->center = center;
	}
};

struct Departments : std::map<std::string,Department*> {
	Departments(Database& db){
		for (size_t i=0; i<db.schedules.size(); i++){
			Turma* sched = db.schedules[i];
			if ( sched->department.size() > 0){
				Departments::iterator it = this->find(sched->department);
				if ( it != this->end() ){
					it->second->schedules.push_back(sched);
				} else {
					Department* department = new Department(sched->department,sched->block);
					department->schedules.push_back(sched);
					std::pair<std::string,Department*> data(sched->department,department);
					this->insert( data );
				}
			}
		}
	}
};

/*-------------------------------------------------------------------------------------*/
