/*=====================================  HEADER  ======================================*/

#include "estruturas.h"
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <map>


using namespace std;

/*-------------------------------------------------------------------------------------*/


struct Adder {
	std::map<std::string,int> idx;

	void inc(std::string idx_str){
		std::map<std::string,int>::iterator it = idx.find(idx_str);
		if ( it == idx.end() ){
			idx.insert( std::pair<std::string,int>(idx_str,1) );
		} else {
			it->second += 1;
		}
	}

	void clear(){
		std::map<std::string,int>::iterator it = idx.begin();
		for (;it!=idx.end(); ++it){
			it->second = 0;
		}
	}

	void show(){
		if ( this->idx.size() == 0 )
			return ;
		std::map<std::string,int>::iterator it = idx.begin();
		for (;it!=idx.end(); ++it){
			cout << it->first << "(" << it->second << ") ";
		}
	}
};



struct Week {
	Turmas schedules[WEEKSIZE][DAYSIZE][6];

	void show(){
		for (int day=2; day<WEEKSIZE; day++){
			for (int hour=7; hour<DAYSIZE; hour++){
				printf("[%d-%02d]: ",day,hour);
				printf("%d ", schedules[day][hour][0].size());
				printf("%d ", schedules[day][hour][1].size());
				printf("%d ", schedules[day][hour][2].size());
				printf("%d ", schedules[day][hour][3].size());
				printf("%d ", schedules[day][hour][4].size());
				printf("%d ", schedules[day][hour][5].size());
				printf("\n");
			}
			printf("\n");
		}
		printf("\n\n");

		for (int day=2; day<WEEKSIZE; day++){
			for (int hour=7; hour<DAYSIZE; hour++){
				printf("[%d-%02d]: ",day,hour);
				Adder adder;
				for (int j=0; j<6; j++){
					for (int i=0; i<schedules[day][hour][j].size(); i++ ){
						adder.inc( schedules[day][hour][j][i]->block );
					}
				}
				adder.show();
				cout << "\n";
			}
			cout << "\n";
		}
	}


};



/*=====================================================================================*/

void main_open(Database& db, std::string filename){
	Week week;

	Adder adder;
	for (size_t i=0; i<db.rooms.size(); i++){
		adder.inc(db.rooms[i]->block);
	}
	cout << "Total de Salas: ";
	adder.show();
	cout << "\n";

	for (size_t i=0; i<db.schedules.size(); i++){
		Turma* sched = db.schedules[i];

		int size = sched->end - sched->ini;
		if ( size == 1 ){
			week.schedules[sched->dia][sched->ini][0].push_back(sched);
		} else if ( size == 2 ){
			if ( (sched->ini%2)==1 ){
				week.schedules[sched->dia][sched->ini][1].push_back(sched);
			} else {
				week.schedules[sched->dia][sched->ini][2].push_back(sched);
			}
		} else if ( size == 3 ){
			week.schedules[sched->dia][sched->ini][3].push_back(sched);
		}  else if ( size == 4 ){
			week.schedules[sched->dia][sched->ini][4].push_back(sched);
		}   else if ( size > 4 ){
			cout << *sched << endl;
			week.schedules[sched->dia][sched->ini][5].push_back(sched);
		}


		if ( !sched->yaml["assigned"] ){
			sched->yaml["do_malloc"] = 'y';
			if ( sched->yaml["suggested"] ){
				string suggest_code =  sched->yaml["suggested"].as<std::string>();
				if ( db.rooms.find(suggest_code) == NULL ){
					sched->yaml["assigned"] = sched->yaml["suggested"].as<std::string>();
				}
				//sched->yaml["assigned"] = sched->yaml["suggested"].as<std::string>();
			}
		}

		//sched->yaml.remove("assigned");
		/*if ( sched->yaml["suggested"] ){
			if ( sched->department == "hidraulica_e_saneamento" ){
				sched->yaml["assigned"]  = "";
				sched->yaml["suggested"] = "";
			} else {
				sched->yaml["assigned"] = sched->yaml["suggested"].as<std::string>();
			}

		} else {
			sched->yaml["assigned"] = "";
		}*/

	}


	week.show();

	std::ofstream fout( filename );
	fout << db.yaml_schedules;
	fout.close();
}


void main_close(Database& db, std::string filename){
	ofstream out;

	db.statistics();

	out.open (filename,ios::trunc);
	for (size_t i=0; i<db.schedules.size(); i++){
		Turma* sched = db.schedules[i];
		if ( sched->do_malloc_room ){
			out << " ";
			if( sched->yaml["assigned"] && sched->yaml["assigned"].as<std::string>() != "" ){
				out << sched->yaml["assigned"].as<std::string>();
			} else {
				out << "###";
			}
			out << " " << *sched << endl;
		}
	}
	out.close();
}



void main_close2(Database& db, std::string filename){
	ofstream out;
	out.open (filename,ios::trunc);
	for (size_t i=0; i<db.schedules.size(); i++){
		Turma* sched = db.schedules[i];
		if ( sched->do_malloc_room ){
			if( sched->yaml["assigned"] && sched->yaml["assigned"].as<std::string>() != "" ){
				out << sched->yaml["assigned"].as<std::string>();
			} else {
				out << "###";
			}
			out << " " << sched->id << endl;
		}
	}
	out.close();
}






void main_exec(Database& database, std::string _arg){
	YAML::Node arg = YAML::Load( _arg );

	vector<int> days;
	YAML::Node arg_day = arg["day"];
	for (int i=0; i<arg_day.size(); i++){
		days.push_back( arg_day[i].as<int>() );
	}

	vector<string> blocks;
	if ( arg["block"] ){
		YAML::Node arg_blocks = arg["block"];
		for (int i=0; i<arg_blocks.size(); i++){
			blocks.push_back( arg_blocks[i].as<std::string>() );
		}
	}

	int total=0, wi_room=0, no_room=0;
	for (int i=0; i<days.size(); i++){
		cout << "Dia " << days[i] << endl;
		Ensalador ens(database, days[i], blocks);
		ens.execute(4);
		ens.execute(3);
		ens.execute(2);

		ens.schedules.statistics(cout, total, wi_room, no_room);
		ens.schedules.stat_suggest();
	}

	if ( !database.isOk() ){
		cout << "[Error]: existe sala com conflito ######################################s\n";
	}

	cout << "### Total: " << total << "; WiRoom: " << wi_room << "; NoRoom: " << no_room << endl;

	database.save("current.yaml");
	database.resting();
}


/*-------------------------------------------------------------------------------------*/



/*======================================  MAIN  =======================================*/

int main(int argc, char *argv[]) {

	string cmd = argv[1];
	if ( cmd == "open" ){
		Database database("rooms.yaml", "schedules.yaml");
		main_open(database, "current.yaml");

	} else {
		Database database("rooms.yaml", "current.yaml");

if ( !database.isOk() ){
	cout << "Tem os seguintes conflitos\n";
}

		if ( cmd == "close" ) {
			main_close(database, "output.txt");
			main_close2(database, "binario.txt");

			Departments departments(database);
			Departments::iterator it = departments.begin();
			for (; it!=departments.end(); ++it){
				Department& department = *it->second;
				cout << department.name << "(" << department.center << ") - ";
				cout << department.schedules.size() << endl;
				Adder adder;
				for (size_t i=0; i<department.schedules.size(); i++){
					Turma* sched = department.schedules[i];
					if ( sched->assigned ){
						adder.inc( sched->assigned->block );
					} else if ( sched->yaml["assigned"]  ) {
						adder.inc( sched->yaml["assigned"].as<std::string>() );
					} else {
						adder.inc( "Sem Sala" );
					}
				}
				cout << "   ";
				adder.show();
				cout << "\n\n";
			}


		} else if ( cmd == "exec" ) {
			database.setFactors(10.0,1.0,500.0);
			main_exec(database, argv[2]);
		}
	}

	return 0;
}

/*-------------------------------------------------------------------------------------*/
