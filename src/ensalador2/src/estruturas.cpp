/*=====================================  HEADER  ======================================*/

#include "estruturas.h"
#include <munkres/munkres.h>
#include <munkres/matrix.h>
#include <cstring>

bool WithPrio = false;
bool doYAML = false;
bool reverse = false;


using namespace std;

/*-------------------------------------------------------------------------------------*/


/*=====================================================================================*/


bool Database::loadRooms(string filename){//function that adds the classroms from the input into the vector of classroms to be assigned
	cout << "Carregando " << filename << endl;
	this->yaml_rooms = YAML::LoadFile(filename);//YAML base node, in this case, the input file
	if (this->yaml_rooms.IsNull()) return false;

	YAML::Node yaml_rooms = this->yaml_rooms["rooms"];//YAML node, in this case, the first node in the "rooms" sections of the input
	if (yaml_rooms.IsNull()) return false;

	for (size_t i=0; i<yaml_rooms.size(); i++){
		Sala* room = new Sala(yaml_rooms[i]);
		this->rooms.push_back(room);//and adds it to the classroom vector
	}
	return true;
}


bool Database::loadSchedules(std::string filename){
cout << "Carregando " << filename << endl;
	this->yaml_schedules = YAML::LoadFile(filename);//YAML base node, the input file
	if (this->yaml_schedules.IsNull()) return false;

	YAML::Node schedules = yaml_schedules["schedules"];//YAML node, the "schedules" section of the input file
	if (schedules.IsNull()) return false;


	for (size_t i=0; i<schedules.size(); i++){
		Turma* sched = new Turma(schedules[i]);
		if( schedules[i]["assigned"] ){
			string code = schedules[i]["assigned"].as<std::string>();
			if ( code.size() > 0 )
				sched->setAssigned(this->rooms.find(code));
		}
		this->schedules.push_back(sched);
	}

	return true;
}




double Database::CalcDist(Sala& s, Turma& t){//uses real world coordinates to calculate the euclidian distance betwen 2 classrooms
	double X,X1,X2,Y,Y1,Y2,Z,Z1,Z2,dist;
	X1 = s.getLat();
	X2 = t.getLat();
	Y1 = s.getLon();
	Y2 = t.getLon();

	X = X1 - X2;
	Y = Y1 - Y2;
		
	Z1 = X * X;
	Z2 = Y * Y;

	Z = Z1 + Z2;

	dist = sqrt(Z) * 100000.0;//(*100000.0) Convert the distance to meters 

//cout << t.code << " " << s.code << " " << dist << endl;
	return dist;
}

double Database::CalcFree(Sala& s, Turma& t){//calculates how much of the classroom is filled by the supposed size of the class scheduled
	double free =  s.getCap() - t.getNv();
	double norm = (free/(double)s.getCap()) * NORM_FATOR;
	return norm;
}

double Database::isAvaliable(Sala& s, Turma& t){//Verifys if the classroom is empty at the time needed for the scheduled class
	/*bool isNot=false;//initiate by assuming the classroom is avaliable
	for (int j= t.ini;j<t.end;j++){//for as long as the class will need the room
		isNot = isNot || s.getOcc(j);//should the classroom be unavaliable, at some point s.getocc() will return true, and is not will perpetualy become true
	}
	if(isNot){//if it's not avaliable, return a high assignment cost
		return OCCUPIED;
	}
	else{//else there's no cost to the assignment in terms of avaliability
		return 0.0;
	}*/
}

double Database::CalcCusto(Sala& s, Turma& t){//calculates the cost of allocating classroom s to class t based on distance between classroom and geografical center of the class, capacity/size of the classroom/class, avaliability of the classroom and classroom sugestions
	double dist = CalcDist(s,t);
	double dist_norm = (dist/this->max_dist) * NORM_FATOR;
	double free = CalcFree(s,t);

	
	double suggested = 20;
	if (  s.code == t.suggested ){
		return 0;
		//suggested = 0;
	} else {
		if (s.getCap() < t.getNv())
			return NOMOREROOM;
	}


	double custo =  dist_norm*this->factor_dist + free*this->factor_free;// + suggested;

	Turmas inUse = this->index.find(t.klass_id);
	for ( size_t i=0; i<inUse.size(); ++i){
		Turma* it = inUse[i];//takes a class on the map of assignments already done
		if( it->id != t.id ){
			if(it->assigned != NULL){
				if (it->assigned->id == s.id){//if the class has already been assigned to the same classroom in another day, the cost of assinment is lower, thus helping assure the assignments will remain the same through the week
					custo -= 5;
				}
			}
		}
	}

	//cout << t.code << " " << s.code << " " << dist_norm << "[" << dist << "] + " << free  << "+" << suggested << "=" << custo << endl;

	return (custo<0) ? 0 : custo;
}



bool Database::isOk(){
	for (size_t i=0; i<this->rooms.size(); i++){
		if ( !this->rooms[i]->isOk() ){
			//cout << "[ERROR-conflito]: " << this->rooms[i]->code << endl;
		}
	}
	return true;
}






/*-------------------------------------------------------------------------------------*/







/*====================================  ENSALADOR  ====================================*/




void Ensalador::loadSchedules(int day, std::string block){
	int total=0, removed=0;
	for (size_t i=0; i<database->schedules.size(); i++){
		Turma* sched = database->schedules[i];
		if ( sched->dia != day )
			continue;
		if ( block.size()>0 && sched->block != block )
			continue;

		total += 1;
		if ( sched->yaml["assigned"] && sched->yaml["assigned"].as<std::string>() != "" ){
			removed += 1;
			continue;
		}

		//int ini = ((sched->ini%2)==0) ? sched->ini-1 : sched->ini;
		int size = sched->end - sched->ini;
		if (size >= 4){
			this->week4.at(sched->ini).push_back(sched);
		} else if (size == 3){
			this->week3.at(sched->ini).push_back(sched);
		} else {
			this->week2.at(sched->ini).push_back(sched);
		}
		this->schedules.push_back(sched);
	}

	cout << "loadSchedule{total: " << total << ", removed: " << removed << "}\n";
}






void Ensalador::loadRooms(std::string block){
	for (size_t i=0; i<database->rooms.size(); i++){
		if ( block=="" || database->rooms[i]->block == block )
			this->rooms.push_back(database->rooms[i]);
	}
}



void Ensalador::execute(int size){
	for (int hour=0; hour<DAYSIZE; hour++){
		Rooms   freerooms = this->rooms.getFree(this->day,hour,3);
		Turmas* turmas    = &this->week2[hour];
		if ( size == 3 )
			turmas = &this->week3[hour];
		else if ( size >= 4 )
			turmas = &this->week4[hour];

		if ( turmas->size() == 0 || freerooms.size() == 0 )
			continue;

		//cout << freerooms.size() << " " << turmas->size() << " ";
		int size = freerooms.size() - turmas->size();
		if ( size < 0 ){
			cout << "[Error]: Vai faltar " << -size << " salas na hora " << hour << ":30\n";
		}
		//cout << endl;


		Munkres<double> munkres;
		Matrix<double> matrix = this->buildMatrix(freerooms, *turmas);
		munkres.solve(matrix);
		this->assignMatrix(matrix, freerooms, *turmas);
	}
}







Matrix<double> Ensalador::buildMatrix(Rooms& rooms, Turmas& turmas){
	int nrows = rooms.size(); 
	int ncols = turmas.size();
	Matrix<double> matrix(nrows, ncols);
	for ( int row = 0 ; row < nrows ; row++ ) {
		for ( int col = 0 ; col < ncols ; col++ ) {
			matrix(row,col) = this->database->CalcCusto(*rooms[row], *turmas[col]);
		}
	}
	return matrix;
}


void Ensalador::assignMatrix(Matrix<double>& matrix, Rooms& rooms, Turmas& turmas){
	int ncols = matrix.columns();
	int nrows = matrix.rows();
	for ( int i_room=0; i_room<nrows; i_room++ ) {
		for ( int i_sched=0; i_sched<ncols; i_sched++ ) {
			if ( matrix(i_room,i_sched) == 0 ){
				Turma* sched = turmas[i_sched];
				Sala*  room  = rooms[i_room];
				sched->setAssigned(room);
				//cout << room->code << " " << sched->code << endl;
				//cout << *room << endl;
			}
		}
	}
}


/*-------------------------------------------------------------------------------------*/




















